﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitBox : MonoBehaviour
{
    public GameObject image1;
    public GameObject image2;
    public GameObject image3;
    public GameObject image4;

    public GameObject priceTag;

    public MapScene mapScene;

    public void Setup()
    {
        image1.SetActive(true);
        image2.SetActive(true);
        image3.SetActive(true);
        image4.SetActive(true);
        priceTag.SetActive(true);
    }

    public void Sold()
    {
        image1.SetActive(false);
        image2.SetActive(false);
        image3.SetActive(false);
        image4.SetActive(false);
        priceTag.SetActive(false);

        //
        GameManager.Instance.CurrentProfile.SoldBoxofVegetables();
        SQLiteManager.Instance.Profile_Update(GameManager.Instance.CurrentProfile);
        //SQLiteManager.Instance.LoadProfile(GameManager.Instance.CurrentDevice.DeviceID, GameManager.Instance.CurrentPlayer.PlayerID);
        mapScene.UpdateScorePanel();
    }

}
