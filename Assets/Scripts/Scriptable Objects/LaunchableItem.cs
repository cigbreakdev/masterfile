﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchableItem : ScriptableObject
{
    public enum ItemType
    {
        Unhealthy,
        Vegetable,
        Candy,
        Powerup,
        Gameover,
        Unidentify
    }

    // Launchable item's information.
    public new string name;
    public ItemType type = ItemType.Unidentify;

    public Vector3 scaleSize;
    public Sprite[] sprite;
    public Vector2 colliderSize;

    public AudioClip[] audioClips;

    public bool isSwipable;
    public bool isTappable;
    public bool isFlamable;

    public int itemHealth;
    public int lifeCost;
}

