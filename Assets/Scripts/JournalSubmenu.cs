﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalSubmenu : MonoBehaviour
{

    public GameObject healthPanel;
    public GameObject healthDetail;
    private Image healthPanelImage;

    public GameObject statPanel;
    public GameObject statDetail;
    private Image statsPanelImage;

    public GameObject missionPanel;
    public GameObject missionDetail;
    private Image missionPanelImage;

    private void Awake()
    {
        HideAll();
    }

    // Use this for initialization
    void OnEnable()
    {
        ClickHealthPanel();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void HideAll()
    {
        SetupHealthPanel(false);
        SetupStatPanel(false);
        SetupMissionPanel(false);
    }

    public void ClickHealthPanel()
    {
        SetupHealthPanel(true);
        SetupStatPanel(false);
        SetupMissionPanel(false);
    }

    public void ClickStatPanel()
    {
        SetupHealthPanel(false);
        SetupStatPanel(true);
        SetupMissionPanel(false);
    }

    public void ClickMissionPanel()
    {
        SetupHealthPanel(false);
        SetupStatPanel(false);
        SetupMissionPanel(true);
    }

    private void SetupHealthPanel(bool Active)
    {
        if (Active)
        {
            //
            healthDetail.SetActive(true);
            healthPanel.GetComponent<Image>().color = new Color(94, 210, 24, 100);
        }
        else
        {
            //
            healthDetail.SetActive(false);
            healthPanel.GetComponent<Image>().color = new Color(255, 255, 255, 100);
        }
    }

    private void SetupStatPanel(bool Active)
    {
        if (Active)
        {
            //
            statDetail.SetActive(true);
            statsPanelImage = statPanel.GetComponent<Image>();
            statsPanelImage.color = new Color(94, 210, 24, 100);
        }
        else
        {
            //
            statDetail.SetActive(false);
            statsPanelImage = statPanel.GetComponent<Image>();
            statsPanelImage.color = new Color(255, 255, 255, 100);
        }
    }

    private void SetupMissionPanel(bool Active)
    {
        if (Active)
        {
            //
            missionDetail.SetActive(true);
            missionPanelImage = missionPanel.GetComponent<Image>();
            missionPanelImage.color = new Color(94, 210, 24, 100);
        }
        else
        {
            //
            missionDetail.SetActive(false);
            missionPanelImage = missionPanel.GetComponent<Image>();
            missionPanelImage.color = new Color(255, 255, 255, 100);
        }
    }
}
