﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetYourQuitDate : MonoBehaviour {

    public Text menuText;
    public Text todayText;
    public Text tomorrowText;

    private string m_menuName = "Mission";
    private DateTime m_today;
    private DateTime m_tomorrow;

    // Called Order 1
    void OnEnable()
    {
        // Bring this game object to the front
        transform.SetAsLastSibling();
        //
        Setup();
    }

    void Setup () {
        menuText.text = m_menuName;
        m_today = DateTime.Now;
        todayText.text = String.Format("{0:MMMM d, yyyy}", m_today);
        m_tomorrow = m_today.AddDays(1.0f);
        tomorrowText.text = String.Format("{0:MMMM d, yyyy}", m_tomorrow);
    }

    public void ClosePanel()
    {
        // Hide this game object
        gameObject.SetActive(false);
    }
}
