﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerOtherChoice : MonoBehaviour
{

    public bool isChecked;

    public void Setup()
    {
        isChecked = false;
    }

    public void UpdateCheckedValue(Toggle toggle)
    {
        Debug.Log("Toogle::" + toggle.isOn);
        if (toggle.isOn == true)
        {
            isChecked = true;
        }
        else
        {
            isChecked = false;
        }
    }
}
