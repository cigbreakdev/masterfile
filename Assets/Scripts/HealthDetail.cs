﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDetail : MonoBehaviour {

    public GameObject myHealthItemPrefab;

    private int m_holderHieght = 420;
    private int m_healthItemCount = 0;

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called HealthDetail - OnEnable");
        SetupHealthDetail();
    }

    void SetupHealthDetail()
    {
        int newHeight = 0;

        m_healthItemCount = SQLiteManager.Instance.RowsCount("MyHealth");
        Debug.Log("m_healthItemCount:" + m_healthItemCount);

        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
        Debug.Log("containerRectTransform's height:" + containerRectTransform.rect.height);

        MyHealthItem current_item;
        for (int i = 1; i < m_healthItemCount; i++)
        {
            current_item = GetMyHealthItem(i);
            if(current_item == null)
            {
                //create a new item, name it, and set the parent
                GameObject newItem = Instantiate(myHealthItemPrefab) as GameObject;
                newItem.name = "HealthItem_" + i;
                MyHealthItem newHealthItem = newItem.GetComponent<MyHealthItem>();
                newHealthItem.Setup(i);
                newItem.transform.SetParent(gameObject.transform, false);

                newHeight += newHealthItem.height + 20;
            }
            else
            {
                current_item.Setup(i);
            }
        }

        /*
        if(newHeight > m_holderHieght)
        {
            containerRectTransform.sizeDelta.Set(containerRectTransform.rect.width, newHeight);
        }
        */
    }

    MyHealthItem GetMyHealthItem(int ID)
    {
        Component[] myHealthItems;
        myHealthItems = GetComponentsInChildren(typeof(MyHealthItem));

        if (myHealthItems == null)
        {
            return null;
        }
        else
        {
            foreach (MyHealthItem item in myHealthItems)
            {
                if(item.itemID == ID)
                {
                    return item;
                }
            }
            return null;
        }

    }

}
