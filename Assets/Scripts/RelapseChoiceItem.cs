﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RelapseChoiceItem : MonoBehaviour {

    public int itemID;
    public bool isChecked;
    public Text choiceLabel;

    private string m_choiceDetail;

    public void Setup(int ID)
    {
        RelapseChoice record = SQLiteManager.Instance.GetRelapseChoiceItem(ID);

        if (record.ID > 0)
        {
            itemID = record.ID;
            choiceLabel.text = record.Detail;
            isChecked = false;
        }
    }

    public void UpdateCheckedValue(Toggle toggle)
    {
        Debug.Log("Toogle::" + toggle.isOn);
        if(toggle.isOn == true)
        {
            isChecked = true;
        }
        else
        {
            isChecked = false;
        }
    }
}
