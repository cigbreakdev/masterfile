﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour {

	public Image image;

	private Sprite sprite = null;

	// Use this for initialization
	void Start () {
		sprite = Resources.Load<Sprite> ("QuitMissionIcons/04_piggybank");
		image.sprite = sprite;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
