﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private string menuName = "Main Menu";

    public Text menuText;

    public GameObject menuJournal;
    public GameObject menuShop;
    public GameObject menuGuide;
    public GameObject menuAchievement;
    public GameObject menuCredit;
    public GameObject menuSetting;


    void OnEnable()
    {
        //
        menuText.text = menuName;
        /*
        menuGuide.SetActive(false);
        menuCredit.SetActive(false);
        menuSetting.SetActive(false);
        */
    }

    public void ClosePanel()
    {
        // Hide this game object
        gameObject.SetActive(false);
    }

}
