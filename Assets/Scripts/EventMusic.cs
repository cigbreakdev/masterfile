﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventMusic : MonoBehaviour
{
    public AudioClip clip;

    public enum DoAfter
    {
        FadeIn,
        Restart,
        Stop
    }
    public DoAfter whatToDo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            switch (whatToDo)
            {
                case DoAfter.FadeIn:
                    SoundManager.Instance.StartCoroutine("PlayEventMusic", clip);

                    break;
                case DoAfter.Restart:
                    SoundManager.Instance.StartCoroutine("PlayEventMusicThenRestart", clip);

                    break;
                case DoAfter.Stop:
                    SoundManager.Instance.StartCoroutine("PlayEventMusicThenStop", clip);
                    break;
            }
        }
    }
}
