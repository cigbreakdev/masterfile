﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Settings")]
    // Use this flag for debug mode running.
    public bool debugMode = true;
    // Use this flag to clear all the key and value in PlayerPrefs.
    public bool clearAllPlayerPrefs = false;

    // So we only have to change it here, and nowhere else
    // N.B. this reflects the baked in 30 Sounds in gameplay.cs
    public int NumberofBackgroundMusicFiles = 30;

    [Header("Variables")]
    // To seperate the table
    public bool IsDebugBuild;
    //
    public bool IsReady = false;
    //
    public bool IsCustomisedNewDayLoad;
    //
    public int LoginID;
    //
    public int OnlineLoginID;
    //
    public int PauseID;
    // Calculate from each level that user played. Need to update every time that user finish play each level.
    public float TotalPlayedTime = 0.0f;
    //
    public MobileDevice CurrentDevice;
    //
    public Player CurrentPlayer;
    //
    public Profile CurrentProfile;
    //
    public Profile OnlineProfile;
    //
    public int CurrentLevel;
    public LevelPlayed lastLevelPlayed;

    [Header("Display Only")]
    [SerializeField]
    private string _deviceInfo = string.Empty;
    [SerializeField]
    private int _playerID;
    [SerializeField]
    private int _profileID;
    [SerializeField]
    private string _secretKey = "Cigbreak4";


    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(GameManager).Name;
                    instance = obj.AddComponent<GameManager>();
                }
            }
            return instance;
        }
    }

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("GameManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    public void Init()
    {
        Debug.Log("GameManager - Init() called.");

        NetworkManager.Instance.Init();

        LocationManager.Instance.Init();

        SQLiteManager.Instance.Init();

        OnlineDBManager.Instance.Init();

        Debug.Log("GameManager:InitGame");
        Debug.Log("GameManager.Instance.IsCustomisedNewDayLoad==" + IsCustomisedNewDayLoad);

        IsReady = false;

        if (clearAllPlayerPrefs)
        {
            // Removes all keys and values from the preferences.
            PlayerPrefs.DeleteAll();

            Debug.Log("Removes all keys and values from the preferences.");
        }

        // Initialise data in MobileDevice table to get the device's information.
        InitialiseMobileDevice();
        // Initialise data in Player table.
        InitialisePlayer();
        // Initialise data in Profile table.
        InitialiseProfile();

        //
        lastLevelPlayed = null;
        //
        IsReady = true;
    }

    private void InitialiseMobileDevice()
    {
        string tempDeviceInfo = GeneratedDeviceInfo();
        // Checking the records in MobileDevice in SQLite database.
        if (SQLiteManager.Instance.IsDeviceExist(tempDeviceInfo))
        {
            // Load device's data from MobileDevice.
            CurrentDevice = SQLiteManager.Instance.LoadDevice(tempDeviceInfo);
            //
            Debug.Log("Load device.");
        }
        else
        {
            // SQLite has no record.
            CurrentDevice = new MobileDevice();
            CurrentDevice.DeviceInfo = GeneratedDeviceInfo();
            CurrentDevice.Platform = Application.platform.ToString();
            CurrentDevice.Model = SystemInfo.deviceModel;
            CurrentDevice.DeviceOS = SystemInfo.operatingSystem;
            CurrentDevice.AccelerometerSupported = SystemInfo.supportsAccelerometer.ToString();
            CurrentDevice.GyroscopeSupported = SystemInfo.supportsGyroscope.ToString();
            CurrentDevice.LocationServiceSupported = SystemInfo.supportsLocationService.ToString();
            CurrentDevice.ScreenResolution = Screen.currentResolution.ToString();
            CurrentDevice.ScreenWidth = Screen.width;
            CurrentDevice.ScreenHeight = Screen.height;
            CurrentDevice.ScreenDPI = Screen.dpi;

            DateTime curDateTime = DateTime.Now;
            string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
            CurrentDevice.CreateDate = mySQLTime;
            CurrentDevice.Status = 1;
            //
            CurrentDevice.DeviceID = SQLiteManager.Instance.Device_Insert(CurrentDevice);
            //
            Debug.Log("Create new device.");
        }
        //
        Debug.Log(CurrentDevice.ToString());
        // To display value at the inspector.
        _deviceInfo = CurrentDevice.DeviceInfo;
    }

    private void InitialisePlayer()
    {
        string tempDeviceInfo = GeneratedDeviceInfo();
        // Checking the records in Player in SQLite database.
        // Using DeviceInfo as PlayerName in Player Table in SQLite database.
        if (SQLiteManager.Instance.IsPlayerExist(tempDeviceInfo))
        {
            // Load player's data from Player.
            CurrentPlayer = SQLiteManager.Instance.LoadPlayer(tempDeviceInfo);
            //
            Debug.Log("Load player.");
        }
        else
        {
            // SQLite has no record.
            CurrentPlayer = new Player();
            CurrentPlayer.PlayerName = GeneratedDeviceInfo();

            DateTime curDateTime = DateTime.Now;
            string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
            CurrentPlayer.CreateDate = mySQLTime;
            CurrentPlayer.Status = 1;
            //
            CurrentPlayer.PlayerID = SQLiteManager.Instance.Player_Insert(CurrentPlayer);
            //
            Debug.Log("Create new player.");
        }
        //
        Debug.Log(CurrentPlayer.ToString());
        // To display value at the inspector.
        _playerID = CurrentPlayer.PlayerID;
    }

    private void InitialiseProfile()
    {
        // Checking the records in Profile in SQLite database.
        // Using DeviceID and PlayerID as key column in Profile Table in SQLite database.
        if (SQLiteManager.Instance.IsProfileExist(CurrentDevice.DeviceID, CurrentPlayer.PlayerID))
        {
            // Load profile's data from Profile.
            CurrentProfile = SQLiteManager.Instance.LoadProfile(CurrentDevice.DeviceID, CurrentPlayer.PlayerID);
            //
            Debug.Log("Load profile.");
        }
        else
        {
            // SQLite has no record.
            CurrentProfile = new Profile();
            CurrentProfile.DeviceID = CurrentDevice.DeviceID;
            CurrentProfile.PlayerID = CurrentPlayer.PlayerID;

            DateTime curDateTime = DateTime.Now;
            string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
            CurrentProfile.StartDate = mySQLTime;
            CurrentProfile.LastLoginDate = mySQLTime;
            CurrentProfile.CreateDate = mySQLTime;
            CurrentProfile.Status = 1;
            //
            CurrentProfile.ProfileID = SQLiteManager.Instance.Profile_Insert(CurrentProfile);
            //
            Debug.Log("Create new profile.");
        }
        //
        Debug.Log(CurrentProfile.ToString());
        // To display value at the inspector.
        _profileID = CurrentProfile.ProfileID;
    }

    /// <summary>
    /// Generateds the device info.
    /// </summary>
    /// <returns>The device info.</returns>
    private string GeneratedDeviceInfo()
    {
        if (SystemInfo.unsupportedIdentifier != SystemInfo.deviceUniqueIdentifier)
        {
            return MD5Sum(SystemInfo.deviceUniqueIdentifier + _secretKey);
        }
        else
            return string.Empty;
    }

    public string MD5Sum(string strToTncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToTncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}
