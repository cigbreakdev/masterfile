﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeviceResponse
{
    // Devices and Players used the same response.
    public string status;
    public string message;
    public string id;
    public string date_in_record;
}

public class ProfileResponse
{
    public string status;
    public string message;
    public ProfileData data;
}

public class ProfileData
{
    public string ProfileID;
    public string DeviceID;
    public string PlayerID;
    public string ReadinessRate;
    public string CigarettePerDay;
    public string FirstCigAfterWakeup;
    public string IsNonSmoker;
    public string StartDate;
    public string LastLoginDate;
    public string FirstTimeOfDay;
    public string TotalLoginDays;
    public string TotalLoginCounts;
    public string ConsecutiveLogin;
}

public class OnlineDBManager : MonoBehaviour
{
    public string BaseURL = "https://www.healthygames.uk/dbservices/";

    private static OnlineDBManager instance;
    public static OnlineDBManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<OnlineDBManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(OnlineDBManager).Name;
                    instance = obj.AddComponent<OnlineDBManager>();
                }
            }
            return instance;
        }
    }

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("OnlineDBManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    public void Init()
    {
        Debug.Log("OnlineDBManager - Init() called.");
    }

    public IEnumerator DeviceRegistration()
    {
        yield return StartCoroutine(OnlineDeviceRegistration(SQLiteManager.Instance.UpdateOnlineDeviceID));
    }

    private IEnumerator OnlineDeviceRegistration(Action<int, string, int> updateOnlineDeviceIDCall)
    {
        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "INSERT");
        //
        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Device.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("OnlineDeviceRegistration: WWW fail.");
                // Fail
                updateOnlineDeviceIDCall(-1, "", 1);
            }
            else
            {
                string jsonString = www.downloadHandler.text;
                DeviceResponse dResponse = JsonUtility.FromJson<DeviceResponse>(jsonString);

                if (dResponse.status == "true")
                {
                    switch (dResponse.message)
                    {
                        case "NEW":
                            Debug.Log("Register new device complete, record ID = " + dResponse.id);
                            updateOnlineDeviceIDCall(int.Parse(dResponse.id), "", 0);
                            break;
                        case "DOWNLOAD":
                            Debug.Log("Download device record from server, record ID = " + dResponse.id);
                            updateOnlineDeviceIDCall(int.Parse(dResponse.id), dResponse.date_in_record, 0);
                            break;
                        case "MATCH":
                            Debug.Log("Device: MATCH.");
                            break;
                        case "NOT_MATCH":
                            Debug.Log("Device: NOT_MATCH.");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    // Show results as text
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("OnlineDeviceRegistration: Insert online database fail.");
                    updateOnlineDeviceIDCall(-1, "", 1);
                }
            }
        }
    }

    public IEnumerator PlayerRegistration()
    {
        yield return StartCoroutine(OnlinePlayerRegistration(SQLiteManager.Instance.UpdateOnlinePlayerID));
    }

    private IEnumerator OnlinePlayerRegistration(Action<int, string, int> updateOnlinePlayerIDCall)
    {
        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("playerName", GameManager.Instance.CurrentPlayer.PlayerName);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "INSERT");

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Player.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("OnlinePlayerRegistration: WWW fail.");
                // Fail
                updateOnlinePlayerIDCall(-1, "", 1);
            }
            else
            {
                string jsonString = www.downloadHandler.text;
                DeviceResponse dResponse = JsonUtility.FromJson<DeviceResponse>(jsonString);

                if (dResponse.status == "true")
                {
                    switch (dResponse.message)
                    {
                        case "NEW":
                            Debug.Log("Register new player complete, record ID = " + dResponse.id);
                            updateOnlinePlayerIDCall(int.Parse(dResponse.id), "", 0);
                            break;
                        case "DOWNLOAD":
                            Debug.Log("Download player record from server, record ID = " + dResponse.id);
                            updateOnlinePlayerIDCall(int.Parse(dResponse.id), dResponse.date_in_record, 0);
                            break;
                        case "MATCH":
                            Debug.Log("Player: MATCH.");
                            break;
                        case "NOT_MATCH":
                            Debug.Log("Player: NOT_MATCH.");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    // Show results as text
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("OnlinePlayerRegistration: Insert online database fail.");
                    updateOnlinePlayerIDCall(-1, "", 1);
                }
            }
        }
    }

    public IEnumerator ProfileRegistration()
    {
        yield return StartCoroutine(OnlineProfileRegistration("INSERT", SQLiteManager.Instance.UpdateOnlineProfile));
    }

    public IEnumerator ProfileUpdate()
    {
        yield return StartCoroutine(OnlineProfileRegistration("UPDATE", SQLiteManager.Instance.UpdateOnlineProfile));
    }

    private IEnumerator OnlineProfileRegistration(string Action ,Action<Profile, int> updateOnlineProfileCall)
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("profileID", GameManager.Instance.CurrentProfile.OnlineProfileID);
        form.AddField("readinessRate", GameManager.Instance.CurrentProfile.ReadinessRate.ToString());
        form.AddField("cigarettePerDay", GameManager.Instance.CurrentProfile.CigarettePerDay);
        form.AddField("firstCigAfterWakeup", GameManager.Instance.CurrentProfile.FirstCigaretteAfterWakeup.ToString());
        form.AddField("isNonSmoker", GameManager.Instance.CurrentProfile.IsNonSmoker);
        form.AddField("startDate", GameManager.Instance.CurrentProfile.StartDate);
        form.AddField("lastLoginDate", GameManager.Instance.CurrentProfile.LastLoginDate);
        form.AddField("firstTimeOfDay", GameManager.Instance.CurrentProfile.FirstTimeOfDay);
        form.AddField("totalLoginDays", GameManager.Instance.CurrentProfile.TotalLoginDays);
        form.AddField("totalLoginCounts", GameManager.Instance.CurrentProfile.TotalLoginCounts);
        form.AddField("consecutiveLogin", GameManager.Instance.CurrentProfile.ConsecutiveLogin);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", Action);
        //

        Profile tempProfile = new Profile();
        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Profile.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("OnlineProfileRegistration: WWW fail.");
                updateOnlineProfileCall(tempProfile, 1);
            }
            else
            {
                // Show results as text
                Debug.Log("Result:" + www.downloadHandler.text);

                string jsonString = www.downloadHandler.text;
                ProfileResponse pResponse = JsonUtility.FromJson<ProfileResponse>(jsonString);
                ProfileData pData;
                if (pResponse.status == "true")
                {
                    switch (pResponse.message)
                    {
                        case "NEW":
                            jsonString = jsonString.Replace("{\"status\":true,\"message\":\"NEW\",\"data\":{", "{");
                            jsonString = jsonString.Replace("}}", "}");
                            pData = JsonUtility.FromJson<ProfileData>(jsonString);
                            //
                            tempProfile = new Profile(pData, GameManager.Instance.CurrentProfile);
                            Debug.Log("OnlineProfileRegistration: New.");
                            updateOnlineProfileCall(tempProfile, 0);
                            break;
                        case "DOWNLOAD":
                            jsonString = jsonString.Replace("{\"status\":true,\"message\":\"DOWNLOAD\",\"data\":{", "{");
                            jsonString = jsonString.Replace("}}", "}");
                            pData = JsonUtility.FromJson<ProfileData>(jsonString);
                            //
                            tempProfile = new Profile(pData, GameManager.Instance.CurrentProfile);
                            Debug.Log("OnlineProfileRegistration: Download.");
                            updateOnlineProfileCall(tempProfile, 0);
                            break;
                        case "OVERWRITE":
                            Debug.Log("Profile: OVERWRITE.");
                            break;
                        case "MATCH":
                            Debug.Log("Profile: MATCH.");
                            break;
                        case "NOT_MATCH":
                            Debug.Log("Profile: NOT_MATCH.");
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Debug.Log("OnlineProfileRegistration: fail.");
                    updateOnlineProfileCall(tempProfile, 1);
                }
            }
        }
    }






















    #region Table :: Device

    /// <summary>
    /// Update the device's information into online database server.
    /// </summary>
    public IEnumerator Device_OnlineInsert()
    {
        yield return StartCoroutine(Device_OnlineInsert(SQLiteManager.Instance.Device_Insert));
    }

    private IEnumerator Device_OnlineInsert(Action<MobileDevice, int> insertLocalDBCall)
    {
        Debug.Log("DeviceInfo:" + GameManager.Instance.CurrentDevice.DeviceInfo);

        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "INSERT");
        //
        MobileDevice record = new MobileDevice();
        record.DeviceInfo = GameManager.Instance.CurrentDevice.DeviceInfo;
        record.CreateDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Device.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Device_Online_Insert: WWW fail.");
                insertLocalDBCall(record, 1);
            }
            else
            {
                int recordID;
                if (Int32.TryParse(www.downloadHandler.text, out recordID))
                {
                    // Show results as text
                    GameManager.Instance.CurrentDevice.OnlineDeviceID = recordID;
                    record.OnlineDeviceID = recordID;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Device_Online_Insert: Insert online database successful.");
                    insertLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    GameManager.Instance.CurrentDevice.OnlineDeviceID = -1;
                    record.OnlineDeviceID = -1;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Device_Online_Insert: Insert online database fail.");
                    insertLocalDBCall(record, 1);
                }
            }
        }
    }

    #endregion

    #region Table :: Player

    /// <summary>
    /// Update the player's information into online database server.
    /// </summary>
    public IEnumerator Player_OnlineInsert()
    {
        yield return StartCoroutine(Player_OnlineInsert(SQLiteManager.Instance.Player_Insert));
    }

    private IEnumerator Player_OnlineInsert(Action<Player, int> insertLocalDBCall)
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("playerName", GameManager.Instance.CurrentPlayer.PlayerName);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "INSERT");
        //
        Player record = new Player();
        record.PlayerName = GameManager.Instance.CurrentPlayer.PlayerName;
        record.CreateDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Player.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Player_OnlineInsert: WWW fail.");
                insertLocalDBCall(record, 1);
            }
            else
            {
                int recordID;
                if (Int32.TryParse(www.downloadHandler.text, out recordID))
                {
                    // Show results as text
                    GameManager.Instance.CurrentPlayer.OnlinePlayerID = recordID;
                    record.OnlinePlayerID = recordID;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Player_OnlineInsert: Insert online database successful.");
                    insertLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    GameManager.Instance.CurrentPlayer.OnlinePlayerID = -1;
                    record.OnlinePlayerID = -1;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Player_OnlineInsert: Insert online database fail.");
                    insertLocalDBCall(record, 1);
                }
            }
        }
    }

    #endregion

    #region Table :: Profile

    /// <summary>
    /// Update the profile's information into online database server.
    /// </summary>
    public IEnumerator Profile_OnlineCreate()
    {
        yield return StartCoroutine(Profile_OnlineCreate(SQLiteManager.Instance.Profile_Create));
    }

    private IEnumerator Profile_OnlineCreate(Action<Profile, int> createLocalDBCall)
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("profileID", GameManager.Instance.CurrentProfile.OnlineProfileID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "CREATE");
        //
        Profile record = new Profile();
        record.DeviceID = GameManager.Instance.CurrentDevice.DeviceID;
        record.OnlineDeviceID = GameManager.Instance.CurrentDevice.OnlineDeviceID;
        record.PlayerID = GameManager.Instance.CurrentPlayer.PlayerID;
        record.OnlinePlayerID = GameManager.Instance.CurrentPlayer.OnlinePlayerID;
        record.StartDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Profile.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Profile_OnlineCreate: WWW fail.");
                createLocalDBCall(record, 1);
            }
            else
            {
                int recordID;
                if (Int32.TryParse(www.downloadHandler.text, out recordID))
                {
                    // Show results as text
                    GameManager.Instance.CurrentProfile.OnlineProfileID = recordID;
                    record.OnlineProfileID = recordID;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Profile_OnlineCreate: Create online database successful.");
                    createLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    GameManager.Instance.CurrentProfile.OnlineProfileID = -1;
                    record.OnlineProfileID = -1;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("Profile_OnlineCreate: Create online database fail.");
                    createLocalDBCall(record, 1);
                }
            }
        }
    }

    /// <summary>
    /// Download the profile's information from online database server.
    /// </summary>
    public IEnumerator Profile_Download()
    {
        yield return null;
        //yield return StartCoroutine (Profile_Download (SQLiteManager.Instance.Profile_Update));
    }

    private IEnumerator Profile_Download(Action<Profile, int> updateLocalDBCall)
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("profileID", GameManager.Instance.CurrentProfile.OnlineProfileID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "DOWNLOAD");
        //
        Profile record = new Profile();
        record.DeviceID = GameManager.Instance.CurrentDevice.DeviceID;
        record.OnlineDeviceID = GameManager.Instance.CurrentDevice.OnlineDeviceID;
        record.PlayerID = GameManager.Instance.CurrentPlayer.PlayerID;
        record.OnlinePlayerID = GameManager.Instance.CurrentPlayer.OnlinePlayerID;
        record.StartDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Profile.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Profile_Download: WWW fail.");
                updateLocalDBCall(record, 1);
            }
            else
            {
                // Parse return_string into Profile class.
                record = new Profile(www.downloadHandler.text);
                Debug.Log(www.downloadHandler.text);

                if (record.OnlineProfileID > 0)
                {
                    Debug.Log("Profile_Download: Profile_Download from database successful.");
                    updateLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    Debug.Log("Profile_Download: Download from online database fail.");
                    updateLocalDBCall(record, 1);
                }
            }
        }
    }

    #endregion

    #region Table :: Login

    /// <summary>
    /// Insert the login information into online database server.
    /// </summary>
    /// <param name="comment">Comment.</param>
    public IEnumerator Login_OnlineInsert(string comment)
    {
        yield return StartCoroutine(Login_OnlineInsert(comment, SQLiteManager.Instance.Login_Insert));
    }

    private IEnumerator Login_OnlineInsert(string comment, Action<Login, int> insertLocalDBCall)
    {
        Debug.Log("DeviceInfo:" + GameManager.Instance.CurrentDevice.DeviceInfo);

        yield return StartCoroutine(LocationManager.Instance.TrackingLocation());
        LocationInfo currentLocation = LocationManager.Instance.CurrentLocation;
        Debug.Log("currentLocation:" + currentLocation.ToString());

        yield return StartCoroutine(LocationManager.Instance.GetPostCode(currentLocation.Latitude, currentLocation.Longitude));
        string postcode = LocationManager.Instance.Postcode;
        Debug.Log("postcode:" + postcode);

        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("loginID", GameManager.Instance.OnlineLoginID);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("latData", currentLocation.Latitude.ToString());
        form.AddField("longData", currentLocation.Longitude.ToString());
        form.AddField("postcode", postcode);
        form.AddField("comment", comment);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "INSERT");
        //
        Login record = new Login();
        record.PlayerID = GameManager.Instance.CurrentPlayer.PlayerID;
        record.OnlinePlayerID = GameManager.Instance.CurrentPlayer.OnlinePlayerID;
        record.DeviceID = GameManager.Instance.CurrentDevice.DeviceID;
        record.OnlineDeviceID = GameManager.Instance.CurrentDevice.OnlineDeviceID;
        record.StartTime = mySQLTime;
        record.StartLat = currentLocation.Latitude;
        record.StartLong = currentLocation.Longitude;
        record.CommentS = comment;
        record.CreateDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Login.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("Login_Online_Insert: WWW fail.");
                insertLocalDBCall(record, 1);
            }
            else
            {
                Debug.Log("Return:" + www.downloadHandler.text);
                int recordID;
                if (Int32.TryParse(www.downloadHandler.text, out recordID))
                {
                    // Show results as text
                    GameManager.Instance.OnlineLoginID = recordID;
                    Debug.Log("Online Login ID:" + GameManager.Instance.OnlineLoginID);
                    Debug.Log("Login_Online_Insert: Insert online database successful.");
                    insertLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    GameManager.Instance.OnlineLoginID = -1;
                    Debug.Log("Online Login ID:" + GameManager.Instance.OnlineLoginID);
                    Debug.Log("Login_Online_Insert: Insert online database fail.");
                    insertLocalDBCall(record, 1);
                }
            }
        }
    }

    /// <summary>
    /// Update the login information into online database server.
    /// </summary>
    /// <param name="comment">Comment.</param>
    public IEnumerator Login_OnlineUpdate(string comment)
    {
        yield return StartCoroutine(Login_OnlineUpdate(comment, SQLiteManager.Instance.Login_Update));
    }

    private IEnumerator Login_OnlineUpdate(string comment, Action<Login, int> updateLocalDBCall)
    {
        Debug.Log("DeviceInfo:" + GameManager.Instance.CurrentDevice.DeviceInfo);

        yield return StartCoroutine(LocationManager.Instance.TrackingLocation());
        LocationInfo currentLocation = LocationManager.Instance.CurrentLocation;
        Debug.Log("currentLocation:" + currentLocation.ToString());

        yield return StartCoroutine(LocationManager.Instance.GetPostCode(currentLocation.Latitude, currentLocation.Longitude));
        string postcode = LocationManager.Instance.Postcode;
        Debug.Log("postcode:" + postcode);

        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        WWWForm form = new WWWForm();
        if (GameManager.Instance.IsDebugBuild)
        {
            form.AddField("buildType", "DEBUG");
        }
        else
        {
            form.AddField("buildType", "PRODUCTION");
        }
        form.AddField("deviceInfo", GameManager.Instance.CurrentDevice.DeviceInfo);
        form.AddField("loginID", GameManager.Instance.OnlineLoginID);
        form.AddField("playerID", GameManager.Instance.CurrentPlayer.OnlinePlayerID);
        form.AddField("deviceID", GameManager.Instance.CurrentDevice.OnlineDeviceID);
        form.AddField("recordDate", mySQLTime);
        form.AddField("latData", currentLocation.Latitude.ToString());
        form.AddField("longData", currentLocation.Longitude.ToString());
        form.AddField("postcode", postcode);
        form.AddField("comment", comment);
        form.AddField("hash", GameManager.Instance.MD5Sum(GameManager.Instance.CurrentDevice.DeviceInfo));
        form.AddField("action", "UPDATE");
        //
        Login record = new Login();
        record.PlayerID = GameManager.Instance.CurrentPlayer.PlayerID;
        record.OnlinePlayerID = GameManager.Instance.CurrentPlayer.OnlinePlayerID;
        record.DeviceID = GameManager.Instance.CurrentDevice.DeviceID;
        record.OnlineDeviceID = GameManager.Instance.CurrentDevice.OnlineDeviceID;
        record.EndTime = mySQLTime;
        record.EndLat = currentLocation.Latitude;
        record.EndLong = currentLocation.Longitude;
        record.CommentE = comment;
        record.UpdateDate = mySQLTime;

        using (UnityWebRequest www = UnityWebRequest.Post(BaseURL + "Login.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                Debug.Log("LoginInfo_Online_Update: WWW fail.");
                updateLocalDBCall(record, 1);
            }
            else
            {
                int recordID;
                if (Int32.TryParse(www.downloadHandler.text, out recordID))
                {
                    // Show results as text
                    GameManager.Instance.OnlineLoginID = recordID;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("LoginInfo_Online_Update: Update online database successful.");
                    updateLocalDBCall(record, 0);
                }
                else
                {
                    // Show results as text
                    GameManager.Instance.OnlineLoginID = -1;
                    Debug.Log(www.downloadHandler.text);
                    Debug.Log("LoginInfo_Online_Update: Update online database fail.");
                    updateLocalDBCall(record, 1);
                }
            }
        }
    }

    #endregion
}
