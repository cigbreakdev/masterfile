﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LocationManager : MonoBehaviour
{
    private static LocationManager instance;
    public static LocationManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LocationManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(LocationManager).Name;
                    instance = obj.AddComponent<LocationManager>();
                }
            }
            return instance;
        }
    }

    // Store the device's current location.
    public LocationInfo CurrentLocation;
    //
    public string Postcode;

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("LocationManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    public void Init()
    {
        Debug.Log("LocationManager - Init() called.");
        //
        CurrentLocation = new LocationInfo();
        Postcode = string.Empty;
    }

	public IEnumerator TrackingLocation ()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
            // Mock up location.
            CurrentLocation = new LocationInfo ();
			yield break;
		}

		// Start service before querying location
		Input.location.Start ();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1) {
			Debug.Log ("LocationService time out.");
            // Mock up location.
            CurrentLocation = new LocationInfo ();
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.Log ("LocationService's status failed.");
            // Mock up location.
            CurrentLocation = new LocationInfo ();
		} else {
			Debug.Log ("LocationService's status success.");
            // Access granted and location value could be retrieved
            CurrentLocation = new LocationInfo (Input.location.lastData.latitude, Input.location.lastData.longitude);
		}

		// Stop service if there is no need to query location updates continuously
		Input.location.Stop ();

		yield return null;
	}

	public IEnumerator GetPostCode(float latitude, float longitude)
	{
		yield return StartCoroutine (RetrievePostcode(latitude, longitude, SetPostcode));
	}

	private IEnumerator RetrievePostcode (float latitude, float longitude, Action<string> setPostcodeCall)
	{
		using (UnityWebRequest www = UnityWebRequest.Get ("http://api.postcodes.io/postcodes?lon=" + longitude.ToString () + "&lat=" + latitude.ToString ())) {
			yield return www.SendWebRequest ();

			string jsonString = string.Empty;
			string temp_postcode = string.Empty;

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				temp_postcode = string.Empty;
				setPostcodeCall (temp_postcode);
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				// Or retrieve results as binary data
				//byte[] results = www.downloadHandler.data;
				jsonString = www.downloadHandler.text;
				PostcodeResponse pResponse = JsonUtility.FromJson<PostcodeResponse> (jsonString);
				if (pResponse.status == "200") {
					//
					jsonString = jsonString.Replace ("{\"status\":200,\"result\":[", "");
					jsonString = jsonString.Replace ("]}", "");
					jsonString = jsonString.Substring (0, jsonString.IndexOf ("}},{")) + "}}";
					//
					PostcodeRecord pRecords = JsonUtility.FromJson<PostcodeRecord> (jsonString);
					temp_postcode = pRecords.postcode.Replace (" ", "");
					Debug.Log (temp_postcode);
					setPostcodeCall (temp_postcode);
				} else {
					temp_postcode = string.Empty;
					Debug.Log (temp_postcode);
					setPostcodeCall (temp_postcode);
				}
			}
		}
	}

	private void SetPostcode (string inPostcode)
	{
		Debug.Log ("Get Postcode Success.");
		//
		Postcode = inPostcode;
	}
}

[System.Serializable]
public class LocationInfo
{
    public float Latitude;
    public float Longitude;

    /// <summary>
    /// Initializes a new instance of the <see cref="LocationInfo"/>class with mockup location.
    /// </summary>
    public LocationInfo()
    {
        Latitude = 51.403104f;
        Longitude = -0.303937f;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="LocationInfo"/> class.
    /// </summary>
    /// <param name="latitude">Latitude.</param>
    /// <param name="longitude">Longitude.</param>
    public LocationInfo(float latitude, float longitude)
    {
        Latitude = latitude;
        Longitude = longitude;
    }

    /// <summary>
    /// Returns a string that represents the current object.
    /// </summary>
    /// <returns>A string that represents the current object.</returns>
    public override string ToString()
    {
        return string.Format("[LocationInfo] latitude:" + Latitude + ", longitude:" + Longitude);
    }
}

public class PostcodeResponse
{
    public string status;
}

public class PostcodeRecord
{
    public string postcode;
    public string quality;
    public string eastings;
    public string northings;
    public string country;
    public string nhs_ha;
    public string longitude;
    public string latitude;
    public string parliamentary_constituency;
    public string european_electoral_region;
    public string primary_care_trust;
    public string region;
    public string lsoa;
    public string msoa;
    public string incode;
    public string outcode;
    public string distance;
    public string admin_district;
    public string parish;
    public string admin_county;
    public string admin_ward;
    public string ccg;
    public string nuts;
    public string codes;
}

