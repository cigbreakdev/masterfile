﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance;
    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SoundManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(SoundManager).Name;
                    instance = obj.AddComponent<SoundManager>();
                }
            }
            return instance;
        }
    }

    public AudioClip main;
    public AudioClip amb;
    public AudioClip aux;

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("SoundManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        Init();
    }

    public AudioSource mainMusicAudioSource;
    public AudioSource ambienceAudioSource;
    public AudioSource auxMusicAudioSource;
    public AudioSource eventMusicAudioSource;

    public AudioMixerSnapshot ambInSnapshot;
    public AudioMixerSnapshot ambOutSnapshot;
    public AudioMixerSnapshot auxInSnapshot;
    public AudioMixerSnapshot auxOutSnapshot;
    public AudioMixerSnapshot eventSnapshot;
    public AudioMixerSnapshot idle;

    // Use this for initialization
    public void Init()
    {
        Debug.Log("SoundManager - Init() called.");
        //
        mainMusicAudioSource.clip = main;
        auxMusicAudioSource.clip = aux;
        mainMusicAudioSource.Play();
        auxMusicAudioSource.Play();
    }

    public IEnumerator PlayEventMusic(AudioClip clip)
    {
        eventSnapshot.TransitionTo(0.05f);
        yield return new WaitForSeconds(0.2f);
        eventMusicAudioSource.clip = clip;
        eventMusicAudioSource.Play();
        while (eventMusicAudioSource.isPlaying)
        {
            yield return null;
        }
        idle.TransitionTo(0.5f);
        yield break;
    }

    public IEnumerator PlayEventMusicThenStop(AudioClip clip)
    {
        eventSnapshot.TransitionTo(0.05f);
        yield return new WaitForSeconds(0.2f);

        mainMusicAudioSource.Stop();
        auxMusicAudioSource.Stop();

        eventMusicAudioSource.clip = clip;
        eventMusicAudioSource.Play();
        while (eventMusicAudioSource.isPlaying)
        {
            yield return null;
        }
        idle.TransitionTo(0.5f);
        yield break;
    }

    public IEnumerator PlayEventMusicThenRestart(AudioClip clip)
    {
        eventSnapshot.TransitionTo(0.05f);
        yield return new WaitForSeconds(0.2f);

        mainMusicAudioSource.Stop();
        auxMusicAudioSource.Stop();

        eventMusicAudioSource.clip = clip;
        eventMusicAudioSource.Play();
        while (eventMusicAudioSource.isPlaying)
        {
            yield return null;
        }
        idle.TransitionTo(0.5f);
        yield return new WaitForSeconds(0.2f);

        mainMusicAudioSource.Play();
        auxMusicAudioSource.Play();

        yield break;
    }

    public IEnumerator StopMainMusic()
    {
        yield return new WaitForSeconds(0.2f);

        mainMusicAudioSource.Stop();
        auxMusicAudioSource.Stop();

        yield break;
    }

    public IEnumerator RestartMainMusic()
    {
        yield return new WaitForSeconds(0.2f);

        mainMusicAudioSource.Play();
        auxMusicAudioSource.Play();

        yield break;
    }
}
