﻿using SQLite4Unity3d;
using UnityEngine;
using System;

#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

// Based on https://github.com/codecoding/SQLite4Unity3d
public class DatabaseService
{
	private string _databaseName = "CigbreakDB.db";

	public string DatabaseName { get { return _databaseName; } }

	private string _dbPath = null;

	public string DatabaseFilePath { get { return _dbPath; } }

	private SQLiteConnection _connection;

	public class ValIntReturn
	{
		public int returnIntValue { get; set; }
	}

	public class ValStringReturn
	{
		public string returnStringValue { get; set; }
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DatabaseService"/> class.
	/// </summary>
	public DatabaseService ()
	{
		#if UNITY_EDITOR
		var dbPath = string.Format (@"Assets/StreamingAssets/{0}", DatabaseName);
		#else
		// check if file exists in Application.persistentDataPath
		var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

		if (!File.Exists(filepath))
		{
		Debug.Log("Database not in Persistent path");
		// if it doesn't ->
		// open StreamingAssets directory and load the db ->

		#if UNITY_ANDROID 
		var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
		while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
		// then save to Application.persistentDataPath
		File.WriteAllBytes(filepath, loadDb.bytes);
		#elif UNITY_IOS
		var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#elif UNITY_WP8
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#else
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#endif

		Debug.Log("Database written");
		}

		var dbPath = filepath;
		#endif

		_connection = new SQLiteConnection (dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
		_dbPath = dbPath;
		Debug.Log ("(Default mode) Final PATH: " + dbPath);
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DatabaseService"/> class.
	/// </summary>
	public DatabaseService (bool forceOverwrite)
	{
		#if UNITY_EDITOR
		var dbPath = string.Format (@"Assets/StreamingAssets/{0}", DatabaseName);
		#else
		// check if file exists in Application.persistentDataPath
		var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

		if(forceOverwrite && File.Exists(filepath))
		{
		File.Delete(filepath);
		}

		if (!File.Exists(filepath))
		{
		Debug.Log("Database not in Persistent path");
		// if it doesn't ->
		// open StreamingAssets directory and load the db ->

		#if UNITY_ANDROID 
		var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
		while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
		// then save to Application.persistentDataPath
		File.WriteAllBytes(filepath, loadDb.bytes);
		#elif UNITY_IOS
		var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#elif UNITY_WP8
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#else
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		#endif

		Debug.Log("Database written");
		}

		var dbPath = filepath;
		#endif

		_connection = new SQLiteConnection (dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
		_dbPath = dbPath;
		Debug.Log ("(Overwritten) Final PATH: " + dbPath);
	}

	/// <summary>
	/// Is the table exist.
	/// </summary>
	/// <returns><c>true</c>, if table exist was ised, <c>false</c> otherwise.</returns>
	/// <param name="TableName">Table name.</param>
	public bool isTableExist (string TableName)
	{
		bool returnFlag = false;
		List<ValStringReturn> dataset = new List<ValStringReturn> ();
		dataset = _connection.Query<ValStringReturn> ("SELECT name as returnStringValue FROM sqlite_master WHERE type='table' AND name=?", TableName);

		foreach (ValStringReturn row in dataset) {
			if (row.returnStringValue == TableName) {
				returnFlag = true;
			} else {
				returnFlag = false;
			}
		}
		return returnFlag;
	}

	/// <summary>
	/// Return table's row count.
	/// </summary>
	/// <returns>The count.</returns>
	/// <param name="TableName">Table name.</param>
	public int RowsCount (string TableName)
	{
		List<ValIntReturn> dataset = new List<ValIntReturn> ();
		dataset = _connection.Query<ValIntReturn> ("SELECT count(*) as returnIntValue FROM " + TableName);
		return dataset [0].returnIntValue;
	}

	public bool GetLastestDatabaseVersion (out string currentVersionOnDevice, out DateTime currentReleaseDateOnDevice)
	{
		bool returnFlag = false;
		if (isTableExist ("VersionControl")) {
			VersionControl record = new VersionControl ();
			record = VersionControl_LoadLastestVersion ();
			currentVersionOnDevice = record.Version;
			currentReleaseDateOnDevice = Convert.ToDateTime (record.Release_Date);
			returnFlag = true;
		} else {
			currentVersionOnDevice = "0.0.0";
			currentReleaseDateOnDevice = Convert.ToDateTime ("01/01/2017 00:00:00 AM");
			returnFlag = false;
		}

		return returnFlag;
	}

	public VersionControl VersionControl_LoadLastestVersion ()
	{
		int recordCount = _connection.Table<VersionControl> ().OrderByDescending (x => x.ID).Count ();
		if (recordCount == 0) {
			VersionControl record = new VersionControl ();
			record.Version = "0.0.0";
			record.Release_Date = "01/01/2017 00:00:00 AM";
			return record;
		} else {
			return _connection.Table<VersionControl> ().OrderByDescending (x => x.ID).First ();
		}
	}

	/// <summary>
	/// Locations the tracking insert.
	/// </summary>
	/// <returns>The tracking insert.</returns>
	/// <param name="deviceInfo">Device Unique Identifier.</param>
	/// <param name="latData">Latitude data.</param>
	/// <param name="longData">Longitude data.</param>
	/// <param name="curDateTime">Current date time.</param>
	/// <param name="comment">Comment can be "OPEN GAME", "CLOSE GAME", "LEVEL 1", etc.</param>
	/// <param name="needUpload">Need upload.</param>
	public int LocationTracking_Insert (string deviceInfo, float latData, float longData, string curDateTime, string comment = null, int needUpload = 0)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		var record = new LocationTracking {
			Device_Info = deviceInfo,
			Latitude = latData,
			Longitude = longData,
			Record_Date = curDateTime,
			Need_Upload = needUpload
		};

		if (comment != null) {
			record.Comment = comment;
		}

		_connection.Insert (record);
		record = _connection.Table<LocationTracking> ().Where (x => x.Record_Date == curDateTime).FirstOrDefault ();
		return record.ID;
	}

	/// <summary>
	/// Count offline data in local database storage.
	/// </summary>
	/// <returns>How many offline data need to upload.</returns>
	public int LocationTracking_NeedUpload_RecordCount (string deviceInfo)
	{
		int recordCount = _connection.Table<LocationTracking> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0).Count ();

		return recordCount;
	}

	/// <summary>
	/// Load the location tracking that need to upload.
	/// </summary>
	/// <returns>The offline location tracking data need to upload.</returns>
	public TableQuery<LocationTracking> LocationTracking_NeedUpload (string deviceInfo)
	{
		return _connection.Table<LocationTracking> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0);
	}

	/// <summary>
	/// Locations the tracking offline update.
	/// </summary>
	/// <param name="rowID">Record ID.</param>
	public void LocationTracking_OfflineUpdate (int rowID)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		// When start from the other scences (not FirstScene) in Unity Editor, locationTrackingID's value equal -1. 
		if (rowID > 0) {
			var record = new LocationTracking ();
			record = _connection.Table<LocationTracking> ().Where (x => x.ID == rowID).FirstOrDefault ();
			record.Need_Upload = 0;
			_connection.Update (record);
		}
	}

	/// <summary>
	/// Levels the data load level.
	/// </summary>
	/// <returns>The data load level.</returns>
	/// <param name="LevelID">Level I.</param>
	public LevelData LevelData_LoadLevel (int LevelID)
	{
		return _connection.Table<LevelData> ().Where (x => x.Level_ID == LevelID).First ();
	}

	public bool LevelData_HasLevelDataInDatabase (int LevelID)
	{
		int recordCount = _connection.Table<LevelData> ().Where (x => x.Level_ID == LevelID).Count ();

		if (recordCount > 0) {
			return true;
		} else {
			return false;
		}
	}

	public int LevelPlayed_Insert (LevelPlayed record)
	{
        return -1;
        /*
        int levelID = record.Level_ID;
		string startTime = record.Level_StartTime;
		string endTime = record.Level_EndTime;

		if (levelID > 0 && startTime != "" && endTime != "") {
			_connection.Insert (record);
			record = _connection.Table<LevelPlayed> ().Where (x => x.Level_ID == levelID && x.Level_StartTime == startTime && x.Level_EndTime == endTime).FirstOrDefault ();
			return record.ID;
		} else {
			return -1;
		}
        */
	}

	/// <summary>
	/// Get max result at specificed level.
	/// Add DeviceInfo to prevent load information from the other device.
	/// </summary>
	/// <returns>The max result.</returns>
	/// <param name="LevelID">Level I.</param>
	public int LevelPlayed_GetMaxResultAt (string deviceInfo, int LevelID)
	{
        return -1;
        /*
        int recordCount = _connection.Table<LevelPlayed> ().Where (x => x.Device_Info == deviceInfo && x.Level_ID == LevelID).OrderByDescending (x => x.Result).Count ();
		if (recordCount == 0) {
			return -1;
		}

		var record = new LevelPlayed ();
		record = _connection.Table<LevelPlayed> ().Where (x => x.Device_Info == deviceInfo && x.Level_ID == LevelID).OrderByDescending (x => x.Result).First ();
		return record.Result;
        */
	}

	public bool LevelPlayed_IsPlayedBefore (string deviceInfo, int LevelID)
	{
        return false;
        /*
        int recordCount = _connection.Table<LevelPlayed> ().Where (x => x.Device_Info == deviceInfo && x.Level_ID == LevelID).OrderByDescending (x => x.Result).Count ();
		if (recordCount > 0) {
			return true;
		} else {
			return false;
		}
        */
	}

	public float LevelPlayed_TotalPlayedTime (string deviceInfo)
	{
		float diffTime = 0.0f;
		DateTime startTime, endTime;
		TimeSpan span;
		int ms = 0, ms_total = 0;
        /*
		TableQuery<LevelPlayed> list = _connection.Table<LevelPlayed> ().Where (x => x.Device_Info == deviceInfo);
		foreach (LevelPlayed row in list) {
			startTime = Convert.ToDateTime (row.Level_StartTime);
			endTime = Convert.ToDateTime (row.Level_EndTime);
			span = endTime - startTime;
			ms = (int)span.TotalMilliseconds;
			ms_total += ms;
		}
		// Convert from milliseconds into day.
		diffTime = (float)ms_total / (24.0f * 60.0f * 60.0f * 1000.0f);
        */
		return diffTime;
	}

	/// <summary>
	/// Load the records that need to upload.
	/// </summary>
	/// <returns>The offline record data need to upload.</returns>
	public TableQuery<LevelPlayed> LevelPlayed_NeedUpload (string deviceInfo)
	{
        return _connection.Table<LevelPlayed>();
        //.Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0);
	}

	/// <summary>
	/// Count offline data in local database storage.
	/// </summary>
	/// <returns>How many offline data need to upload.</returns>
	public int LevelPlayed_NeedUpload_RecordCount (string deviceInfo)
	{
        return -1;
        /*
		int recordCount = _connection.Table<LevelPlayed> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0).Count ();

		return recordCount;
        */
	}

	/// <summary>
	/// Load level's reward from database.
	/// </summary>
	/// <returns>The data of level's reward.</returns>
	/// <param name="LevelID">Level I.</param>
	public LevelReward LevelReward_LoadReward (int LevelID)
	{
		int recordCount = _connection.Table<LevelReward> ().Where (x => x.Level_ID == LevelID).Count ();
		if (recordCount == 0) {
			var reward = new LevelReward ();
			return reward;
		}

		return _connection.Table<LevelReward> ().Where (x => x.Level_ID == LevelID).First ();
	}

	/// <summary>
	/// Load player profile from database.
	/// </summary>
	/// <returns>The player profile.</returns>
	/// <param name="deviceInfo">Device info.</param>
	/// <param name="needUpload">If device is online then needUpload = 0 else needUpload = 1.</param>
	public Profile PlayerProfile_LoadProfile (string deviceInfo, int needUpload = 0)
	{
		Profile record = new Profile ();
        return record;
        /*
		DateTime curDateTime = DateTime.Now;
		int dateDiff = 0;
		// PlayerProfile_CheckProfile return true if it found profile data on this device.
		if (PlayerProfile_CheckProfile (deviceInfo)) {
			//
			record = _connection.Table<PlayerProfile> ().Where (x => x.Device_Info == deviceInfo).OrderBy (x => x.ID).First ();
			// Calculate consecutive login
			DateTime lastLoginDate = Convert.ToDateTime (record.Last_Login_Date);
			dateDiff = (curDateTime.Date - lastLoginDate.Date).Days;
			// if dateDiff == 0 mean the same day then do nothing.
			if (dateDiff == 1) {
				record.Consecutive_Login = record.Consecutive_Login + 1;
				record.Total_Login_Days = record.Total_Login_Days + 1;
				record.First_Time_Of_Day = 1;
			} else if (dateDiff > 1) {
				record.Consecutive_Login = 1;
				record.Total_Login_Days = record.Total_Login_Days + 1;
				record.First_Time_Of_Day = 1;
			} else {
				record.First_Time_Of_Day = 0;
			}
			// Update login count.
			record.Total_Login_Counts = record.Total_Login_Counts + 1;
			// Update last login date.
			record.Last_Login_Date = curDateTime.ToString ();
			// Update status.
			record.Need_Upload = needUpload;
			_connection.Update (record);
		} else {
			record = PlayerProfile_Insert (deviceInfo, curDateTime.ToString (), needUpload);
		}

		return record;
        */
	}

	private bool PlayerProfile_CheckProfile (string deviceInfo)
	{
        return false;
        /*
		int recordCount = _connection.Table<Profile> ().Where (x => x.Device_Info == deviceInfo).Count ();
		// Found profile for this device.
		if (recordCount == 1) {
			return true;
		} else {
			return false;
		}
        */
	}

	private Profile PlayerProfile_Insert (string deviceInfo, string curDateTime, int needUpload = 0)
	{
        Profile record = new Profile();
        return record;
        /*
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		var record = new PlayerProfile {
			Device_Info = deviceInfo,
			Start_Date = curDateTime,
			Last_Login_Date = curDateTime,
			First_Time_Of_Day = 1,
			Total_Login_Days = 1,
			Total_Login_Counts = 1,
			Consecutive_Login = 1,
			Need_Upload = needUpload
		};

		_connection.Insert (record);
		record = _connection.Table<PlayerProfile> ().Where (x => x.Device_Info == deviceInfo && x.Start_Date == curDateTime).FirstOrDefault ();
		return record;
        */
	}

	public void PlayerProfile_Update (Profile profile)
	{
		_connection.Update (profile);
	}

	/// <summary>
	/// Count offline data in local database storage.
	/// </summary>
	/// <returns>How many offline data need to upload.</returns>
	public int PlayerProfile_NeedUpload_RecordCount (string deviceInfo)
	{
        return -1;
        /*
		int recordCount = _connection.Table<Profile> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0).Count ();

		return recordCount;
        */
	}

	/// <summary>
	/// Load the player profile that need to upload.
	/// </summary>
	/// <returns>The offline player profile data need to upload.</returns>
	public TableQuery<Profile> PlayerProfile_NeedUpload (string deviceInfo)
	{
        return _connection.Table<Profile>();
        //.Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0);
	}

	public QuitMission QuitMission_AtDay (int gameDay)
	{
		return _connection.Table<QuitMission> ().Where (x => x.GameDay == gameDay).First ();
	}

	public StatsData StatsData_At (int index)
	{
		return _connection.Table<StatsData> ().Where (x => x.ID == index).First ();
	}

	/// <summary>
	/// Insert readiness into database.
	/// </summary>
	/// <returns>Inserted row id.</returns>
	/// <param name="deviceInfo">Device Unique Identifier.</param>
	/// <param name="rate">Readiness Rate.</param>
	/// <param name="firstCigsAfterWakeup">How long after waking do you have your first cigarette?</param>
	/// <param name="cigsPerDay">How many cigarettes you smoked per day?</param>
	/// <param name="isNonSmoker">Are you non smoker.</param>
	/// <param name="answerDate">Answer date.</param>
	/// <param name="needUpload">Need upload.</param>
	public int Readiness_Insert (string deviceInfo, float rate, float firstCigsAfterWakeup, int cigsPerDay, int isNonSmoker, string answerDate, int needUpload = 0)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		var record = new Readiness {
			Device_Info = deviceInfo,
			Readiness_Rate = rate,
			First_Cigs_After_Wakeup = firstCigsAfterWakeup,
			Cigs_Per_Day = cigsPerDay,
			Non_Smoker = isNonSmoker,
			Answer_Date = answerDate,
			Need_Upload = needUpload
		};

		_connection.Insert (record);
		record = _connection.Table<Readiness> ().Where (x => x.Answer_Date == answerDate).FirstOrDefault ();
		return record.ID;
	}

	/// <summary>
	/// Count offline data in local database storage.
	/// </summary>
	/// <returns>How many offline data need to upload.</returns>
	public int Readiness_NeedUpload_RecordCount (string deviceInfo)
	{
		int recordCount = _connection.Table<Readiness> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0).Count ();

		return recordCount;
	}

	/// <summary>
	/// Load the readiness data that need to upload.
	/// </summary>
	/// <returns>The offline readiness data need to upload.</returns>
	public TableQuery<Readiness> Readiness_NeedUpload (string deviceInfo)
	{
		return _connection.Table<Readiness> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0);
	}

	/// <summary>
	/// Readiness offline update.
	/// </summary>
	/// <param name="rowID">Record ID.</param>
	public void Readiness_OfflineUpdate (int rowID)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		if (rowID > 0) {
			var record = new Readiness ();
			record = _connection.Table<Readiness> ().Where (x => x.ID == rowID).FirstOrDefault ();
			record.Need_Upload = 0;
			_connection.Update (record);
		}
	}

	public int Readiness_CigsPerDay (string deviceInfo)
	{
		DateTime curDateTime = DateTime.Now;
		Readiness record = new Readiness ();

		int recordCount = _connection.Table<Readiness> ().Where (x => x.Device_Info == deviceInfo).OrderByDescending (x => x.ID).Count ();
		// Found.
		if (recordCount == 1) {
			record = _connection.Table<Readiness> ().Where (x => x.Device_Info == deviceInfo).OrderByDescending (x => x.ID).First ();
		} else {
			record.Cigs_Per_Day = 0;
		}
		return record.Cigs_Per_Day;
	}


	/// <summary>
	/// Insert onboarding answer into database.
	/// </summary>
	/// <returns>Inserted row id.</returns>
	/// <param name="deviceInfo">Device Unique Identifier.</param>
	/// <param name="firstCigsAfterWakeup">How long after waking do you have your first cigarette?</param>
	/// <param name="cigsPerYesterday">How many cigarettes you smoked on yesterday?</param>
	/// <param name="triggeredSmoke">Triggered smoke answer.</param>
	/// <param name="relapseFactor">Relapse factor answer.</param>
	/// <param name="answerDate">Answer date.</param>
	/// <param name="needUpload">Need upload.</param>
	public int OnboardingAnswer_Insert (string deviceInfo, float firstCigsAfterWakeup, int cigsPerYesterday, string triggeredSmoke, string relapseFactor, string answerDate, int needUpload = 0)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		var record = new OnboardingAnswer {
			Device_Info = deviceInfo,
			First_Cigs_After_Wakeup = firstCigsAfterWakeup,
			Cigs_On_Yesterday = cigsPerYesterday,
			Triggered_Smoke = triggeredSmoke,
			Relapse_Factor = relapseFactor,
			Answer_Date = answerDate,
			Need_Upload = needUpload
		};

		_connection.Insert (record);
		record = _connection.Table<OnboardingAnswer> ().Where (x => x.Answer_Date == answerDate).FirstOrDefault ();
		return record.ID;
	}

	/// <summary>
	/// Count offline data in local database storage.
	/// </summary>
	/// <returns>How many offline data need to upload.</returns>
	public int OnboardingAnswer_NeedUpload_RecordCount (string deviceInfo)
	{
		int recordCount = _connection.Table<OnboardingAnswer> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0).Count ();

		return recordCount;
	}

	/// <summary>
	/// Load the onboarding answer that need to upload.
	/// </summary>
	/// <returns>The offline onboarding answer data need to upload.</returns>
	public TableQuery<OnboardingAnswer> OnboardingAnswer_NeedUpload (string deviceInfo)
	{
		return _connection.Table<OnboardingAnswer> ().Where (x => x.Device_Info == deviceInfo && x.Need_Upload > 0);
	}

	/// <summary>
	/// OnboardingAnswer offline update.
	/// </summary>
	/// <param name="rowID">Record ID.</param>
	public void OnboardingAnswer_OfflineUpdate (int rowID)
	{
		// needUpload = 0 mean this record is already insert into online database server. if not, needUpload = 1
		if (rowID > 0) {
			var record = new OnboardingAnswer ();
			record = _connection.Table<OnboardingAnswer> ().Where (x => x.ID == rowID).FirstOrDefault ();
			record.Need_Upload = 0;
			_connection.Update (record);
		}
	}

	public OnboardingAnswer OnboardingAnswer_GetLastAnswer (string deviceInfo)
	{
		DateTime curDateTime = DateTime.Now;
		OnboardingAnswer record = new OnboardingAnswer ();
		List<OnboardingAnswer> dataset = new List<OnboardingAnswer> ();
		dataset = _connection.Query<OnboardingAnswer> ("SELECT ID, Device_Info, First_Cigs_After_Wakeup, Cigs_On_Yesterday, Triggered_Smoke, Relapse_Factor, Answer_Date, Need_Upload FROM OnboardingAnswer WHERE Device_Info = '" + deviceInfo + "' AND Answer_Date < '" + curDateTime.ToString () + "' ORDER BY Answer_Date DESC");
		// Found data.
		if (dataset.Count > 0) {
			record = dataset [0];
		} else {
			record.ID = 0;
		}

		return record;
	}
}
