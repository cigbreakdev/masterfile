﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SQLite4Unity3d;
using System;
using System.IO;

public class SQLiteManager : MonoBehaviour
{
    private static SQLiteManager instance;
    public static SQLiteManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SQLiteManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(SQLiteManager).Name;
                    instance = obj.AddComponent<SQLiteManager>();
                }
            }
            return instance;
        }
    }

    [SerializeField]
    private string m_databaseName = "CigbreakDB.db";
    [SerializeField]
    private string m_databasePath;
    private SQLiteConnection m_connection;

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("SQLiteManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    public void Init()
    {
        Debug.Log("SQLiteManager - Init() called.");
        //
#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", m_databaseName);
#else
		// check if file exists in Application.persistentDataPath
		var filepath = string.Format("{0}/{1}", Application.persistentDataPath, m_databaseName);
		if (!File.Exists(filepath))
		{
		    // if it doesn't ->
		    // open StreamingAssets directory and load the db ->
#if UNITY_ANDROID
		    var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + m_databaseName);  
            // this is the path to your StreamingAssets in android
		    while (!loadDb.isDone) { }  
            // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
		    // then save to Application.persistentDataPath
		    File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
		    var loadDb = Application.dataPath + "/Raw/" + m_databaseName;  
            // this is the path to your StreamingAssets in iOS
		    // then save to Application.persistentDataPath
		    File.Copy(loadDb, filepath);
#elif UNITY_WP8
		    var loadDb = Application.dataPath + "/StreamingAssets/" + m_databaseName;  
            // this is the path to your StreamingAssets in iOS
		    // then save to Application.persistentDataPath
		    File.Copy(loadDb, filepath);
#elif UNITY_WINRT
		    var loadDb = Application.dataPath + "/StreamingAssets/" + m_databaseName;  
            // this is the path to your StreamingAssets in iOS
		    // then save to Application.persistentDataPath
		    File.Copy(loadDb, filepath);
#else
		    var loadDb = Application.dataPath + "/StreamingAssets/" + m_databaseName;  
            // this is the path to your StreamingAssets in iOS
		    // then save to Application.persistentDataPath
		    File.Copy(loadDb, filepath);
#endif
		}
		var dbPath = filepath;
#endif
        m_connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        m_databasePath = dbPath;

        //
        Debug.Log("SQLiteManager:" + m_databasePath);
    }

    public class ValIntReturn
    {
        public int returnIntValue { get; set; }
    }

    public class ValStringReturn
    {
        public string returnStringValue { get; set; }
    }

    /// <summary>
    /// Is the table exist.
    /// </summary>
    /// <returns><c>true</c>, if table exist, <c>false</c> otherwise.</returns>
    /// <param name="TableName">Table name.</param>
    public bool IsTableExist(string TableName)
    {
        bool returnFlag = false;
        List<ValStringReturn> dataset = new List<ValStringReturn>();
        dataset = m_connection.Query<ValStringReturn>("SELECT name as returnStringValue FROM sqlite_master WHERE type='table' AND name=?", TableName);

        foreach (ValStringReturn row in dataset)
        {
            if (row.returnStringValue == TableName)
            {
                returnFlag = true;
            }
            else
            {
                returnFlag = false;
            }
        }
        return returnFlag;
    }

    /// <summary>
    /// Return table's row count.
    /// </summary>
    /// <returns>The count.</returns>
    /// <param name="TableName">Table name.</param>
    public int RowsCount(string TableName)
    {
        if (IsTableExist(TableName))
        {
            List<ValIntReturn> dataset = new List<ValIntReturn>();
            dataset = m_connection.Query<ValIntReturn>("SELECT count(*) as returnIntValue FROM " + TableName);
            return dataset[0].returnIntValue;
        }

        return 0;
    }

    public bool IsDeviceExist(string deviceInfo)
    {
        //
        int recordCount = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == deviceInfo).Count();

        if (recordCount == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int Device_Insert(MobileDevice record)
    {
        if (IsDeviceExist(record.DeviceInfo))
        {
            Debug.Log("Device_Insert :: Found current device's information in SQLite database. ");
            //
            return -1;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == record.DeviceInfo && x.CreateDate == record.CreateDate).FirstOrDefault();

            Debug.Log("Device_Insert :: Insert completed. ");
            //
            return record.DeviceID;
        }
    }

    public MobileDevice LoadDevice(string deviceInfo)
    {
        return m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == deviceInfo).FirstOrDefault();
    }

    public bool IsPlayerExist(string playerName)
    {
        //
        int recordCount = m_connection.Table<Player>().Where(x => x.PlayerName == playerName).Count();

        if (recordCount == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int Player_Insert(Player record)
    {
        if (IsPlayerExist(record.PlayerName))
        {
            Debug.Log("Player_Insert :: Found current player's information in SQLite database. ");
            //
            return -1;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<Player>().Where(x => x.PlayerName == record.PlayerName && x.CreateDate == record.CreateDate).FirstOrDefault();

            Debug.Log("Player_Insert :: Insert completed. ");
            //
            return record.PlayerID;
        }
    }

    public Player LoadPlayer(string playerName)
    {
        return m_connection.Table<Player>().Where(x => x.PlayerName == playerName).FirstOrDefault();
    }

    public bool IsProfileExist(int deviceID, int playerID)
    {
        //
        int recordCount = m_connection.Table<Profile>().Where(x => x.DeviceID == deviceID && x.PlayerID == playerID).Count();

        if (recordCount == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public int Profile_Insert(Profile record)
    {
        if (IsProfileExist(record.DeviceID, record.PlayerID))
        {
            Debug.Log("Profile_Insert :: Found current profile's information in SQLite database. ");
            //
            return -1;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<Profile>().Where(x => x.DeviceID == record.DeviceID && x.PlayerID == record.PlayerID && x.CreateDate == record.CreateDate).FirstOrDefault();

            Debug.Log("Profile_Insert :: Insert completed. ");
            //
            return record.ProfileID;
        }
    }

    public void Profile_Update(Profile record)
    {
        m_connection.Update(record);
        //
        Debug.Log("Profile_Update :: Update completed. ");
    }

    public Profile LoadProfile(int deviceID, int playerID)
    {
        return m_connection.Table<Profile>().Where(x => x.DeviceID == deviceID && x.PlayerID == playerID).FirstOrDefault();
    }

    public DailyAnswer GetLastDailyAnswer(int profileID)
    {
        //
        int recordCount = m_connection.Table<DailyAnswer>().Where(x => x.ProfileID == profileID).Count();
        DailyAnswer record = new DailyAnswer();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<DailyAnswer>().Where(x => x.ProfileID == profileID).OrderByDescending(x => x.JournalDay).First();
            return record;
        }
    }

    public void DailyAnswer_Insert(DailyAnswer record)
    {
        m_connection.Insert(record);
        //
        Debug.Log("DailyAnswer_Insert :: Insert completed. ");
    }

    /// <summary>
    /// Levels the data load level.
    /// </summary>
    /// <returns>The data load level.</returns>
    /// <param name="LevelID">Level I.</param>
    public LevelData LoadLevel(int LevelID)
    {
        return m_connection.Table<LevelData>().Where(x => x.Level_ID == LevelID).First();
    }

    /// <summary>
    /// Get max result at specificed level.
    /// Add ProfileID to prevent load information from the other device.
    /// </summary>
    /// <returns>The max result.</returns>
    /// <param name="ProfileID">Profile ID</param>
    /// <param name="LevelIndex">Level Index</param>
    public int GetPastResult(int profileID, int levelIndex)
    {
        int recordCount = m_connection.Table<LevelPlayed>().Where(x => x.ProfileID == profileID && x.Level == levelIndex).OrderByDescending(x => x.Result).Count();
        if (recordCount == 0)
        {
            return -1;
        }

        var record = new LevelPlayed();
        record = m_connection.Table<LevelPlayed>().Where(x => x.ProfileID == profileID && x.Level == levelIndex).OrderByDescending(x => x.Result).First();
        return record.Result;
    }

    public void LevelPlayed_Insert(LevelPlayed record)
    {
        m_connection.Insert(record);
        //
        Debug.Log("LevelPlayed_Insert :: Insert completed. ");
    }

    public LevelReward LoadReward(int LevelID)
    {
        return m_connection.Table<LevelReward>().Where(x => x.Level_ID == LevelID).First();
    }

    public MyHealth GetMyHealthItem(int ID)
    {
        //
        int recordCount = m_connection.Table<MyHealth>().Where(x => x.ID == ID).Count();
        MyHealth record = new MyHealth();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<MyHealth>().Where(x => x.ID == ID).First();
            return record;
        }
    }

    public StatsData GetMyStatsItem(int ID)
    {
        //
        int recordCount = m_connection.Table<StatsData>().Where(x => x.ID == ID).Count();
        StatsData record = new StatsData();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<StatsData>().Where(x => x.ID == ID).First();
            return record;
        }
    }

    public QuitMission GetMyMissionItem(int ID)
    {
        //
        int recordCount = m_connection.Table<QuitMission>().Where(x => x.ID == ID).Count();
        QuitMission record = new QuitMission();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<QuitMission>().Where(x => x.ID == ID).First();
            return record;
        }
    }

    public TriggerChoice GetTriggerChoiceItem(int ID)
    {
        //
        int recordCount = m_connection.Table<TriggerChoice>().Where(x => x.ID == ID).Count();
        TriggerChoice record = new TriggerChoice();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<TriggerChoice>().Where(x => x.ID == ID).First();
            return record;
        }
    }

    public RelapseChoice GetRelapseChoiceItem(int ID)
    {
        //
        int recordCount = m_connection.Table<RelapseChoice>().Where(x => x.ID == ID).Count();
        RelapseChoice record = new RelapseChoice();

        if (recordCount == 0)
        {
            record.ID = -1;
            return record;
        }
        else
        {
            record = m_connection.Table<RelapseChoice>().Where(x => x.ID == ID).First();
            return record;
        }
    }










    public void ClearAllData()
    {
        m_connection.DeleteAll<MobileDevice>();
        m_connection.DeleteAll<Player>();
        m_connection.DeleteAll<Profile>();
        m_connection.DeleteAll<DailyAnswer>();
        m_connection.DeleteAll<LevelPlayed>();
    }









    /*
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    */



    public void LoadDevice(ref MobileDevice device)
    {
        string deviceInfo = device.DeviceInfo;
        if (IsDeviceExist(deviceInfo))
        {
            // Found device data.
            device = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == deviceInfo).FirstOrDefault();
        }
        else
        {
            // No Data.
            DateTime curDateTime = DateTime.Now;
            string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
            device.CreateDate = mySQLTime;
            device.Status = 1;
            // Insert new data.
            m_connection.Insert(device);
            // Refresh data.
            device = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == deviceInfo).FirstOrDefault();
            //
            Debug.Log("Insert new device data into SQLite.");
        }
    }

    public void UpdateOnlineDeviceID(int onlineDeviceID, string createDate, int status)
    {
        // Online process is succeed.
        if (status == 0)
        {
            GameManager.Instance.CurrentDevice.OnlineDeviceID = onlineDeviceID;
            if (createDate != "")
            {
                GameManager.Instance.CurrentDevice.CreateDate = createDate;
            }
            GameManager.Instance.CurrentDevice.Status = 0;
            // update data.
            m_connection.Update(GameManager.Instance.CurrentDevice);
            //
            Debug.Log("Update OnlineDeviceID complete.");
        }
        else
        {
            GameManager.Instance.CurrentDevice.Status += status;
            // update data.
            m_connection.Update(GameManager.Instance.CurrentDevice);
        }
    }

    public void LoadPlayer(ref Player player)
    {
        string playerName = player.PlayerName;
        if (IsPlayerExist(playerName))
        {
            // Found player data.
            player = m_connection.Table<Player>().Where(x => x.PlayerName == playerName).FirstOrDefault();
        }
        else
        {
            // No Data.
            DateTime curDateTime = DateTime.Now;
            string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
            player.CreateDate = mySQLTime;
            player.Status = 1;
            // Insert new data.
            m_connection.Insert(player);
            // Refresh data.
            player = m_connection.Table<Player>().Where(x => x.PlayerName == playerName).FirstOrDefault();
            //
            Debug.Log("Insert new player data into SQLite.");
        }
    }

    public void UpdateOnlinePlayerID(int onlinePlayerID, string createDate, int status)
    {
        // Online process is succeed.
        if (status == 0)
        {
            GameManager.Instance.CurrentPlayer.OnlinePlayerID = onlinePlayerID;
            if (createDate != "")
            {
                GameManager.Instance.CurrentPlayer.CreateDate = createDate;
            }
            GameManager.Instance.CurrentPlayer.Status = 0;
            // update data.
            m_connection.Update(GameManager.Instance.CurrentPlayer);
            //
            Debug.Log("Update OnlinePlayerID complete.");
        }
        else
        {
            GameManager.Instance.CurrentPlayer.Status += status;
            // update data.
            m_connection.Update(GameManager.Instance.CurrentPlayer);
        }
    }

    public void LoadProfile(ref Profile profile, int deviceID, int playerID)
    {
        if (deviceID == -1 && playerID == -1)
        {
            // No device and player data.
            // Do Nothing.
        }
        else
        {
            if (IsProfileExist(deviceID, playerID))
            {
                // Found profile data.
                profile = m_connection.Table<Profile>().Where(x => x.DeviceID == deviceID && x.PlayerID == playerID).FirstOrDefault();
            }
            else
            {
                // No Data.
                DateTime curDateTime = DateTime.Now;
                string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
                profile.DeviceID = deviceID;
                profile.PlayerID = playerID;
                profile.StartDate = mySQLTime;
                profile.LastLoginDate = mySQLTime;
                profile.FirstTimeOfDay = 1;
                profile.TotalLoginDays = 1;
                profile.TotalLoginCounts = 1;
                profile.ConsecutiveLogin = 1;
                profile.Status = 1;
                // Insert new data.
                m_connection.Insert(profile);
                // Refresh Data
                profile = m_connection.Table<Profile>().Where(x => x.DeviceID == deviceID && x.PlayerID == playerID).FirstOrDefault();
                //
                Debug.Log("Insert new profile data into SQLite.");
            }
        }
    }

    public void UpdateOnlineProfile(Profile onlineRecord, int status)
    {
        switch (status)
        {
            case 0:
                // Online process is succeed.
                GameManager.Instance.CurrentProfile = onlineRecord;
                // update data.
                m_connection.Update(GameManager.Instance.CurrentProfile);
                Debug.Log("Update OnlineProfile complete.");
                break;
            case 1:
                // Online process is failed.
                GameManager.Instance.CurrentProfile.Status += status;
                // update data.
                m_connection.Update(GameManager.Instance.CurrentProfile);
                Debug.Log("Update OnlineProfile fail.");
                break;
            case 2:
                // Online process is succeed but data is duplicate.
                GameManager.Instance.OnlineProfile = onlineRecord;
                Debug.Log("Online process is succeed but data is duplicate..");
                break;
            default:
                break;
        }
    }

    public int CountOfflineData(string TableName)
    {
        if (IsTableExist(TableName))
        {
            List<ValIntReturn> dataset = new List<ValIntReturn>();
            dataset = m_connection.Query<ValIntReturn>("SELECT count(*) as returnIntValue FROM " + TableName + " WHERE Status = 1");
            return dataset[0].returnIntValue;
        }

        return 0;
    }



    public void Device_Insert(MobileDevice record, int status)
    {
        // status = 0 mean this record is already insert into online database server. if not, status = 1
        record.Status = status;

        MobileDevice temp;

        if (IsDeviceExist(record.DeviceInfo))
        {
            temp = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == record.DeviceInfo).FirstOrDefault();
            if (temp.OnlineDeviceID == -1 && status == 0)
            {
                temp.OnlineDeviceID = record.OnlineDeviceID;
                m_connection.Update(temp);
            }
            GameManager.Instance.CurrentDevice = temp;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<MobileDevice>().Where(x => x.DeviceInfo == record.DeviceInfo && x.CreateDate == record.CreateDate).FirstOrDefault();
            GameManager.Instance.CurrentDevice = record;
        }
        Debug.Log("Device_Insert :: Device = " + GameManager.Instance.CurrentDevice.ToString());
    }



    public void Player_Insert(Player record, int status)
    {
        // status = 0 mean this record is already insert into online database server. if not, status = 1
        record.Status = status;

        Player temp;

        if (IsPlayerExist(record.PlayerName))
        {
            temp = m_connection.Table<Player>().Where(x => x.PlayerName == record.PlayerName).FirstOrDefault();
            if (temp.OnlinePlayerID == -1 && status == 0)
            {
                temp.OnlinePlayerID = record.OnlinePlayerID;
                m_connection.Update(temp);
            }
            GameManager.Instance.CurrentPlayer = temp;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<Player>().Where(x => x.PlayerName == record.PlayerName && x.CreateDate == record.CreateDate).FirstOrDefault();
            GameManager.Instance.CurrentPlayer = record;
        }
        Debug.Log("Player_Insert :: Player = " + GameManager.Instance.CurrentPlayer.ToString());
    }



    public void Profile_Create(Profile record, int status)
    {
        // status = 0 mean this record is already insert into online database server. if not, status = 1
        record.Status = status;

        Profile temp;

        if (IsProfileExist(record.DeviceID, record.PlayerID))
        {
            // Profile is already existed.
            temp = m_connection.Table<Profile>().Where(x => x.DeviceID == record.DeviceID && x.PlayerID == record.PlayerID).FirstOrDefault();
            if (temp.OnlineProfileID == -1 && status == 0)
            {
                temp.OnlineProfileID = record.OnlineProfileID;
                m_connection.Update(temp);
            }
            GameManager.Instance.CurrentProfile = temp;
        }
        else
        {
            m_connection.Insert(record);
            record = m_connection.Table<Profile>().Where(x => x.DeviceID == record.DeviceID && x.PlayerID == record.PlayerID).FirstOrDefault();
            GameManager.Instance.CurrentProfile = record;
        }
        Debug.Log("Profile_Create :: Profile = " + GameManager.Instance.CurrentProfile.ToString());
    }



    private bool IsProfileExist(int onlineProfileID, int onlineDeviceID, int onlinePlayerID)
    {
        //
        int recordCount = m_connection.Table<Profile>().Where(x => x.OnlineProfileID == onlineProfileID && x.OnlineDeviceID == onlineDeviceID && x.OnlinePlayerID == onlinePlayerID).Count();

        if (recordCount == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Login_Insert(Login record, int status)
    {
        // status = 0 mean this record is already insert into online database server. if not, status = 1
        record.Status = status;

        m_connection.Insert(record);
        record = m_connection.Table<Login>().Where(x => x.PlayerID == record.PlayerID && x.DeviceID == record.DeviceID && x.CreateDate == record.CreateDate).FirstOrDefault();
        GameManager.Instance.LoginID = record.LoginID;
        Debug.Log("Login_Insert :: LoginID = " + GameManager.Instance.LoginID.ToString());
    }

    public void Login_Update(Login record, int status)
    {
        //
        int recordCount = m_connection.Table<Login>().Where(x => x.LoginID == GameManager.Instance.LoginID).Count();
        Debug.Log("Login_Update :: LoginID = " + GameManager.Instance.LoginID.ToString());
        Debug.Log("Login_Update :: record count = " + recordCount.ToString());
        // status = 0 mean this record is already insert into online database server. if not, status = 1
        Login update_record = new Login();

        if (recordCount == 1)
        {
            //
            update_record = m_connection.Table<Login>().Where(x => x.LoginID == GameManager.Instance.LoginID).FirstOrDefault();
            //
            update_record.EndTime = record.EndTime;
            update_record.EndLat = record.EndLat;
            update_record.EndLong = record.EndLong;
            update_record.CommentE = record.CommentE;
            update_record.UpdateDate = record.UpdateDate;
            update_record.Status = update_record.Status + status;
            //
            Debug.Log("record :: " + record.ToString());
            Debug.Log("update_record :: " + update_record.ToString());
            //
            m_connection.Update(update_record);
        }
        //
        GameManager.Instance.LoginID = -1;
    }

    public void Pause_Insert(Login record)
    {
        m_connection.Insert(record);
        record = m_connection.Table<Login>().Where(x => x.PlayerID == record.PlayerID && x.DeviceID == record.DeviceID && x.CreateDate == record.CreateDate).FirstOrDefault();
        GameManager.Instance.PauseID = record.LoginID;
        Debug.Log("Pause_Insert :: PauseID = " + GameManager.Instance.PauseID);
    }

    public void Pause_Update(Login record)
    {
        //
        int recordCount = m_connection.Table<Login>().Where(x => x.LoginID == record.LoginID).Count();
        Debug.Log("Pause_Update :: PauseID = " + record.LoginID.ToString());
        Debug.Log("Pause_Update :: record count = " + recordCount.ToString());

        Login update_record = new Login();

        if (recordCount == 1)
        {
            //
            update_record = m_connection.Table<Login>().Where(x => x.LoginID == record.LoginID).FirstOrDefault();
            //
            update_record.EndTime = record.EndTime;
            update_record.EndLat = record.EndLat;
            update_record.EndLong = record.EndLong;
            update_record.CommentE = record.CommentE;
            update_record.UpdateDate = record.UpdateDate;
            //
            Debug.Log("record :: " + record.ToString());
            Debug.Log("update_record :: " + update_record.ToString());
            //
            m_connection.Update(update_record);
        }
        //
        GameManager.Instance.PauseID = -1;
    }
}
