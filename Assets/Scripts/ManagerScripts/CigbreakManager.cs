﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System;

//
using UnityEngine.UI;


public class CigbreakManager : MonoBehaviour
{
	//Static instance of CigbreakManager which allows it to be accessed by any other script.
	public static CigbreakManager instance = null;
	//Store a reference to our NetworkManager which will handle device and internet connection.
	private NetworkManager networkScript;

	private SQLiteManager sqliteManager;
	private OnlineDBManager onlineDBManager;
	private LocationManager locationManager;

	[SerializeField]
	private GameObject debugPanel;
	[SerializeField]
	private bool _isDebugBuild;

	public bool IsDebugBuild { get { return _isDebugBuild; } set { _isDebugBuild = value; } }

	private string secretKey = "Cigbreak4";

	private bool _onlineStatus;

	public bool OnlineStatus { get { return _onlineStatus; } set { _onlineStatus = value; } }

	// Calculate from each level that user played. Need to update every time that user finish play each level.
	public float TotalPlayedTimeOnDevice = 0.0f;

	//
	private Player _current_player;

	public Player CurrentPlayer { get { return _current_player; } set { _current_player = value; } }
	//
	private MobileDevice _current_device;

	public MobileDevice CurrentDevice { get { return _current_device; } set { _current_device = value; } }
	//
	private Profile _current_profile;

	public Profile CurrentProfile { get { return _current_profile; } set { _current_profile = value; } }
	//
	private bool allowQuitting = false;

	public bool AllowQuitting { get { return allowQuitting; } set { allowQuitting = value; } }
	//
	private bool _isPaused = false;

	public bool IsPaused { get { return _isPaused; } set { _isPaused = value; } }

	#region SQLite Database: Primary Key

	// Primary Key of Table::Login
	private int _LoginID = -1;

	public int LoginID { get { return _LoginID; } set { _LoginID = value; } }

	private int _PauseID = -1;

	public int PauseID { get { return _PauseID; } set { _PauseID = value; } }

	#endregion

	#region Online Database: Primary Key

	// Primary Key of Table::Login
	private int _Online_LoginID = -1;

	public int OnlineLoginID { get { return _Online_LoginID; } set { _Online_LoginID = value; } }

	#endregion

	[Header ("Debug Settings")]
	public bool Debugging = false;
	public bool ForceOverwriteDatabase = false;

	// Initial value with -1 will used when start from the other scene, not FirstScence.
	public int lastID = -1;
	public int lastLocationTrackingForOnline = -1;

	//
	public bool isOfflineInsertProcessFinished = false;
	public bool isOfflineInsertCompleted = false;

	public int levelPlayed = 0;
	public bool isLevelClearedBefore = false;
	public int pastResult = -1;

	public bool checkedOfflineData ()
	{
		DatabaseService dbService = new DatabaseService ();
		int record = 0;

		record = dbService.LevelPlayed_NeedUpload_RecordCount (CurrentDevice.DeviceInfo) +
		dbService.LocationTracking_NeedUpload_RecordCount (CurrentDevice.DeviceInfo) +
		dbService.OnboardingAnswer_NeedUpload_RecordCount (CurrentDevice.DeviceInfo) +
		dbService.PlayerProfile_NeedUpload_RecordCount (CurrentDevice.DeviceInfo) +
		dbService.Readiness_NeedUpload_RecordCount (CurrentDevice.DeviceInfo);

		if (record > 0) {
			return true;
		} else {
			return false;
		}
	}
	/*
	public void InsertOnboardingAnswer (float firstCravingAfterAwake, int numberOfCigsYesterday, string triggerAnswer, string relapseAnswer, string answerDate)
	{
		var dbService = new DatabaseService ();
		// Check device is online or not.
		checkedOnline ();
		if (isDeviceOnline) {
			// Device has internet connection.
			// Insert into Cigbreak database server.
			onlineDBManager.OnboardingAnswer_Insert (DeviceInfo, firstCravingAfterAwake, numberOfCigsYesterday, triggerAnswer, relapseAnswer, answerDate, 0);

		}
		// Insert into local database asset.
		lastID = dbService.OnboardingAnswer_Insert (DeviceInfo, firstCravingAfterAwake, numberOfCigsYesterday, triggerAnswer, relapseAnswer, answerDate, onlineStatus);
		CatchDebugMessage ("InsertOnboardingAnswer Last ID:" + lastID.ToString ());
	}
*/
	/*
	public IEnumerator TransferOffline_OnboardingAnswer (int rowID, string deviceInfo, float firstCravingAfterAwake, int numberOfCigsYesterday, string triggerAnswer, string relapseAnswer, string answerDate)
	{
		yield return StartCoroutine (onlineDBManager.OnboardingAnswer_Offline_Insert (deviceInfo, firstCravingAfterAwake, numberOfCigsYesterday, triggerAnswer, relapseAnswer, answerDate, 1));

		//
		if (isOfflineInsertCompleted) {
			var dbService = new DatabaseService ();
			dbService.OnboardingAnswer_OfflineUpdate (rowID);
		}

		yield return null;
	}
	*/
	/*
	public void InsertReadiness (float readinessRate, float firstCravingAfterAwake, int numberOfCigsPerDay, int nonSmoker, string answerDate)
	{
		var dbService = new DatabaseService ();
		// Check device is online or not.
		checkedOnline ();
		if (isDeviceOnline) {
			// Device has internet connection.
			// Insert into Cigbreak database server.
			onlineDBManager.Readiness_Insert (DeviceInfo, readinessRate, firstCravingAfterAwake, numberOfCigsPerDay, nonSmoker, answerDate, 0);

		}
		// Insert into local database asset.
		lastID = dbService.Readiness_Insert (DeviceInfo, readinessRate, firstCravingAfterAwake, numberOfCigsPerDay, nonSmoker, answerDate, onlineStatus);
		CatchDebugMessage ("InsertReadiness Last ID:" + lastID.ToString ());
	}
	*/
	/*
	public IEnumerator TransferOffline_Readiness (int rowID, string deviceInfo, float readinessRate, float firstCravingAfterAwake, int numberOfCigsPerDay, int nonSmoker, string answerDate)
	{
		yield return onlineDBManager.Readiness_Offline_Insert (deviceInfo, readinessRate, firstCravingAfterAwake, numberOfCigsPerDay, nonSmoker, answerDate, 1);

		//
		if (isOfflineInsertCompleted) {
			var dbService = new DatabaseService ();
			dbService.Readiness_OfflineUpdate (rowID);
		}

		yield return null;
	}
	*/
	// Awake is always called before any Start functions
	void Awake ()
	{
		// Check if instance already exists
		if (instance == null) {
			// if not, set instance to this
			instance = this;
		}
		// If instance already exists and it's not this:
		else if (instance != this) {
			// Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy (gameObject);    
		}
		// Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad (gameObject);
	}

	public IEnumerator Setup ()
	{
		// Setup object by dependency
		SetupDebugPanel ();

		// Location Manager
		if (GetComponent<LocationManager> () == null) {
			Debug.Log ("Location Manager script is not found!!!");
		}
		locationManager = GetComponent<LocationManager> ();
		//locationManager.InitialiseManager ();

		// SQLite Manager
		if (GetComponent<SQLiteManager> () == null) {
			Debug.Log ("SQLite Manager script is not found!!!");
		}
		sqliteManager = GetComponent<SQLiteManager> ();
		//sqliteManager.InitialiseManager ();

		// Online Database Manager
		if (GetComponent<OnlineDBManager> () == null) {
			Debug.Log ("OnlineDB Manager script is not found!!!");
		}
		onlineDBManager = GetComponent<OnlineDBManager> ();
		//onlineDBManager.InitialiseManager ();

		//
		if (GetComponent<NetworkManager> () == null) {
			Debug.Log ("Network Manager script is not found!!!");
		}

		//Get a component reference to the attached NetworkManager script
		networkScript = GetComponent<NetworkManager> ();

		//
		if (Debug.isDebugBuild) {
			_isDebugBuild = true;
		} else {
			_isDebugBuild = false;
		}

		// Device setup.
		_current_device = new MobileDevice ();
		_current_device.DeviceInfo = generatedDeviceInfo ();
		WriteLine ("CigbreakManager::Device Info:: " + _current_device.ToString ());
		// Player setup.
		_current_player = new Player ();
		_current_player.PlayerName = _current_device.DeviceInfo;
		WriteLine ("CigbreakManager::Player Name:: " + _current_player.ToString ());
		// Profile setup.
		_current_profile = new Profile ();

		// Check this device can connect to online database server.
		// true mean "The device can connect to Cigbreak web server"
		//_onlineStatus = networkScript.hasInternetConnection ();
		_isPaused = false;

		yield return StartCoroutine (Device_Setup ());
		WriteLine ("CigbreakManager::Current device::" + _current_device.ToString ());

		yield return StartCoroutine (Player_Setup ());
		WriteLine ("CigbreakManager::Current player::" + _current_player.ToString ());
		/*
		yield return StartCoroutine (Profile_Setup ());
		WriteLine ("CigbreakManager::Current profile::" + _current_profile.ToString ());
		*/

		yield return StartCoroutine (onlineDBManager.Login_OnlineInsert ("Start Game"));

        /*
		// // // // //
		DatabaseService dbService = new DatabaseService ();
		// TODO: Should use some parameter instead of fixed string. In case, change table name.
		if (_onlineStatus) {
			WriteLine ("The device can connect to Cigbreak web server");
			// TODO :: Re-implement this function
			currentProfile = dbService.PlayerProfile_LoadProfile (CurrentDevice.DeviceInfo, 0);

			WriteLine ("Current profile:" + currentProfile.ToString ());
			if (currentProfile.First_Time_Of_Day == 1 && currentProfile.Total_Login_Days == 1 &&
			    currentProfile.Total_Login_Counts == 1 && currentProfile.Consecutive_Login == 1) {
				WriteLine ("This is new profile record on the device.");
				// Need to check with online database server to load profile.
				StartCoroutine (getProfileFromOnlineServer (CurrentDevice.DeviceInfo));
			}
		} else {
			WriteLine ("The device cannot connect to Cigbreak web server");

			currentProfile = dbService.PlayerProfile_LoadProfile (CurrentDevice.DeviceInfo, 1);
		}
		TotalPlayedTimeOnDevice = dbService.LevelPlayed_TotalPlayedTime (CurrentDevice.DeviceInfo);
        */
		// // // // //
	}

	public void SetupDebugPanel ()
	{
		// Debug Manager
		if (debugPanel == null) {
			debugPanel = GameObject.Find ("DebugPanel");

		}
	}

	IEnumerator Device_Setup ()
	{
		// Connect to online database to get OnlineDeviceID first.
		yield return StartCoroutine (onlineDBManager.Device_OnlineInsert ());
	}

	IEnumerator Player_Setup ()
	{
		// Connect to online database to get OnlinePlayerID first.
		yield return StartCoroutine (onlineDBManager.Player_OnlineInsert ());
	}



	IEnumerator Profile_Setup ()
	{
		// Count profile
		// Count inventory 
		// Count playedrecord
		// Count onboardingAnswer

		yield return StartCoroutine (onlineDBManager.Profile_OnlineCreate ());
	}

	public void WriteLine (string message)
	{
			Debug.Log (message);
	}

	public void PauseGame ()
	{
		DateTime curDateTime = DateTime.Now;
		string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

		// Pause & Resume doesn't need to post data at real-time.
		Login record = new Login ();
		record.PlayerID = _current_player.PlayerID;
		record.OnlinePlayerID = _current_player.OnlinePlayerID;
		record.DeviceID = _current_device.DeviceID;
		record.OnlineDeviceID = _current_device.OnlineDeviceID;
		record.StartTime = mySQLTime;
		record.StartLat = locationManager.CurrentLocation.Latitude;
		record.StartLong = locationManager.CurrentLocation.Longitude;
		record.CommentS = "Pause Game";
		record.CreateDate = mySQLTime;
		record.Status = 1;

		sqliteManager.Pause_Insert (record);
		_isPaused = true;
	}


	public void ResumeGame ()
	{
		DateTime curDateTime = DateTime.Now;
		string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

		// Pause & Resume doesn't need to post data at real-time.
		Login record = new Login ();
		record.LoginID = _PauseID;
		record.PlayerID = _current_player.PlayerID;
		record.OnlinePlayerID = _current_player.OnlinePlayerID;
		record.DeviceID = _current_device.DeviceID;
		record.OnlineDeviceID = _current_device.OnlineDeviceID;
		record.EndTime = mySQLTime;
		record.EndLat = locationManager.CurrentLocation.Latitude;
		record.EndLong = locationManager.CurrentLocation.Longitude;
		record.CommentE = "Resume Game";
		record.UpdateDate = mySQLTime;
		record.Status = 1;

		sqliteManager.Pause_Update (record);
		_isPaused = false;
	}

	public IEnumerator QuitGame ()
	{
		if (onlineDBManager == null) {
			yield return null;
		} else {
			yield return StartCoroutine (onlineDBManager.Login_OnlineUpdate ("Exit Game"));
		}
		allowQuitting = true;
		Ouit ();
	}

	void Ouit ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#endif
		Application.Quit ();
	}

	#region Online Database Management

	private string baseURL = "https://www.healthygames.uk/dbservices/";

	public IEnumerator LevelPlayed_Insert (LevelPlayed record, Action callWhenFinished)
	{
		yield return checkDeviceIsOnline (setDeviceStatus);
		Debug.Log ("LevelPlayed_Insert - Device is online: " + _onlineStatus.ToString ());
		if (_onlineStatus) {
			Debug.Log ("Call Coroutine to update online database server. (LevelPlayed_Insert)");
			yield return StartCoroutine (LevelPlayed_Insert_Online (record, 0, insertLevelPlayedIntoLocalDB));
			callWhenFinished.Invoke ();
		} else {
			Debug.Log ("Call Coroutine to update local database.");
			insertLevelPlayedIntoLocalDB (record, 1);
			callWhenFinished.Invoke ();
		}
	}

	private IEnumerator LevelPlayed_Insert_Online (LevelPlayed record, int onlineStatus, Action<LevelPlayed, int> insertLocalDBCall)
	{
        yield return null;
        /*
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("levelID", record.Level_ID);
		form.AddField ("levelStartTime", record.Level_StartTime);
		form.AddField ("levelEndTime", record.Level_EndTime);
		form.AddField ("result", record.Result);
		form.AddField ("lifeRemain", record.Life_Remain);
		form.AddField ("swipe", record.Swipe);
		form.AddField ("swipeSuccess", record.Swipe_Success);
		form.AddField ("swipeMultiple", record.Swipe_Multiple);
		form.AddField ("tap", record.Tap);
		form.AddField ("tapSuccess", record.Tap_Success);
		form.AddField ("cigaretteSpawn", record.Cigarette_Spawn);
		form.AddField ("cigaretteBreak", record.Cigarette_Break);
		form.AddField ("rollupSpawn", record.Rollup_Spawn);
		form.AddField ("rollupBreak", record.Rollup_Break);
		form.AddField ("cigarSpawn", record.Cigar_Spawn);
		form.AddField ("cigarBreak", record.Cigar_Break);
		form.AddField ("pipeSpawn", record.Pipe_Spawn);
		form.AddField ("pipeBreak", record.Pipe_Break);
		form.AddField ("carrotSpawn", record.Carrot_Spawn);
		form.AddField ("carrotBreak", record.Carrot_Break);
		form.AddField ("carrotTap", record.Carrot_Tap);
		form.AddField ("celerySpawn", record.Celery_Spawn);
		form.AddField ("celeryBreak", record.Celery_Break);
		form.AddField ("celeryTap", record.Celery_Tap);
		form.AddField ("springOnionSpawn", record.SpringOnion_Spawn);
		form.AddField ("springOnionBreak", record.SpringOnion_Break);
		form.AddField ("springOnionTap", record.SpringOnion_Tap);
		form.AddField ("chiliSpawn", record.Chili_Spawn);
		form.AddField ("chiliBreak", record.Chili_Break);
		form.AddField ("chiliTap", record.Chili_Tap);
		form.AddField ("cornSpawn", record.Corn_Spawn);
		form.AddField ("cornBreak", record.Corn_Break);
		form.AddField ("cornTap", record.Corn_Tap);
		form.AddField ("bananaSpawn", record.Banana_Spawn);
		form.AddField ("bananaBreak", record.Banana_Break);
		form.AddField ("bananaTap", record.Banana_Tap);
		form.AddField ("dynamiteSpawn", record.Dynamite_Spawn);
		form.AddField ("dynamiteBreak", record.Dynamite_Break);
		form.AddField ("lollipopSpawn", record.Lollipop_Spawn);
		form.AddField ("lollipopBreak", record.Lollipop_Break);
		form.AddField ("sprayUse", record.Spray_Use);
		form.AddField ("gumUse", record.Gum_Use);
		form.AddField ("patchUse", record.Patch_Use);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertLevelPlayed.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("LevelPlayed_Insert_Online: WWW fail.");
				insertLocalDBCall (record, 1);
			} else {
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("LevelPlayed_Insert_Online: Insert online database successful.");
					insertLocalDBCall (record, 0);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("LevelPlayed_Insert_Online: Insert online database fail.");
					insertLocalDBCall (record, 1);
				}
			}
		}
        */
	}

	public IEnumerator LevelPlayed_Offline_Update (LevelPlayed record, Action<int> updateLocalDBCall)
	{
        yield return null;
        /*
		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("levelID", record.Level_ID);
		form.AddField ("levelStartTime", record.Level_StartTime);
		form.AddField ("levelEndTime", record.Level_EndTime);
		form.AddField ("result", record.Result);
		form.AddField ("lifeRemain", record.Life_Remain);
		form.AddField ("swipe", record.Swipe);
		form.AddField ("swipeSuccess", record.Swipe_Success);
		form.AddField ("swipeMultiple", record.Swipe_Multiple);
		form.AddField ("tap", record.Tap);
		form.AddField ("tapSuccess", record.Tap_Success);
		form.AddField ("cigaretteSpawn", record.Cigarette_Spawn);
		form.AddField ("cigaretteBreak", record.Cigarette_Break);
		form.AddField ("rollupSpawn", record.Rollup_Spawn);
		form.AddField ("rollupBreak", record.Rollup_Break);
		form.AddField ("cigarSpawn", record.Cigar_Spawn);
		form.AddField ("cigarBreak", record.Cigar_Break);
		form.AddField ("pipeSpawn", record.Pipe_Spawn);
		form.AddField ("pipeBreak", record.Pipe_Break);
		form.AddField ("carrotSpawn", record.Carrot_Spawn);
		form.AddField ("carrotBreak", record.Carrot_Break);
		form.AddField ("carrotTap", record.Carrot_Tap);
		form.AddField ("celerySpawn", record.Celery_Spawn);
		form.AddField ("celeryBreak", record.Celery_Break);
		form.AddField ("celeryTap", record.Celery_Tap);
		form.AddField ("springOnionSpawn", record.SpringOnion_Spawn);
		form.AddField ("springOnionBreak", record.SpringOnion_Break);
		form.AddField ("springOnionTap", record.SpringOnion_Tap);
		form.AddField ("chiliSpawn", record.Chili_Spawn);
		form.AddField ("chiliBreak", record.Chili_Break);
		form.AddField ("chiliTap", record.Chili_Tap);
		form.AddField ("cornSpawn", record.Corn_Spawn);
		form.AddField ("cornBreak", record.Corn_Break);
		form.AddField ("cornTap", record.Corn_Tap);
		form.AddField ("bananaSpawn", record.Banana_Spawn);
		form.AddField ("bananaBreak", record.Banana_Break);
		form.AddField ("bananaTap", record.Banana_Tap);
		form.AddField ("dynamiteSpawn", record.Dynamite_Spawn);
		form.AddField ("dynamiteBreak", record.Dynamite_Break);
		form.AddField ("lollipopSpawn", record.Lollipop_Spawn);
		form.AddField ("lollipopBreak", record.Lollipop_Break);
		form.AddField ("sprayUse", record.Spray_Use);
		form.AddField ("gumUse", record.Gum_Use);
		form.AddField ("patchUse", record.Patch_Use);
		form.AddField ("isOffline", record.Need_Upload);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertLevelPlayed.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("LevelPlayed_Offline_Update: WWW fail.");	
				// Do nothing.
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				if (www.downloadHandler.text == "SUCCESS") {
					Debug.Log ("LevelPlayed_Offline_Update: Insert online database successful.");
					updateLocalDBCall (record.ID);
				} else {
					Debug.Log ("LevelPlayed_Offline_Update: Insert online database fail.");					
					// Do nothing.
				}
			}
		}
        */
	}

	private IEnumerator getProfileFromOnlineServer (string deviceInfo)
	{
        yield return null;
        /*
		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", deviceInfo);
		form.AddField ("hash", MD5Sum (deviceInfo));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "getProfileOnWebserver.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("getProfileFromOnlineServer: WWW fail.");
			} else {
				Debug.Log ("getProfileFromOnlineServer: get data successful.");

				if (www.downloadHandler.text.IndexOf ("NODATA") != -1) {
					// Show results as text
					Debug.Log ("NO DATA:" + www.downloadHandler.text);
					// Upload new profile info into online database server
					yield return StartCoroutine (PlayerProfile_Insert_Online (currentProfile, 0, updatePlayerProfileOnLocalDB));
				} else {
					// Show results as text
					Debug.Log ("DATA:" + www.downloadHandler.text);

					string jsonString = www.downloadHandler.text;
					ProfileResponse pResponse = JsonUtility.FromJson<ProfileResponse> (jsonString);

					if (pResponse.status == "true" && pResponse.message == "Success") {
						jsonString = jsonString.Replace ("{\"status\":true,\"message\":\"Success\",\"data\":{", "{");
						jsonString = jsonString.Replace ("}}", "}");

						ProfileData pData = JsonUtility.FromJson<ProfileData> (jsonString);
						Debug.Log ("Current profile:BEFORE:" + currentProfile.ToString ());

						int profileID = currentProfile.ID;
						//currentProfile = new PlayerProfile (pData);
						currentProfile.ID = profileID;

						DatabaseService dbService = new DatabaseService ();
						dbService.PlayerProfile_Update (currentProfile);
						currentProfile = dbService.PlayerProfile_LoadProfile (deviceInfo, 0);
						Debug.Log ("Current profile:AFTER:" + currentProfile.ToString ());
					}
				}
			}
		}
        */
	}

	private IEnumerator LocationTracking_Insert_Online (string comment, int onlineStatus, Action<string, int> insertLocalDBCall)
	{
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", CurrentDevice.DeviceInfo);
		form.AddField ("latData", currentLocation.Latitude.ToString ());
		form.AddField ("longData", currentLocation.Longitude.ToString ());
		form.AddField ("postcode", Postcode);
		form.AddField ("recordDate", curDateTime.ToString ());
		form.AddField ("comment", comment);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (CurrentDevice.DeviceInfo));


		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertLocationTracking.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("LocationTracking_Insert_Online: WWW fail.");
				insertLocalDBCall (comment, 1);
			} else {
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("LocationTracking_Insert_Online: Insert online database successful.");
					insertLocalDBCall (comment, 0);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("LocationTracking_Insert_Online: Insert online database fail.");
					insertLocalDBCall (comment, 1);
				}
			}
		}
	}

	public IEnumerator LocationTracking_Offline_Update (LocationTracking record, Action<int> updateLocalDBCall)
	{
		yield return StartCoroutine (GetPostcode (record.Latitude, record.Longitude, setPostcode));
		Debug.Log ("Postcode:" + Postcode);

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("latData", record.Latitude.ToString ());
		form.AddField ("longData", record.Longitude.ToString ());
		form.AddField ("postcode", Postcode);
		form.AddField ("recordDate", record.Record_Date);
		form.AddField ("comment", record.Comment);
		form.AddField ("isOffline", record.Need_Upload);
		form.AddField ("hash", MD5Sum (record.Device_Info));


		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertLocationTracking.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("LocationTracking_Offline_Update: WWW fail.");	
				// Do nothing.
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				if (www.downloadHandler.text == "SUCCESS") {
					Debug.Log ("LocationTracking_Offline_Update: Insert online database successful.");
					updateLocalDBCall (record.ID);
				} else {
					Debug.Log ("LocationTracking_Offline_Update: Insert online database fail.");					
					// Do nothing.
				}
			}
		}
	}

	private IEnumerator PlayerProfile_Insert_Online (Profile record, int onlineStatus, Action<Profile> updateLocalDBCall)
	{
        yield return null;
        /*
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("startDate", record.Start_Date);
		form.AddField ("lastLoginDate", record.Last_Login_Date);
		form.AddField ("firstTimeOfDay", record.First_Time_Of_Day);
		form.AddField ("totalLoginDays", record.Total_Login_Days);
		form.AddField ("totalLoginCounts", record.Total_Login_Counts);
		form.AddField ("consecutiveLogin", record.Consecutive_Login);
		form.AddField ("starsFromSmokeless", record.Stars_From_Smokeless);
		form.AddField ("starsFromLevelCleared", record.Stars_From_Level_Cleared);
		form.AddField ("totalStars", record.Total_Stars);
		form.AddField ("currentCoins", record.Current_Coins);
		form.AddField ("totalCoins", record.Total_Coins);
		form.AddField ("currentCarrot", record.Current_Carrot);
		form.AddField ("soldCarrot", record.Sold_Carrot);
		form.AddField ("totalCarrot", record.Total_Carrot);
		form.AddField ("currentCelery", record.Current_Celery);
		form.AddField ("soldCelery", record.Sold_Celery);
		form.AddField ("totalCelery", record.Total_Celery);
		form.AddField ("currentSpringOnion", record.Current_Spring_Onion);
		form.AddField ("soldSpringOnion", record.Sold_Spring_Onion);
		form.AddField ("totalSpringOnion", record.Total_Spring_Onion);
		form.AddField ("currentCorn", record.Current_Corn);
		form.AddField ("soldCorn", record.Sold_Corn);
		form.AddField ("totalCorn", record.Total_Corn);
		form.AddField ("currentChili", record.Current_Chili);
		form.AddField ("soldChili", record.Sold_Chili);
		form.AddField ("totalChili", record.Total_Chili);
		form.AddField ("currentBanana", record.Current_Banana);
		form.AddField ("soldBanana", record.Sold_Banana);
		form.AddField ("totalBanana", record.Total_Banana);
		form.AddField ("currentSpray", record.Current_Spray);
		form.AddField ("totalSpray", record.Total_Spray);
		form.AddField ("currentGum", record.Current_Gum);
		form.AddField ("totalGum", record.Total_Gum);
		form.AddField ("current_Patch", record.Current_Patch);
		form.AddField ("totalPatch", record.Total_Patch);
		form.AddField ("totalLevelPassed", record.Total_Level_Passed);
		form.AddField ("totalLevelPassedWithThreeStars", record.Total_Level_Passed_With_Three_Stars);
		form.AddField ("totalMapCleared", record.Total_Map_Cleared);
		form.AddField ("totalBrokeCigarettes", record.Total_Broke_Cigarettes);
		form.AddField ("totalBrokeRollups", record.Total_Broke_Rollups);
		form.AddField ("totalBrokeCigars", record.Total_Broke_Cigars);
		form.AddField ("totalBrokePipes", record.Total_Broke_Pipes);
		form.AddField ("totalBrokeDynamites", record.Total_Broke_Dynamites);
		form.AddField ("totalSprayUsed", record.Total_Spray_Used);
		form.AddField ("totalGumUsed", record.Total_Gum_Used);
		form.AddField ("totalPatchUsed", record.Total_Patch_Used);
		form.AddField ("totalQuitMissionCompleted", record.Total_Quit_Mission_Completed);
		form.AddField ("totalGameMissionCompleted", record.Total_Game_Mission_Completed);
		form.AddField ("totalSoldVeggies", record.Total_Sold_Veggies);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertPlayerProfile.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("PlayerProfile_Insert_Online: WWW fail.");
				record.Need_Upload = 1;
				updateLocalDBCall (record);
			} else {
				Debug.Log ("999: " + www.downloadHandler.text);
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("PlayerProfile_Insert_Online: Insert online database successful.");
					record.Need_Upload = 0;
					updateLocalDBCall (record);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("PlayerProfile_Insert_Online: Insert online database fail.");
					record.Need_Upload = 1;
					updateLocalDBCall (record);
				}
			}
		}
        */
	}

	public IEnumerator PlayerProfile_Update (Profile record, Action callWhenFinished)
	{
        yield return null;
        /*
		yield return checkDeviceIsOnline (setDeviceStatus);
		Debug.Log ("PlayerProfile_Update - Device is online: " + _onlineStatus.ToString ());
		if (_onlineStatus) {
			Debug.Log ("Call Coroutine to update online database server. (PlayerProfile_Update)");
			yield return StartCoroutine (PlayerProfile_Update_Online (record, 0, updatePlayerProfileOnLocalDB));
			callWhenFinished.Invoke ();
		} else {
			Debug.Log ("Call Coroutine to update local database.");
			record.Need_Upload = 1;
			updatePlayerProfileOnLocalDB (record);
			callWhenFinished.Invoke ();
		}
        */
	}

	private IEnumerator PlayerProfile_Update_Online (Profile record, int onlineStatus, Action<Profile> updateLocalDBCall)
	{
        yield return null;
        /*
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("startDate", record.Start_Date);
		form.AddField ("lastLoginDate", record.Last_Login_Date);
		form.AddField ("firstTimeOfDay", record.First_Time_Of_Day);
		form.AddField ("totalLoginDays", record.Total_Login_Days);
		form.AddField ("totalLoginCounts", record.Total_Login_Counts);
		form.AddField ("consecutiveLogin", record.Consecutive_Login);
		form.AddField ("starsFromSmokeless", record.Stars_From_Smokeless);
		form.AddField ("starsFromLevelCleared", record.Stars_From_Level_Cleared);
		form.AddField ("totalStars", record.Total_Stars);
		form.AddField ("currentCoins", record.Current_Coins);
		form.AddField ("totalCoins", record.Total_Coins);
		form.AddField ("currentCarrot", record.Current_Carrot);
		form.AddField ("soldCarrot", record.Sold_Carrot);
		form.AddField ("totalCarrot", record.Total_Carrot);
		form.AddField ("currentCelery", record.Current_Celery);
		form.AddField ("soldCelery", record.Sold_Celery);
		form.AddField ("totalCelery", record.Total_Celery);
		form.AddField ("currentSpringOnion", record.Current_Spring_Onion);
		form.AddField ("soldSpringOnion", record.Sold_Spring_Onion);
		form.AddField ("totalSpringOnion", record.Total_Spring_Onion);
		form.AddField ("currentCorn", record.Current_Corn);
		form.AddField ("soldCorn", record.Sold_Corn);
		form.AddField ("totalCorn", record.Total_Corn);
		form.AddField ("currentChili", record.Current_Chili);
		form.AddField ("soldChili", record.Sold_Chili);
		form.AddField ("totalChili", record.Total_Chili);
		form.AddField ("currentBanana", record.Current_Banana);
		form.AddField ("soldBanana", record.Sold_Banana);
		form.AddField ("totalBanana", record.Total_Banana);
		form.AddField ("currentSpray", record.Current_Spray);
		form.AddField ("totalSpray", record.Total_Spray);
		form.AddField ("currentGum", record.Current_Gum);
		form.AddField ("totalGum", record.Total_Gum);
		form.AddField ("current_Patch", record.Current_Patch);
		form.AddField ("totalPatch", record.Total_Patch);
		form.AddField ("totalLevelPassed", record.Total_Level_Passed);
		form.AddField ("totalLevelPassedWithThreeStars", record.Total_Level_Passed_With_Three_Stars);
		form.AddField ("totalMapCleared", record.Total_Map_Cleared);
		form.AddField ("totalBrokeCigarettes", record.Total_Broke_Cigarettes);
		form.AddField ("totalBrokeRollups", record.Total_Broke_Rollups);
		form.AddField ("totalBrokeCigars", record.Total_Broke_Cigars);
		form.AddField ("totalBrokePipes", record.Total_Broke_Pipes);
		form.AddField ("totalBrokeDynamites", record.Total_Broke_Dynamites);
		form.AddField ("totalSprayUsed", record.Total_Spray_Used);
		form.AddField ("totalGumUsed", record.Total_Gum_Used);
		form.AddField ("totalPatchUsed", record.Total_Patch_Used);
		form.AddField ("totalQuitMissionCompleted", record.Total_Quit_Mission_Completed);
		form.AddField ("totalGameMissionCompleted", record.Total_Game_Mission_Completed);
		form.AddField ("totalSoldVeggies", record.Total_Sold_Veggies);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "updatePlayerProfile.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("PlayerProfile_Update_Online: WWW fail.");
				record.Need_Upload = 1;
				updateLocalDBCall (record);
			} else {
				Debug.Log ("999: " + www.downloadHandler.text);
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("PlayerProfile_Update_Online: Update online database successful.");
					record.Need_Upload = 0;
					updateLocalDBCall (record);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("PlayerProfile_Update_Online: Update online database fail.");
					record.Need_Upload = 1;
					updateLocalDBCall (record);
				}
			}
		}
        */
	}

	public IEnumerator Readiness_Insert (Readiness record, Action callWhenFinished)
	{
		yield return checkDeviceIsOnline (setDeviceStatus);
		Debug.Log ("Readiness_Insert - Device is online: " + _onlineStatus.ToString ());
		if (_onlineStatus) {
			Debug.Log ("Call Coroutine to update online database server. (Readiness_Insert)");
			yield return StartCoroutine (Readiness_Insert_Online (record, 0, insertReadinessIntoLocalDB));
			callWhenFinished.Invoke ();
		} else {
			Debug.Log ("Call Coroutine to update local database.");
			insertReadinessIntoLocalDB (record, 1);
			callWhenFinished.Invoke ();
		}
	}

	private IEnumerator Readiness_Insert_Online (Readiness record, int onlineStatus, Action<Readiness, int> insertLocalDBCall)
	{
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("readinessRate", record.Readiness_Rate.ToString ());
		form.AddField ("firstCigsAfterWakeup", record.First_Cigs_After_Wakeup.ToString ());
		form.AddField ("cigsPerDay", record.Cigs_Per_Day.ToString ());
		form.AddField ("nonSmoker", record.Non_Smoker.ToString ());
		form.AddField ("answerDate", record.Answer_Date);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertReadiness.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("Readiness_Insert_Online: WWW fail.");
				insertLocalDBCall (record, 1);
			} else {
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("Readiness_Insert_Online: Insert online database successful.");
					insertLocalDBCall (record, 0);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("Readiness_Insert_Online: Insert online database fail.");
					insertLocalDBCall (record, 1);
				}
			}
		}
	}

	public IEnumerator Readiness_Offline_Update (Readiness record, Action<int> updateLocalDBCall)
	{
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("readinessRate", record.Readiness_Rate.ToString ());
		form.AddField ("firstCigsAfterWakeup", record.First_Cigs_After_Wakeup.ToString ());
		form.AddField ("cigsPerDay", record.Cigs_Per_Day.ToString ());
		form.AddField ("nonSmoker", record.Non_Smoker.ToString ());
		form.AddField ("answerDate", record.Answer_Date);
		form.AddField ("isOffline", _onlineStatus.ToString ());
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertReadiness.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("Readiness_Offline_Update: WWW fail.");	
				// Do nothing.
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				if (www.downloadHandler.text == "SUCCESS") {
					Debug.Log ("Readiness_Offline_Update: Insert online database successful.");
					updateLocalDBCall (record.ID);
				} else {
					Debug.Log ("Readiness_Offline_Update: Insert online database fail.");					
					// Do nothing.
				}
			}
		}
	}

	public IEnumerator OnboardingAnswer_Insert (OnboardingAnswer record, Action callWhenFinished)
	{
		yield return checkDeviceIsOnline (setDeviceStatus);
		Debug.Log ("OnboardingAnswer_Insert - Device is online: " + _onlineStatus.ToString ());
		if (_onlineStatus) {
			Debug.Log ("Call Coroutine to update online database server. (OnboardingAnswer_Insert)");
			yield return StartCoroutine (OnboardingAnswer_Insert_Online (record, 0, insertOnboardingAnswerIntoLocalDB));
			callWhenFinished.Invoke ();
		} else {
			Debug.Log ("Call Coroutine to update local database.");
			insertOnboardingAnswerIntoLocalDB (record, 1);
			callWhenFinished.Invoke ();
		}
	}

	private IEnumerator OnboardingAnswer_Insert_Online (OnboardingAnswer record, int onlineStatus, Action<OnboardingAnswer, int> insertLocalDBCall)
	{
		DateTime curDateTime = DateTime.Now;

		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("firstCigsAfterWakeup", record.First_Cigs_After_Wakeup.ToString ());
		form.AddField ("cigsOnYesterday", record.Cigs_On_Yesterday.ToString ());
		form.AddField ("triggeredSmoke", record.Triggered_Smoke);
		form.AddField ("relapseFactor", record.Relapse_Factor);
		form.AddField ("answerDate", record.Answer_Date);
		form.AddField ("isOffline", onlineStatus);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertOnboardingAnswer.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("OnboardingAnswer_Insert_Online: WWW fail.");
				insertLocalDBCall (record, 1);
			} else {
				int recordID;
				if (Int32.TryParse (www.downloadHandler.text, out recordID)) {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("OnboardingAnswer_Insert_Online: Insert online database successful.");
					insertLocalDBCall (record, 0);
				} else {
					// Show results as text
					Debug.Log (www.downloadHandler.text);
					Debug.Log ("OnboardingAnswer_Insert_Online: Insert online database fail.");
					insertLocalDBCall (record, 1);
				}
			}
		}
	}

	public IEnumerator OnboardingAnswer_Offline_Update (OnboardingAnswer record, Action<int> updateLocalDBCall)
	{
		WWWForm form = new WWWForm ();
		form.AddField ("deviceInfo", record.Device_Info);
		form.AddField ("firstCigsAfterWakeup", record.First_Cigs_After_Wakeup.ToString ());
		form.AddField ("cigsOnYesterday", record.Cigs_On_Yesterday.ToString ());
		form.AddField ("triggeredSmoke", record.Triggered_Smoke);
		form.AddField ("relapseFactor", record.Relapse_Factor);
		form.AddField ("answerDate", record.Answer_Date);
		form.AddField ("isOffline", record.Need_Upload);
		form.AddField ("hash", MD5Sum (record.Device_Info));

		using (UnityWebRequest www = UnityWebRequest.Post (baseURL + "insertOnboardingAnswer.php", form)) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				Debug.Log ("OnboardingAnswer_Offline_Update: WWW fail.");	
				// Do nothing.
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				if (www.downloadHandler.text == "SUCCESS") {
					Debug.Log ("OnboardingAnswer_Offline_Update: Insert online database successful.");
					updateLocalDBCall (record.ID);
				} else {
					Debug.Log ("OnboardingAnswer_Offline_Update: Insert online database fail.");					
					// Do nothing.
				}
			}
		}
	}



	#endregion

	#region Local database

	public void insertLevelPlayedIntoLocalDB (LevelPlayed record, int status)
	{
		var dbService = new DatabaseService ();
		record.Status = status;
		int recordID = dbService.LevelPlayed_Insert (record);
		Debug.Log ("Local DB - Insert LevelPlayed ID:" + record.ID.ToString ());
	}

	public void updatePlayerProfileOnLocalDB (Profile record)
	{
        /*
		var dbService = new DatabaseService ();
		dbService.PlayerProfile_Update (record);
		Debug.Log ("Local DB - Update PlayerProfile ID:" + record.ID.ToString ());
        */
	}

	/// <summary>
	/// Inserts LocationTracking into Local DB.
	/// </summary>
	/// <param name="comment">Comment.</param>
	/// <param name="uploadStatus">Upload status is 1 mean this record need upload to online database server.\n
	/// 0 mean this record already upload to online database server</param>
	private void insertLocationTrackingIntoLocalDB (string comment, int uploadStatus)
	{
		DateTime curDateTime = DateTime.Now;
		var dbService = new DatabaseService ();
		int recordID = dbService.LocationTracking_Insert (CurrentDevice.DeviceInfo, currentLocation.Latitude, currentLocation.Longitude, curDateTime.ToString (), comment, uploadStatus);
		Debug.Log ("Local DB - Insert LocationTracking ID:" + recordID.ToString ());
	}

	public void updateLocationTrackingOnLocalDB (int recordID)
	{
		var dbService = new DatabaseService ();
		dbService.LocationTracking_OfflineUpdate (recordID);
		Debug.Log ("Local DB - Update LocationTracking ID:" + recordID.ToString ());
	}

	/// <summary>
	/// Inserts Readiness into Local DB.
	/// </summary>
	/// <param name="record">Readiness data record.</param>
	/// <param name="uploadStatus">Upload status is 1 mean this record need upload to online database server.\n
	/// 0 mean this record already upload to online database server</param>
	private void insertReadinessIntoLocalDB (Readiness record, int uploadStatus)
	{
		DateTime curDateTime = DateTime.Now;
		var dbService = new DatabaseService ();
		int recordID = dbService.Readiness_Insert (record.Device_Info, record.Readiness_Rate, record.First_Cigs_After_Wakeup, record.Cigs_Per_Day, record.Non_Smoker, record.Answer_Date, uploadStatus);
		Debug.Log ("Local DB - Insert Readiness ID:" + recordID.ToString ());
	}

	public void updateReadinessOnLocalDB (int recordID)
	{
		var dbService = new DatabaseService ();
		dbService.Readiness_OfflineUpdate (recordID);
		Debug.Log ("Local DB - Update Readiness ID:" + recordID.ToString ());
	}

	/// <summary>
	/// Inserts OnboardingAnswer into Local DB.
	/// </summary>
	/// <param name="record">OnboardingAnswer data record.</param>
	/// <param name="uploadStatus">Upload status is 1 mean this record need upload to online database server.\n
	/// 0 mean this record already upload to online database server</param>
	private void insertOnboardingAnswerIntoLocalDB (OnboardingAnswer record, int uploadStatus)
	{
		DateTime curDateTime = DateTime.Now;
		var dbService = new DatabaseService ();
		int recordID = dbService.OnboardingAnswer_Insert (record.Device_Info, record.First_Cigs_After_Wakeup, record.Cigs_On_Yesterday, record.Triggered_Smoke, record.Relapse_Factor, record.Answer_Date, uploadStatus);
		Debug.Log ("Local DB - Insert OnboardingAnswer ID:" + recordID.ToString ());
	}

	public void updateOnboardingAnswerOnLocalDB (int recordID)
	{
		var dbService = new DatabaseService ();
		dbService.OnboardingAnswer_OfflineUpdate (recordID);
		Debug.Log ("Local DB - Update OnboardingAnswer ID:" + recordID.ToString ());
	}

	#endregion

	#region Postcode API Service

	public string Postcode = string.Empty;

	private IEnumerator GetPostcode (float latData, float longData, Action<string> setPostcodeCall)
	{
		using (UnityWebRequest www = UnityWebRequest.Get ("http://api.postcodes.io/postcodes?lon=" + longData.ToString () + "&lat=" + latData.ToString ())) {
			yield return www.SendWebRequest ();

			string jsonString = string.Empty;
			string temp_postcode = string.Empty;

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				setPostcodeCall (temp_postcode);
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				// Or retrieve results as binary data
				//byte[] results = www.downloadHandler.data;
				jsonString = www.downloadHandler.text;
				PostcodeResponse pResponse = JsonUtility.FromJson<PostcodeResponse> (jsonString);
				if (pResponse.status == "200") {
					//
					jsonString = jsonString.Replace ("{\"status\":200,\"result\":[", "");
					jsonString = jsonString.Replace ("]}", "");
					jsonString = jsonString.Substring (0, jsonString.IndexOf ("}},{")) + "}}";
					//
					PostcodeRecord pRecords = JsonUtility.FromJson<PostcodeRecord> (jsonString);
					temp_postcode = pRecords.postcode.Replace (" ", "");
					Debug.Log (temp_postcode);
					setPostcodeCall (temp_postcode);
				} else {
					temp_postcode = string.Empty;
					Debug.Log (temp_postcode);
					setPostcodeCall (temp_postcode);
				}
			}
		}
	}

	private void setPostcode (string inPostcode)
	{
		Debug.Log ("Get Postcode Success.");
		//
		Postcode = inPostcode;
	}

	#endregion

	#region Device Infomation

	/// <summary>
	/// Generateds the device info.
	/// </summary>
	/// <returns>The device info.</returns>
	public string generatedDeviceInfo ()
	{
		if (SystemInfo.unsupportedIdentifier != SystemInfo.deviceUniqueIdentifier) {
			return MD5Sum (SystemInfo.deviceUniqueIdentifier + secretKey);
		} else
			return string.Empty;
	}

	public string MD5Sum (string strToTncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ();
		byte[] bytes = ue.GetBytes (strToTncrypt);

		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ();
		byte[] hashBytes = md5.ComputeHash (bytes);

		// convert the encrypted bytes back to a string (base 16)
		string hashString = "";

		for (int i = 0; i < hashBytes.Length; i++) {
			hashString += System.Convert.ToString (hashBytes [i], 16).PadLeft (2, '0');
		}

		return hashString.PadLeft (32, '0');
	}

	private IEnumerator checkDeviceIsOnline (Action<bool> setDeviceStatusCall)
	{
		using (UnityWebRequest www = UnityWebRequest.Get ("https://www.healthygames.uk/endpoint.php")) {
			yield return www.SendWebRequest ();

			if (www.isNetworkError || www.isHttpError) {
				Debug.Log (www.error);
				setDeviceStatusCall (false);
			} else {
				// Show results as text
				Debug.Log (www.downloadHandler.text);
				// Or retrieve results as binary data
				//byte[] results = www.downloadHandler.data;
				if (www.downloadHandler.text == "1") {
					setDeviceStatusCall (true);
				} else {
					setDeviceStatusCall (false);
				}
			}
		}
	}

	private void setDeviceStatus (bool isOnline)
	{
		Debug.Log ("Check device's status success.");
		//
		_onlineStatus = isOnline;
	}

	#endregion

	#region Location Management

	public LocationInfo currentLocation;

	private IEnumerator getCurrentLocation (string state)
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			WriteLine ("Real location.");
			yield return StartCoroutine (startTrackingLocation ());
			WriteLine ("Current location:" + currentLocation.ToString ());
			yield return StartCoroutine (GetPostcode (currentLocation.Latitude, currentLocation.Longitude, setPostcode));
			WriteLine ("Postcode:" + Postcode);
			yield return StartCoroutine (LocationTracking_Insert (state));
		} else {
			// Mock up location.
			WriteLine ("Mock up location.");
			currentLocation = new LocationInfo ();
			WriteLine ("Current location:" + currentLocation.ToString ());
			yield return StartCoroutine (GetPostcode (currentLocation.Latitude, currentLocation.Longitude, setPostcode));
			WriteLine ("Postcode:" + Postcode);
			yield return StartCoroutine (LocationTracking_Insert (state));
		}
	}

	// Use this for initialization
	private IEnumerator startTrackingLocation ()
	{
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
			// Mock up location.
			currentLocation = new LocationInfo ();
			yield break;
		}

		// Start service before querying location
		Input.location.Start ();

		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}

		// Service didn't initialize in 20 seconds
		if (maxWait < 1) {
			Debug.Log ("LocationService time out.");
			// Mock up location.
			currentLocation = new LocationInfo ();
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.Log ("LocationService's status failed.");
			// Mock up location.
			currentLocation = new LocationInfo ();
		} else {
			Debug.Log ("LocationService's status success.");
			// Access granted and location value could be retrieved
			currentLocation = new LocationInfo (Input.location.lastData.latitude, Input.location.lastData.longitude);
		}

		// Stop service if there is no need to query location updates continuously
		Input.location.Stop ();
	}

	/// <summary>
	/// Inserts the location tracking.
	/// </summary>
	/// <returns>Insert live data will return record id. Insert offline data will return status</returns>
	/// <param name="deviceInfo">Device Unique Identifier.</param>
	/// <param name="latData">Latitude data.</param>
	/// <param name="longData">Longitude data.</param>
	/// <param name="recordDate">Record date.</param>
	/// <param name="comment">Comment.</param>
	/// <param name="isOffline">If this is offline data set value to 1, else 0.</param>
	private IEnumerator LocationTracking_Insert (string comment)
	{
		yield return checkDeviceIsOnline (setDeviceStatus);
		Debug.Log ("LocationTracking_Insert -Device is online: " + _onlineStatus.ToString ());
		if (_onlineStatus) {
			Debug.Log ("Call Coroutine to update online database server.");
			yield return StartCoroutine (LocationTracking_Insert_Online (comment, 0, insertLocationTrackingIntoLocalDB));
		} else {
			Debug.Log ("Call Coroutine to update local database.");
			insertLocationTrackingIntoLocalDB (comment, 1);
		}
	}

	#endregion

}






