﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System;

public class NetworkManager : MonoBehaviour
{
    private static NetworkManager instance;
    public static NetworkManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<NetworkManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.name = typeof(NetworkManager).Name;
                    instance = obj.AddComponent<NetworkManager>();
                }
            }
            return instance;
        }
    }

    public bool AccessViaNetwork { get; private set; }
    public bool AccessViaWiFi { get; private set; }
    public bool AccessToWebServer { get; private set; }

    // Awake is always called before any Start functions
    void Awake()
    {
        Debug.Log("NetworkManager - Awake() called.");
        // Check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        }
        // If instance already exists and it's not this:
        else if (instance != this)
        {
            // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    public void Init()
    {
        Debug.Log("NetworkManager - Init() called.");
        //
        CheckInternetReachability();
        //
        if (AccessViaNetwork || AccessViaWiFi)
        {
            CheckConnectionWithServer();
        }
        else
        {
            AccessToWebServer = false;
        }
    }

    private void CheckInternetReachability()
    {
        // Application.internetReachability returns the type of Internet reachability currently possible on the device.
        if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            // Network is reachable via carrier data network.
            AccessViaNetwork = true;
        }

        if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            // Network is reachable via WiFi or cable.
            AccessViaWiFi = true;
        }

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            // Network is not reachable.
            AccessViaNetwork = false;
            AccessViaWiFi = false;
        }
    }

    private void CheckConnectionWithServer()
    {
        // based on stackoverflow topic
        // http://answers.unity3d.com/questions/567497/how-to-100-check-internet-availability.html
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.healthygames.uk/endpoint.php");

        // Fixed HTTP WebRequest fails from Stackoverflow
        // https://stackoverflow.com/questions/4926676/mono-https-webrequest-fails-with-the-authentication-or-decryption-has-failed
        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;

        using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
        {
            bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
            if (isSuccess)
            {
                using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                {
                    html = reader.ReadToEnd();
                    //Debug.Log ("Response from web server :: " + html);
                }
            }
        }

        if (html == "1")
        {
            AccessToWebServer = true;
        }
        else
        {
            AccessToWebServer = false;
        }
    }

    private bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain,
        // look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    continue;
                }
                chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                bool chainIsValid = chain.Build((X509Certificate2)certificate);
                if (!chainIsValid)
                {
                    isOk = false;
                    break;
                }
            }
        }
        return isOk;
    }
}
