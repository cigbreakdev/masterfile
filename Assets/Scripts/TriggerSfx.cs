﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSfx : MonoBehaviour
{
    public AudioClip clip;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            GetComponent<AudioSource>().PlayOneShot(clip);
        }
    }
}
