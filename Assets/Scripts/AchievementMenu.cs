﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementMenu : MonoBehaviour {

    private string menuName = "Achievement";

    public Text menuText;

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called Order 1 - OnEnable called");
        // Bring this game object to the front
        transform.SetAsLastSibling();
        //
        DoSomeSetup();
    }

    // Called Order 2
    void DoSomeSetup()
    {
        Debug.Log("Called Order 2 - DoSomeSetup");
        //
        menuText.text = menuName;
    }

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        Debug.Log("Called Order 3 - Start");

    }

    // Called when the game is terminated. Called Order Last
    void OnDisable()
    {
        Debug.Log("Called when the game is terminated - OnDisable");
    }

    public void ClosePanel()
    {
        // Hide this game object
        gameObject.SetActive(false);
    }
}

