﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBackgroundMusic : MonoBehaviour
{
    public AudioClip main;
    public AudioClip aux;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            SoundManager.Instance.mainMusicAudioSource.clip = main;
            SoundManager.Instance.auxMusicAudioSource.clip = aux;
            SoundManager.Instance.mainMusicAudioSource.Play();
            SoundManager.Instance.auxMusicAudioSource.Play();
        }
    }
}
