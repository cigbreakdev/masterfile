﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial4 : MonoBehaviour {

    public GameObject journalPanel;

    public Color baseColor = new Color(255f, 255f, 255f, 100f);
    public Color swapColor = new Color(255f, 0f, 0f, 255f);

    private Image m_journalPanelImage;
    private bool calledFirst = false;

    // Use this for initialization
    void OnEnable()
    {
        Debug.Log("Tutorial called.");
        calledFirst = false;
        m_journalPanelImage = journalPanel.GetComponent<Image>();
    }

    void Update()
    {
        if(!calledFirst)
        {
            calledFirst = true;
            StartCoroutine(Blink());
        }
    }

    IEnumerator Blink()
    {
        if(m_journalPanelImage.color == baseColor)
        {
            m_journalPanelImage.color = swapColor;
        }
        else
        {
            m_journalPanelImage.color = baseColor;
        }
        yield return new WaitForSeconds(1.0f);
        StartCoroutine(Blink());
    }
}
