﻿using UnityEngine;
using System.Collections;

public class DisabledCanvas : MonoBehaviour {

	public Canvas debugCanvas = null;

	public void Close () {
		debugCanvas.enabled = false;
	}
}
