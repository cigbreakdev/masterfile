﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChoiceDetail : MonoBehaviour
{
    public GameObject triggerChoiceItemPrefab;
    public GameObject triggerOtherChoiceItemPrefab;

    private int m_triggerChoiceItemCount = 0;

    public void SetupTriggerChoiceDetail()
    {
        int newHeight = 0;

        m_triggerChoiceItemCount = SQLiteManager.Instance.RowsCount("TriggerChoice");
        Debug.Log("m_triggerChoiceItemCount:" + m_triggerChoiceItemCount);

        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
        Debug.Log("containerRectTransform's height:" + containerRectTransform.rect.height);

        TriggerChoiceItem current_item;
        for (int i = 1; i <= m_triggerChoiceItemCount; i++)
        {
            current_item = GetTriggerChoiceItem(i);
            if (current_item == null)
            {
                //create a new item, name it, and set the parent
                GameObject newItem = Instantiate(triggerChoiceItemPrefab) as GameObject;
                newItem.name = "TriggerChoiceItem_" + i;
                TriggerChoiceItem newTriggerChoiceItem = newItem.GetComponent<TriggerChoiceItem>();
                newTriggerChoiceItem.Setup(i);
                newItem.transform.SetParent(gameObject.transform, false);
                // Grid layout: height = 80
                newHeight += 80;
            }
            else
            {
                current_item.Setup(i);
            }
        }

        if (!IsTriggerOtherChoiceExist())
        {
            Debug.Log("Add the other choice.");
            // Add the other choice.
            //create a new item, name it, and set the parent
            GameObject otherChoiceItem = Instantiate(triggerOtherChoiceItemPrefab) as GameObject;
            otherChoiceItem.name = "TriggerOtherChoiceItem";
            TriggerOtherChoice newTriggerOtherChoiceItem = otherChoiceItem.GetComponent<TriggerOtherChoice>();
            newTriggerOtherChoiceItem.Setup();
            otherChoiceItem.transform.SetParent(gameObject.transform, false);
            // Grid layout: height = 80
            newHeight += 80;
        }

        /*
        if(newHeight > m_holderHieght)
        {
            containerRectTransform.sizeDelta.Set(containerRectTransform.rect.width, newHeight);
        }
        */
    }

    TriggerChoiceItem GetTriggerChoiceItem(int ID)
    {
        Component[] triggerChoiceItems;
        triggerChoiceItems = GetComponentsInChildren(typeof(TriggerChoiceItem));

        if (triggerChoiceItems == null)
        {
            return null;
        }
        else
        {
            foreach (TriggerChoiceItem item in triggerChoiceItems)
            {
                if (item.itemID == ID)
                {
                    return item;
                }
            }
            return null;
        }
    }

    bool IsTriggerOtherChoiceExist()
    {
        Component[] triggerOtherChoiceItems;
        triggerOtherChoiceItems = GetComponentsInChildren(typeof(TriggerOtherChoice));

        if (triggerOtherChoiceItems == null)
        {
            Debug.Log("TriggerOtherChoice does not found.");
            return false;
        }
        else
        {
            if (triggerOtherChoiceItems.Length > 0)
            {
                Debug.Log("TriggerOtherChoice found.");
                return true;
            }
            return false;
        }
    }

    public string GetTriggerAnswer()
    {
        string m_triggerAnswer = string.Empty;

        Component[] triggerChoiceItems;
        triggerChoiceItems = GetComponentsInChildren(typeof(TriggerChoiceItem));

        return m_triggerAnswer;
    }
}
