﻿using SQLite4Unity3d;

public class DailyAnswer
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }

    public int ProfileID { get; set; }

    public float FirstCigaretteAfterWakeup { get; set; }

    public int CigaretteOnYesterday { get; set; }

    public string TriggeredSmoke { get; set; }

    public string RelapseFactor { get; set; }

    public int JournalDay { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

    public override string ToString()
    {
        return string.Format("[DailyAnswer: ID={0}, ProfileID={1}, FirstCigaretteAfterWakeup={2}, " +
            "CigaretteOnYesterday={3}, TriggeredSmoke={4}, RelapseFactor={5}, JournalDay={6}, " +
            "CreateDate={7}, UpdateDate={8}, Status={9}]", 
            ID, ProfileID, FirstCigaretteAfterWakeup, CigaretteOnYesterday, TriggeredSmoke,
            RelapseFactor, JournalDay, CreateDate, UpdateDate, Status);
    }
}
