﻿using SQLite4Unity3d;

public class OnboardingAnswer
{
	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public string Device_Info { get; set; }

	public float First_Cigs_After_Wakeup { get; set; }

	public int Cigs_On_Yesterday { get; set; }

	public string Triggered_Smoke { get; set; }

	public string Relapse_Factor { get; set; }

	public string Answer_Date { get; set; }

	public int Need_Upload { get; set;}

	public override string ToString ()
	{
		return string.Format ("[OnboardingAnswer: ID={0}, Device_Info={1}, First_Cigs_After_Wakeup={2}, " +
			"Cigs_On_Yesterday={3}, Triggered_Smoke={4}, Relapse_Factor={5}, Answer_Date={6}, " +
			"Need_Upload={7}]", ID, Device_Info, First_Cigs_After_Wakeup, Cigs_On_Yesterday, Triggered_Smoke, 
			Relapse_Factor, Answer_Date, Need_Upload);
	}
}
