﻿using SQLite4Unity3d;

public class LevelPlayed
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }
    public int ProfileID { get; set; }
    public int Level { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public int Result { get; set; }
    public int PastResult { get; set; }
    public int RemainedLife { get; set; }
    public int TotalSwipe { get; set; }
    public int TotalTap { get; set; }
    public int CigaretteSpawn { get; set; }
    public int CigaretteBreak { get; set; }
    public int RollupSpawn { get; set; }
    public int RollupBreak { get; set; }
    public int CigarSpawn { get; set; }
    public int CigarBreak { get; set; }
    public int PipeSpawn { get; set; }
    public int PipeBreak { get; set; }
    public int CarrotSpawn { get; set; }
    public int CarrotBreak { get; set; }
    public int CarrotTap { get; set; }
    public int CelerySpawn { get; set; }
    public int CeleryBreak { get; set; }
    public int CeleryTap { get; set; }
    public int SpringOnionSpawn { get; set; }
    public int SpringOnionBreak { get; set; }
    public int SpringOnionTap { get; set; }
    public int ChiliSpawn { get; set; }
    public int ChiliBreak { get; set; }
    public int ChiliTap { get; set; }
    public int CornSpawn { get; set; }
    public int CornBreak { get; set; }
    public int CornTap { get; set; }
    public int BananaSpawn { get; set; }
    public int BananaBreak { get; set; }
    public int BananaTap { get; set; }
    public int BroccoliSpawn { get; set; }
    public int BroccoliBreak { get; set; }
    public int BroccoliTap { get; set; }
    public int OkraSpawn { get; set; }
    public int OkraBreak { get; set; }
    public int OkraTap { get; set; }
    public int SweetPotatoSpawn { get; set; }
    public int SweetPotatoBreak { get; set; }
    public int SweetPotatoTap { get; set; }
    public int DynamiteSpawn { get; set; }
    public int DynamiteBreak { get; set; }
    public int SprayUse { get; set; }
    public int GumUse { get; set; }
    public string CreateDate { get; set; }
    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

    public LevelPlayed()
    {
        ProfileID = -1;
        Level = -1;
        StartTime = string.Empty;
        EndTime = string.Empty;
        Result = 0;
        PastResult = -1;
        RemainedLife = 0;
        TotalSwipe = 0;
        TotalTap = 0;
        CigaretteSpawn = 0;
        CigaretteBreak = 0;
        RollupSpawn = 0;
        RollupBreak = 0;
        CigarSpawn = 0;
        CigarBreak = 0;
        PipeSpawn = 0;
        PipeBreak = 0;
        CarrotSpawn = 0;
        CarrotBreak = 0;
        CarrotTap = 0;
        CelerySpawn = 0;
        CeleryBreak = 0;
        CeleryTap = 0;
        SpringOnionSpawn = 0;
        SpringOnionBreak = 0;
        SpringOnionTap = 0;
        ChiliSpawn = 0;
        ChiliBreak = 0;
        ChiliTap = 0;
        CornSpawn = 0;
        CornBreak = 0;
        CornTap = 0;
        BananaSpawn = 0;
        BananaBreak = 0;
        BananaTap = 0;
        BroccoliSpawn = 0;
        BroccoliBreak = 0;
        BroccoliTap = 0;
        OkraSpawn = 0;
        OkraBreak = 0;
        OkraTap = 0;
        SweetPotatoSpawn = 0;
        SweetPotatoBreak = 0;
        SweetPotatoTap = 0;
        DynamiteSpawn = 0;
        DynamiteBreak = 0;
        SprayUse = 0;
        GumUse = 0;
        CreateDate = string.Empty;
        UpdateDate = string.Empty;
        Status = 1;
    }

    public override string ToString()
    {
        return string.Format("[LevelPlayed: ID={0}, ProfileID={1}, Level={2}, StartTime={3}, EndTime={4}, Result={5}, PastResult={51}," +
            " RemainedLife={6}," +
            " TotalSwipe={7}, TotalTap={8}, CigaretteSpawn={9}, CigaretteBreak={10}, RollupSpawn={11}, RollupBreak={12}, CigarSpawn={13}," +
            " CigarBreak={14}, PipeSpawn={15}, PipeBreak={16}, CarrotSpawn={17}, CarrotBreak={18}, CarrotTap={19}, CelerySpawn={20}," +
            " CeleryBreak={21}, CeleryTap={22}, SpringOnionSpawn={23}, SpringOnionBreak={24}, SpringOnionTap={25}, ChiliSpawn={26}," +
            " ChiliBreak={27}, ChiliTap={28}, CornSpawn={29}, CornBreak={30}, CornTap={31}, BananaSpawn={32}, BananaBreak={33}, BananaTap={34}," +
            " BroccoliSpawn={35}, BroccoliBreak={36}, BroccoliTap={37}, OkraSpawn={38}, OkraBreak={39}, OkraTap={40}, SweetPotatoSpawn={41}," +
            " SweetPotatoBreak={42}, SweetPotatoTap={43}, DynamiteSpawn={44}, DynamiteBreak={45}, SprayUse={46}, GumUse={47}, CreateDate={48}," +
            " UpdateDate={49}, Status={50}]", ID, ProfileID, Level, StartTime, EndTime, Result, RemainedLife, TotalSwipe, TotalTap,
            CigaretteSpawn, CigaretteBreak, RollupSpawn, RollupBreak, CigarSpawn, CigarBreak, PipeSpawn, PipeBreak, CarrotSpawn,
            CarrotBreak, CarrotTap, CelerySpawn, CeleryBreak, CeleryTap, SpringOnionSpawn, SpringOnionBreak, SpringOnionTap, ChiliSpawn,
            ChiliBreak, ChiliTap, CornSpawn, CornBreak, CornTap, BananaSpawn, BananaBreak, BananaTap, BroccoliSpawn, BroccoliBreak, BroccoliTap,
            OkraSpawn, OkraBreak, OkraTap, SweetPotatoSpawn, SweetPotatoBreak, SweetPotatoTap, DynamiteSpawn, DynamiteBreak, SprayUse,
            GumUse, CreateDate, UpdateDate, Status, PastResult);
    }

    public void UpdateSpawnedStats(string itemName)
    {
        switch (itemName)
        {
            case "Cigarette":
                CigaretteSpawn++;
                break;
            case "Rollup":
                RollupSpawn++;
                break;
            case "Cigar":
                CigarSpawn++;
                break;
            case "Pipe":
                PipeSpawn++;
                break;
            case "Carrot":
                CarrotSpawn++;
                break;
            case "Celery":
                CelerySpawn++;
                break;
            case "SpringOnion":
                SpringOnionSpawn++;
                break;
            case "Corn":
                CornSpawn++;
                break;
            case "Chili":
                ChiliSpawn++;
                break;
            case "Banana":
                BananaSpawn++;
                break;
            case "Broccoli":
                BroccoliSpawn++;
                break;
            case "Okra":
                OkraSpawn++;
                break;
            case "SweetPotato":
                SweetPotatoSpawn++;
                break;
            case "Dynamite":
                DynamiteSpawn++;
                break;
            default:
                break;
        }
    }

    public void UpdateBreakStats(string itemName)
    {
        switch (itemName)
        {
            case "Cigarette":
                CigaretteBreak++;
                break;
            case "Rollup":
                RollupBreak++;
                break;
            case "Cigar":
                CigarBreak++;
                break;
            case "Pipe":
                PipeBreak++;
                break;
            case "Carrot":
                CarrotBreak++;
                break;
            case "Celery":
                CeleryBreak++;
                break;
            case "SpringOnion":
                SpringOnionBreak++;
                break;
            case "Corn":
                CornBreak++;
                break;
            case "Chili":
                ChiliBreak++;
                break;
            case "Banana":
                BananaBreak++;
                break;
            case "Broccoli":
                BroccoliBreak++;
                break;
            case "Okra":
                OkraBreak++;
                break;
            case "SweetPotato":
                SweetPotatoBreak++;
                break;
            case "Dynamite":
                DynamiteBreak++;
                break;
            default:
                break;
        }
    }

    public void UpdateBreakStats(string itemName, int count)
    {
        switch (itemName)
        {
            case "Cigarette":
                CigaretteBreak += count;
                break;
            case "Rollup":
                RollupBreak += count;
                break;
            case "Cigar":
                CigarBreak += count;
                break;
            case "Pipe":
                PipeBreak += count;
                break;
            case "Carrot":
                CarrotBreak += count;
                break;
            case "Celery":
                CeleryBreak += count;
                break;
            case "SpringOnion":
                SpringOnionBreak += count;
                break;
            case "Corn":
                CornBreak += count;
                break;
            case "Chili":
                ChiliBreak += count;
                break;
            case "Banana":
                BananaBreak += count;
                break;
            case "Broccoli":
                BroccoliBreak += count;
                break;
            case "Okra":
                OkraBreak += count;
                break;
            case "SweetPotato":
                SweetPotatoBreak += count;
                break;
            case "Dynamite":
                DynamiteBreak += count;
                break;
            default:
                break;
        }
    }

    public void UpdateTapStats(string itemName)
    {
        switch (itemName)
        {
            case "Carrot":
                CarrotTap++;
                break;
            case "Celery":
                CeleryTap++;
                break;
            case "SpringOnion":
                SpringOnionTap++;
                break;
            case "Corn":
                CornTap++;
                break;
            case "Chili":
                ChiliTap++;
                break;
            case "Banana":
                BananaTap++;
                break;
            case "Broccoli":
                BroccoliTap++;
                break;
            case "Okra":
                OkraTap++;
                break;
            case "SweetPotato":
                SweetPotatoTap++;
                break;
            default:
                break;
        }
    }

    public void UpdateTapStats(string itemName, int count)
    {
        switch (itemName)
        {
            case "Carrot":
                CarrotTap += count;
                break;
            case "Celery":
                CeleryTap += count;
                break;
            case "SpringOnion":
                SpringOnionTap += count;
                break;
            case "Corn":
                CornTap += count;
                break;
            case "Chili":
                ChiliTap += count;
                break;
            case "Banana":
                BananaTap += count;
                break;
            case "Broccoli":
                BroccoliTap += count;
                break;
            case "Okra":
                OkraTap += count;
                break;
            case "SweetPotato":
                SweetPotatoTap += count;
                break;
            default:
                break;
        }
    }
}
