﻿using SQLite4Unity3d;

public class QuitMission {

	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public int GameDay { get; set; }

	public string Milestone { get; set; }

	public string MissionDetail { get; set; }

	public string MissionIcon { get; set; }

	public string RewardType { get; set; }

	public int Amount { get; set; }

	public override string ToString ()
	{
		return string.Format ("[QuitMission: ID={0}, GameDay={1}, Milestone={2}, MissionDetail={3}, MissionIcon={4}, RewardType={5}, Amount={6}]", ID, GameDay, Milestone, MissionDetail, MissionIcon, RewardType, Amount);
	}
}
