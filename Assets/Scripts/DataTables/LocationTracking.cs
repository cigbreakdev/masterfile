﻿using SQLite4Unity3d;

public class LocationTracking
{
	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public string Device_Info { get; set; }

	public float Latitude { get; set; }

	public float Longitude { get; set; }

	public string Record_Date { get; set; }

	public string Comment { get; set; }

	public int Need_Upload { get; set;}

	public override string ToString ()
	{
		return string.Format ("[LocationTracking: ID={0}, Device_Info={1}, Latitude={2}, Longitude={3}, Record_Date={4}, Comment={5}, Need_Upload={6}]", ID, Device_Info, Latitude, Longitude, Record_Date, Comment, Need_Upload);
	}
}
