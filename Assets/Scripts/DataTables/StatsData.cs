﻿using SQLite4Unity3d;

public class StatsData {

	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public string StatsTitle { get; set; }

	public string StatsDesc { get; set; }

	public string StatsParameter { get; set; }

	public override string ToString ()
	{
		return string.Format ("[StatsData: ID={0}, StatsTitle={1}, StatsDesc={2}, StatsParameter={3}]", ID, StatsTitle, StatsDesc, StatsParameter);
	}
}
