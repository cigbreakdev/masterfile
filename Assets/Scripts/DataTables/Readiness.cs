﻿using SQLite4Unity3d;

public class Readiness
{
	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public string Device_Info { get; set; }

	public float Readiness_Rate { get; set; }

	public float First_Cigs_After_Wakeup { get; set; }

	public int Cigs_Per_Day { get; set; }

	public int Non_Smoker { get; set; }

	public string Answer_Date { get; set; }

	public int Need_Upload { get; set;}

	public override string ToString ()
	{
		return string.Format ("[Readiness: ID={0}, Device_Info={1}, Readiness_Rate={2}, " +
			"First_Cigs_After_Wakeup={3}, Cigs_Per_Day={4}, Non_Smoker={5}, Answer_Date={6}, " +
			"Need_Upload={7}]", ID, Device_Info, Readiness_Rate, First_Cigs_After_Wakeup, 
			Cigs_Per_Day, Non_Smoker, Answer_Date, Need_Upload);
	}
}
