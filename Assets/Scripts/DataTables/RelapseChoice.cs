﻿using SQLite4Unity3d;

public class RelapseChoice
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }

    public string Detail { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

    public override string ToString()
    {
        return string.Format("[RelapseChoice: ID={0}, Detail={1}, " +
            "CreateDate={2}, UpdateDate={3}, Status={4}]",
            ID, Detail, CreateDate, UpdateDate, Status);
    }
}
