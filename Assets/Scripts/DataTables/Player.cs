﻿using SQLite4Unity3d;

public class Player
{
	[PrimaryKey, AutoIncrement]
	public int PlayerID { get; set; }
	// This is online parameter, it will update when the device online first times.
	public int OnlinePlayerID { get; set; }

	public string PlayerName { get; set; }

	public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

	public override string ToString ()
	{
		return string.Format ("[Player: PlayerID={0}, OnlinePlayerID={1}, PlayerName={2}, CreateDate={3}, UpdateDate={4}, Status={5}]", 
            PlayerID, OnlinePlayerID, PlayerName, CreateDate, UpdateDate, Status);
	}

	public Player ()
	{
		PlayerID = -1;
		OnlinePlayerID = -1;
        // Need upload to server
        Status = 1;
	}
}
