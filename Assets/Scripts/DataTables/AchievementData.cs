﻿using SQLite4Unity3d;

public class AchievementData
{

    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }

    public string Category { get; set; }

    public string Name { get; set; }

    public string Wording { get; set; }

    public int Stars { get; set; }

    public int Goal { get; set; }

    public string Variable_Name { get; set; }

    public override string ToString()
    {
        return string.Format("[AchievementData: ID={0}, Category={1}, Name={2}, " +
            "Wording={3}, Stars={4}, Goal={5}, Variable_Name={6}]", 
            ID, Category, Name, Wording, Stars, Goal, Variable_Name);
    }
}



