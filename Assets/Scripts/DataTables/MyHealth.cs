﻿using SQLite4Unity3d;

public class MyHealth
{
    [PrimaryKey, AutoIncrement]
    public int ID { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public int Minutes { get; set; }

    public MyHealth()
    {
        ID = -1;
        Title = string.Empty;
        Description = string.Empty;
        Minutes = 0;
    }

    public override string ToString()
    {
        return string.Format("[MyHealth: ID={0}, Title={1}, Description={2}, Minutes={3}]", ID, Title, Description, Minutes);
    }
}
