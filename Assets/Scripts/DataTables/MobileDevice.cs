﻿using SQLite4Unity3d;

public class MobileDevice
{
    [PrimaryKey, AutoIncrement]
    public int DeviceID { get; set; }
    // This is online parameter, it will update when the device online first times.
    public int OnlineDeviceID { get; set; }

    public string DeviceInfo { get; set; }

    public string Platform { get; set; }

    public string Model { get; set; }

    public string DeviceOS { get; set; }

    public string AccelerometerSupported { get; set; }

    public string GyroscopeSupported { get; set; }

    public string LocationServiceSupported { get; set; }

    public string ScreenResolution { get; set; }

    public int ScreenWidth { get; set; }

    public int ScreenHeight { get; set; }

    public float ScreenDPI { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

    public override string ToString()
    {
        return string.Format("[MobileDevice: DeviceID={0}, OnlineDeviceID={1}, DeviceInfo={2}, Platform={3}, " +
            "Model={4}, DeviceOS={5}, AccelerometerSupported={6}, GyroscopeSupported={7}, LocationServiceSupported={8}, " +
            "ScreenResolution={9}, ScreenWidth(pixels)={10}, ScreenHeight(pixels)={11}, ScreenDPI={12}, CreateDate={13}, " +
            "UpdateDate={14}, Status={15}]",
            DeviceID, OnlineDeviceID, DeviceInfo, Platform, Model, DeviceOS, AccelerometerSupported, GyroscopeSupported,
            LocationServiceSupported, ScreenResolution, ScreenWidth, ScreenHeight, ScreenDPI, CreateDate, UpdateDate, Status);
    }

    public MobileDevice()
    {
        DeviceID = -1;
        OnlineDeviceID = -1;
        // Need upload to server.
        Status = 1;
    }
}
