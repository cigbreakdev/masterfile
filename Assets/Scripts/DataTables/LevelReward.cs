﻿using SQLite4Unity3d;

public class LevelReward {

	[PrimaryKey]
	public int Level_ID { get; set; }

	public int Coins { get; set; }

	public int Carrot { get; set; }

	public int Celery { get; set; }

	public int Spring_Onion { get; set; }

	public int Corn { get; set; }

	public int Chili { get; set; }

	public int Banana { get; set; }

	public int Spray { get; set; }

	public int Gum { get; set; }

	public int Patch { get; set; }

	public LevelReward() {
		Level_ID = -1;
		Coins = 0;
		Carrot = 0;
		Celery = 0;
		Spring_Onion = 0;
		Corn = 0;
		Chili = 0;
		Banana = 0;
		Spray = 0;
		Gum = 0;
		Patch = 0;
	}

	public override string ToString ()
	{
		return string.Format ("[LevelReward: Level_ID={0}, Coins={1}, Carrot={2}, Celery={3}, Spring_Onion={4}, Corn={5}, Chili={6}, Banana={7}, Spray={8}, Gum={9}, Patch={10}]", Level_ID, Coins, Carrot, Celery, Spring_Onion, Corn, Chili, Banana, Spray, Gum, Patch);
	}
}
