﻿using SQLite4Unity3d;

public class Login {
	
	[PrimaryKey, AutoIncrement]
	public int LoginID { get; set; }

	public int PlayerID { get; set; }
	// This is online parameter, it will update when the device online first times.
	public int OnlinePlayerID { get; set; }

	public int DeviceID { get; set; }
	// This is online parameter, it will update when the device online first times.
	public int OnlineDeviceID { get; set; }

	public string StartTime { get; set; }

	public float StartLat { get; set; }

	public float StartLong { get; set; }

	public string CommentS { get; set; }

	public string EndTime { get; set; }

	public float EndLat { get; set; }

	public float EndLong { get; set; }

	public string CommentE { get; set; }

	public string CreateDate { get; set; }

	public string UpdateDate { get; set; }
	// When status = 0 means its online, status > 1 means need update to the online server.
	public int Status { get; set; }

	public override string ToString ()
	{
		return string.Format ("[Login: LoginID={0}, PlayerID={1}, DeviceID={2}, StartTime={3}, StartLat={4}, StartLong={5}, CommentS={6}, EndTime={7}, EndLat={8}, EndLong={9}, CommentE={10}, CreateDate={11}, UpdateDate={12}, Status={13}]", LoginID, PlayerID, DeviceID, StartTime, StartLat, StartLong, CommentS, EndTime, EndLat, EndLong, CommentE, CreateDate, UpdateDate, Status);
	}
}