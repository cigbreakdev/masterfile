﻿using SQLite4Unity3d;

public class VersionControl
{
	[PrimaryKey, AutoIncrement]
	public int ID { get; set; }

	public string Version { get; set; }

	public string Release_Date { get; set; }

	public string Comment { get; set; }

	public override string ToString ()
	{
		return string.Format ("[VersionControl: ID={0}, Version={1}, Release_Date={2}, Comment={3}]", ID, Version, Release_Date, Comment);
	}
}
