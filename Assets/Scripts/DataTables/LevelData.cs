﻿using SQLite4Unity3d;

public class LevelData {

	[PrimaryKey]
	public int Level_ID { get; set; }

	public int Goal { get; set; }

	public int Has_Tutorial { get; set; }

	public string Tutorial_Name { get; set; }

	public string Start_Image { get; set; }

	public string Start_Text { get; set; }

	public float Min_Torque { get; set; }

	public float Max_Torque { get; set; }

	public float First_Spawn_Delay { get; set; }

	public float Time_Before_Repeating { get; set; }

	public float Cigarette_SpawnChance { get; set; }

	public float Rollup_SpawnChance { get; set; }

	public float Cigar_SpawnChance { get; set; }

	public float Pipe_SpawnChance { get; set; }

	public float Carrot_SpawnChance { get; set; }

	public float Celery_SpawnChance { get; set; }

	public float SpringOnion_SpawnChance { get; set; }

	public float Chili_SpawnChance { get; set; }

	public float Corn_SpawnChance { get; set; }

	public float Banana_SpawnChance { get; set; }

	public float Dynamite_SpawnChance { get; set; }

	public float Lollipop_SpawnChance { get; set; }

	public override string ToString ()
	{
		return string.Format ("[LevelData: Level_ID={0}, Goal={1}, Has_Tutorial={2}, Tutorial_Name={3}, Start_Image={4}, Start_Text={5}, Min_Torque={6}, Max_Torque={7}, First_Spawn_Delay={8}, Time_Before_Repeating={9}, Cigarette_SpawnChance={10}, Rollup_SpawnChance={11}, Cigar_SpawnChance={12}, Pipe_SpawnChance={13}, Carrot_SpawnChance={14}, Celery_SpawnChance={15}, SpringOnion_SpawnChance={16}, Chili_SpawnChance={17}, Corn_SpawnChance={18}, Banana_SpawnChance={19}, Dynamite_SpawnChance={20}, Lollipop_SpawnChance={21}]", Level_ID, Goal, Has_Tutorial, Tutorial_Name, Start_Image, Start_Text, Min_Torque, Max_Torque, First_Spawn_Delay, Time_Before_Repeating, Cigarette_SpawnChance, Rollup_SpawnChance, Cigar_SpawnChance, Pipe_SpawnChance, Carrot_SpawnChance, Celery_SpawnChance, SpringOnion_SpawnChance, Chili_SpawnChance, Corn_SpawnChance, Banana_SpawnChance, Dynamite_SpawnChance, Lollipop_SpawnChance);
	}
}
