﻿using SQLite4Unity3d;

public class Profile
{
    [PrimaryKey, AutoIncrement]
    public int ProfileID { get; set; }
    // This is online parameter, it will update when the device online first times.
    public int OnlineProfileID { get; set; }
    public int DeviceID { get; set; }
    // This is online parameter, it will update when the device online first times.
    public int OnlineDeviceID { get; set; }
    public int PlayerID { get; set; }
    // This is online parameter, it will update when the device online first times.
    public int OnlinePlayerID { get; set; }

    public float ReadinessRate { get; set; }

    public int CigarettePerDay { get; set; }

    public float FirstCigaretteAfterWakeup { get; set; }

    public int IsNonSmoker { get; set; }
    // Start date is the same as answer date.
    public string StartDate { get; set; }
    public string LastLoginDate { get; set; }

    public int FirstTimeOfDay { get; set; }

    public int TotalLoginDays { get; set; }
    public int TotalLoginCounts { get; set; }
    public int ConsecutiveLogin { get; set; }

    public int StarsFromSmokeless { get; set; }
    public int StarsFromLevelCleared { get; set; }
    public int TotalStar { get; set; }

    public int CurrentCoin { get; set; }
    public int TotalCoin { get; set; }

    public int CurrentCarrot { get; set; }
    public int SoldCarrot { get; set; }
    public int TotalCarrot { get; set; }
    public int TappedCarrot { get; set; }
    public int SwipedCarrot { get; set; }

    public int CurrentCelery { get; set; }
    public int SoldCelery { get; set; }
    public int TotalCelery { get; set; }
    public int TappedCelery { get; set; }
    public int SwipedCelery { get; set; }

    public int CurrentSpringOnion { get; set; }
    public int SoldSpringOnion { get; set; }
    public int TotalSpringOnion { get; set; }
    public int TappedSpringOnion { get; set; }
    public int SwipedSpringOnion { get; set; }

    public int CurrentCorn { get; set; }
    public int SoldCorn { get; set; }
    public int TotalCorn { get; set; }
    public int TappedCorn { get; set; }
    public int SwipedCorn { get; set; }

    public int CurrentChili { get; set; }
    public int SoldChili { get; set; }
    public int TotalChili { get; set; }
    public int TappedChili { get; set; }
    public int SwipedChili { get; set; }

    public int CurrentBanana { get; set; }
    public int SoldBanana { get; set; }
    public int TotalBanana { get; set; }
    public int TappedBanana { get; set; }
    public int SwipedBanana { get; set; }

    public int CurrentBroccoli { get; set; }
    public int SoldBroccoli { get; set; }
    public int TotalBroccoli { get; set; }
    public int TappedBroccoli { get; set; }
    public int SwipedBroccoli { get; set; }

    public int CurrentOkra { get; set; }
    public int SoldOkra { get; set; }
    public int TotalOkra { get; set; }
    public int TappedOkra { get; set; }
    public int SwipedOkra { get; set; }

    public int CurrentSweetPotato { get; set; }
    public int SoldSweetPotato { get; set; }
    public int TotalSweetPotato { get; set; }
    public int TappedSweetPotato { get; set; }
    public int SwipedSweetPotato { get; set; }

    public int TotalBrokeCigarette { get; set; }
    public int SwipedCigarette { get; set; }

    public int TotalBrokeRollup { get; set; }
    public int SwipedRollup { get; set; }

    public int TotalBrokeCigar { get; set; }
    public int SwipedCigar { get; set; }

    public int TotalBrokePipe { get; set; }
    public int SwipedPipe { get; set; }

    public int TotalBrokeDynamite { get; set; }
    public int SwipedDynamite { get; set; }

    public int CurrentSpray { get; set; }
    public int TotalSpray { get; set; }
    public int UsedSpray { get; set; }
    public int BoughtSpray { get; set; }

    public int CurrentGum { get; set; }
    public int TotalGum { get; set; }
    public int UsedGum { get; set; }
    public int BoughtGum { get; set; }

    public int TotalLevelPassed { get; set; }

    public int TotalLevelPassed3Stars { get; set; }

    public int TotalMapCleared { get; set; }

    public int TotalJournalCompleted { get; set; }

    public int TotalMissionCompleted { get; set; }

    public int TotalSoldVeggies { get; set; }

    public string CreateDate { get; set; }

    public string UpdateDate { get; set; }
    // When status = 0 means its online, status > 1 means need update to the online server.
    public int Status { get; set; }

	public override string ToString ()
	{
		return string.Format ("[Profile: ProfileID={0}, OnlineProfileID={1}, DeviceID={2}, OnlineDeviceID={3}, PlayerID={4}, OnlinePlayerID={5}, " +
			"ReadinessRate={6}, CigarettePerDay={7}, FirstCigaretteAfterWakeup={8}, IsNonSmoker={9}, StartDate={10}, LastLoginDate={11}, " +
			"FirstTimeOfDay={12}, TotalLoginDays={13}, TotalLoginCounts={14}, ConsecutiveLogin={15}, StarsFromSmokeless={16}, " +
			"StarsFromLevelCleared={17}, TotalStar={18}, CurrentCoin={19}, TotalCoin={20}, CurrentCarrot={21}, SoldCarrot={22}, " +
			"TotalCarrot={23}, TappedCarrot={24}, SwipedCarrot={25}, CurrentCelery={26}, SoldCelery={27}, TotalCelery={28}, " +
			"TappedCelery={29}, SwipedCelery={30}, CurrentSpringOnion={31}, SoldSpringOnion={32}, TotalSpringOnion={33}, " +
			"TappedSpringOnion={34}, SwipedSpringOnion={35}, CurrentCorn={36}, SoldCorn={37}, TotalCorn={38}, TappedCorn={39}, " +
			"SwipedCorn={40}, CurrentChili={41}, SoldChili={42}, TotalChili={43}, TappedChili={44}, SwipedChili={45}, " +
			"CurrentBanana={46}, SoldBanana={47}, TotalBanana={48}, TappedBanana={49}, SwipedBanana={50}, CurrentBroccoli={51}, " +
			"SoldBroccoli={52}, TotalBroccoli={53}, TappedBroccoli={54}, SwipedBroccoli={55}, CurrentOkra={56}, SoldOkra={57}, " +
			"TotalOkra={58}, TappedOkra={59}, SwipedOkra={60}, CurrentSweetPotato={61}, SoldSweetPotato={62}, TotalSweetPotato={63}, " +
			"TappedSweetPotato={64}, SwipedSweetPotato={65}, TotalBrokeCigarette={66}, SwipedCigarette={67}, TotalBrokeRollup={68}, " +
			"SwipedRollup={69}, TotalBrokeCigar={70}, SwipedCigar={71}, TotalBrokePipe={72}, SwipedPipe={73}, TotalBrokeDynamite={74}, " +
			"SwipedDynamite={75}, CurrentSpray={76}, TotalSpray={77}, UsedSpray={78}, BoughtSpray={79}, CurrentGum={80}, TotalGum={81}, " +
			"UsedGum={82}, BoughtGum={83}, TotalLevelPassed={84}, TotalLevelPassed3Stars={85}, TotalMapCleared={86}, TotalJournalCompleted={87}, " +
			"TotalMissionCompleted={88}, TotalSoldVeggies={89}, CreateDate={90}, UpdateDate={91}, Status={92}]", 
			ProfileID, OnlineProfileID, DeviceID, OnlineDeviceID, PlayerID, OnlinePlayerID, ReadinessRate, CigarettePerDay, 
			FirstCigaretteAfterWakeup, IsNonSmoker, StartDate, LastLoginDate, FirstTimeOfDay, TotalLoginDays, TotalLoginCounts, 
			ConsecutiveLogin, StarsFromSmokeless, StarsFromLevelCleared, TotalStar, CurrentCoin, TotalCoin, CurrentCarrot, SoldCarrot, 
			TotalCarrot, TappedCarrot, SwipedCarrot, CurrentCelery, SoldCelery, TotalCelery, TappedCelery, SwipedCelery, CurrentSpringOnion, 
			SoldSpringOnion, TotalSpringOnion, TappedSpringOnion, SwipedSpringOnion, CurrentCorn, SoldCorn, TotalCorn, TappedCorn, 
			SwipedCorn, CurrentChili, SoldChili, TotalChili, TappedChili, SwipedChili, CurrentBanana, SoldBanana, TotalBanana, 
			TappedBanana, SwipedBanana, CurrentBroccoli, SoldBroccoli, TotalBroccoli, TappedBroccoli, SwipedBroccoli, CurrentOkra, 
			SoldOkra, TotalOkra, TappedOkra, SwipedOkra, CurrentSweetPotato, SoldSweetPotato, TotalSweetPotato, TappedSweetPotato, 
			SwipedSweetPotato, TotalBrokeCigarette, SwipedCigarette, TotalBrokeRollup, SwipedRollup, TotalBrokeCigar, SwipedCigar, 
			TotalBrokePipe, SwipedPipe, TotalBrokeDynamite, SwipedDynamite, CurrentSpray, TotalSpray, UsedSpray, BoughtSpray, CurrentGum, 
			TotalGum, UsedGum, BoughtGum, TotalLevelPassed, TotalLevelPassed3Stars, TotalMapCleared, TotalJournalCompleted, 
			TotalMissionCompleted, TotalSoldVeggies, CreateDate, UpdateDate, Status);
	}

    public Profile()
    {
        ProfileID = -1;
        OnlineProfileID = -1;
        DeviceID = -1;
        OnlineDeviceID = -1;
        PlayerID = -1;
        OnlinePlayerID = -1;
        // This is new profile record setup.
        FirstTimeOfDay = 1;
        TotalLoginDays = 0;
        TotalLoginCounts = 0;
        ConsecutiveLogin = 0;
        // Need upload to server.
        Status = 1;
    }

    public Profile(ProfileData data, Profile current)
    {
        ProfileID = current.ProfileID;
        OnlineProfileID = int.Parse(data.ProfileID);
        DeviceID = current.DeviceID;
        OnlineDeviceID = int.Parse(data.DeviceID);
        PlayerID = current.PlayerID;
        OnlinePlayerID = int.Parse(data.PlayerID);
        ReadinessRate = float.Parse(data.ReadinessRate);
        CigarettePerDay = int.Parse(data.CigarettePerDay);
        FirstCigaretteAfterWakeup = float.Parse(data.FirstCigAfterWakeup);
        IsNonSmoker = int.Parse(data.IsNonSmoker);
        StartDate = data.StartDate;
        LastLoginDate = data.LastLoginDate;
        FirstTimeOfDay = int.Parse(data.FirstTimeOfDay);
        TotalLoginDays = int.Parse(data.TotalLoginDays);
        TotalLoginCounts = int.Parse(data.TotalLoginCounts);
        ConsecutiveLogin = int.Parse(data.ConsecutiveLogin);
        Status = 0;
    }

    public Profile(string profileString)
    {
        OnlineProfileID = -1;
        OnlineDeviceID = -1;
        OnlinePlayerID = -1;

        string[] data = profileString.Split('|');
        if (data[0] != "")
        {
            OnlineProfileID = int.Parse(data[0]);
        }

        if (data[1] != "")
        {
            OnlineDeviceID = int.Parse(data[1]);
        }

        if (data[2] != "")
        {
            OnlinePlayerID = int.Parse(data[2]);
        }

        if (data[3] != "")
        {
            ReadinessRate = float.Parse(data[3]);
        }

        if (data[4] != "")
        {
            CigarettePerDay = int.Parse(data[4]);
        }

        if (data[5] != "")
        {
            FirstCigaretteAfterWakeup = float.Parse(data[5]);
        }

        if (data[6] != "")
        {
            IsNonSmoker = int.Parse(data[6]);
        }

        if (data[7] != "")
        {
            StartDate = data[7];
        }

        if (data[8] != "")
        {
            LastLoginDate = data[8];
        }

        if (data[9] != "")
        {
            FirstTimeOfDay = int.Parse(data[9]);
        }

        if (data[10] != "")
        {
            TotalLoginDays = int.Parse(data[10]);
        }

        if (data[11] != "")
        {
            TotalLoginCounts = int.Parse(data[11]);
        }

        if (data[12] != "")
        {
            ConsecutiveLogin = int.Parse(data[12]);
        }

        if (data.Length > 0)
        {
            Status = 0;
        }
    }

    public void updateProfile(LevelPlayed record)
    {
        if (record.PastResult != -1)
        {
            // If current result is greater than past result, add up the star
            if (record.Result > record.PastResult)
            {
                StarsFromLevelCleared += record.Result - record.PastResult;
                TotalStar += record.Result - record.PastResult;

                if (record.Result == 3)
                {
                    TotalLevelPassed3Stars++;
                }
            }
        }
        else
        {
            StarsFromLevelCleared += record.Result;
            TotalStar += record.Result;
            TotalLevelPassed++;
            // 20180722 - Change coin reward method. (1 star mean 1 coin)
            // [Removed] First time cleared, get 1 coin.
            // CurrentCoin++;
            // TotalCoin++;

            if (record.Result == 3)
            {
                TotalLevelPassed3Stars++;
            }

            if (record.Level == 14 || record.Level == 27 || record.Level == 42 || record.Level == 60)
            {
                TotalMapCleared++;
            }
        }

        // Calculate coin
        ReceivedCoin(record.Result);

        TotalBrokeCigarette += record.CigaretteBreak;
        TotalBrokeRollup += record.RollupBreak;
        TotalBrokeCigar += record.CigarBreak;
        TotalBrokePipe += record.PipeBreak;
        TotalBrokeDynamite += record.DynamiteBreak;

        UsedSpray += record.SprayUse;
        UsedGum += record.GumUse;

        CurrentCarrot += record.CarrotTap;
        TotalCarrot += record.CarrotTap;
        TappedCarrot += record.CarrotTap;

        CurrentCelery += record.CeleryTap;
        TotalCelery += record.CeleryTap;
        TappedCelery += record.CeleryTap;

        CurrentSpringOnion += record.SpringOnionTap;
        TotalSpringOnion += record.SpringOnionTap;
        TappedSpringOnion += record.SpringOnionTap;

        CurrentCorn += record.CornTap;
        TotalCorn += record.CornTap;
        TappedCorn += record.CornTap;

        CurrentChili += record.ChiliTap;
        TotalChili += record.ChiliTap;
        TappedChili += record.ChiliTap;

        CurrentBanana += record.BananaTap;
        TotalBanana += record.BananaTap;
        TappedBanana += record.BananaTap;
    }

    private void ReceivedCoin(int result)
    {
        CurrentCoin += result;
        TotalCoin += result;
    }

    public void updateProfile(LevelReward reward)
    {
        CurrentCoin += reward.Coins;
        TotalCoin += reward.Coins;
        CurrentCarrot += reward.Carrot;
        TotalCarrot += reward.Carrot;
        CurrentCelery += reward.Celery;
        TotalCelery += reward.Celery;
        CurrentSpringOnion += reward.Spring_Onion;
        TotalSpringOnion += reward.Spring_Onion;
        CurrentChili += reward.Chili;
        TotalChili += reward.Chili;
        CurrentCorn += reward.Corn;
        TotalCorn += reward.Corn;
        CurrentBanana += reward.Banana;
        TotalBanana += reward.Banana;
        CurrentSpray += reward.Spray;
        TotalSpray += reward.Spray;
        CurrentGum += reward.Gum;
        TotalGum += reward.Gum;
    }

    public int getStatsValue(string name)
    {
        switch (name)
        {
            case "Total_Login_Days":
                return TotalLoginDays;
            case "Consecutive_Login":
                return ConsecutiveLogin;
            case "Stars_From_Smokeless":
                return StarsFromSmokeless;
            case "Total_Coins":
                return TotalCoin;
            case "Total_Level_Passed":
                return TotalLevelPassed;
            case "Total_Broke_Cigarettes":
                return TotalBrokeCigarette;
            default:
                return 0;
        }
    }

    public void SoldBoxofVegetables()
    {
        CurrentCoin++;
        TotalCoin++;
    }
}
