﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial5 : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;

    public GameObject dynamite;
    public Sprite complete;
    public Sprite breaked;

    // Use this for initialization
    void OnEnable()
    {
        Debug.Log("Tutorial called.");
        gameObject.transform.position = startPoint.position;
        gameObject.GetComponent<Mask>().showMaskGraphic = false;
        startTime = Time.time;
        journeyLength = Vector3.Distance(startPoint.position, endPoint.position);
        dynamite.GetComponent<Image>().sprite = complete;
        // Calculate speed
        speed = journeyLength / 1.5f;
    }

    private void Update()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(startPoint.position, endPoint.position, fracJourney);
        yield return null;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VisibleArea")
        {
            Debug.Log("Visible Enter");
            gameObject.GetComponent<Mask>().showMaskGraphic = true;
        }

        if (collision.gameObject.name == "Dynamite")
        {
            Debug.Log("Break");
            dynamite.GetComponent<Image>().sprite = breaked;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name == "VisibleArea")
        {
            Debug.Log("Visible Exit");
            gameObject.transform.position = startPoint.position;
            gameObject.GetComponent<Mask>().showMaskGraphic = false;
            startTime = Time.time;
            journeyLength = Vector3.Distance(startPoint.position, endPoint.position);
            dynamite.GetComponent<Image>().sprite = complete;
            StartCoroutine(Move());
        }
    }
}
