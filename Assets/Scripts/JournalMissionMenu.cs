﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalMissionMenu : MonoBehaviour
{
    public GameObject canvas;
    public GameObject[] journalPopupPrefab;
    private GameObject m_journalPopup;

    public void ShowSetYourQuitDate()
    {
        // How to get journal day or current mission
        m_journalPopup = Instantiate(journalPopupPrefab[0]) as GameObject;
        m_journalPopup.name = "JournalMission_0";
        m_journalPopup.SetActive(true);
        m_journalPopup.transform.SetParent(canvas.transform, false);
    }
}
