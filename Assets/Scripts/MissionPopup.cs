﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionPopup : MonoBehaviour
{
    private string menuName = "Mission";
    public Text menuText;

    //
    public int missionID;
    public int isTodayMission;
    //
    public Text titleText;
    public Image missionImage;
    public Text descriptionText;

    // Called Order 1
    void OnEnable()
    {
        // Bring this game object to the front
        transform.SetAsLastSibling();
        //
        menuText.text = menuName;
        //
        missionID = PlayerPrefs.GetInt("MissionID", -1);
        isTodayMission = PlayerPrefs.GetInt("IsTodayMission", -1);
        //
        Setup();
    }

    void Setup()
    {
        QuitMission record = SQLiteManager.Instance.GetMyMissionItem(missionID);
        //
        titleText.text = "Day " + record.GameDay.ToString();
        //missionImage.sprite = ResourceManager.Instance.GetDailyMissionSprite(record.GameDay);
        descriptionText.text = record.MissionDetail;
    }

    public void ClosePanel()
    {
        // Hide this game object
        gameObject.SetActive(false);
    }
}
