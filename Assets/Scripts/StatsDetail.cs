﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsDetail : MonoBehaviour {

    public GameObject myStatsItemPrefab;

    private int m_holderHieght = 420;
    private int m_statsItemCount = 0;

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called StatsDetail - OnEnable");
        SetupStatsDetail();
    }

    void SetupStatsDetail()
    {
        int newHeight = 0;

        m_statsItemCount = SQLiteManager.Instance.RowsCount("StatsData");
        Debug.Log("m_statsItemCount:" + m_statsItemCount);

        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
        Debug.Log("containerRectTransform's height:" + containerRectTransform.rect.height);

        MyStatsItem current_item;
        for (int i = 1; i < m_statsItemCount; i++)
        {
            current_item = GetMyStatsItem(i);
            if (current_item == null)
            {
                //create a new item, name it, and set the parent
                GameObject newItem = Instantiate(myStatsItemPrefab) as GameObject;
                newItem.name = "StatsItem_" + i;
                MyStatsItem newStatsItem = newItem.GetComponent<MyStatsItem>();
                newStatsItem.Setup(i);
                newItem.transform.SetParent(gameObject.transform, false);

                newHeight += newStatsItem.height + 20;
            }
            else
            {
                current_item.Setup(i);
            }
        }

        /*
        if(newHeight > m_holderHieght)
        {
            containerRectTransform.sizeDelta.Set(containerRectTransform.rect.width, newHeight);
        }
        */
    }

    MyStatsItem GetMyStatsItem(int ID)
    {
        Component[] myStatsItems;
        myStatsItems = GetComponentsInChildren(typeof(MyStatsItem));

        if (myStatsItems == null)
        {
            return null;
        }
        else
        {
            foreach (MyStatsItem item in myStatsItems)
            {
                if (item.itemID == ID)
                {
                    return item;
                }
            }
            return null;
        }

    }
}
