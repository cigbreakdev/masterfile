﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionDetail : MonoBehaviour {

    public GameObject myMissionItemPrefab;
    public GameObject missionPopup;

    private int m_holderHieght = 420;
    private int m_missionItemCount = 0;

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called MissionDetail - OnEnable");
        SetupMissionDetail();
    }

    void SetupMissionDetail()
    {
        int newHeight = 0;

        m_missionItemCount = SQLiteManager.Instance.RowsCount("QuitMission");
        Debug.Log("m_statsItemCount:" + m_missionItemCount);

        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
        Debug.Log("containerRectTransform's height:" + containerRectTransform.rect.height);

        MyMissionItem current_item;
        for (int i = 1; i < m_missionItemCount; i++)
        {
            current_item = GetMyMissionItem(i);
            if (current_item == null)
            {
                //create a new item, name it, and set the parent
                GameObject newItem = Instantiate(myMissionItemPrefab) as GameObject;
                newItem.name = "MissionItem_" + i;
                MyMissionItem newMissionItem = newItem.GetComponent<MyMissionItem>();
                newMissionItem.Setup(i, missionPopup);
                newItem.transform.SetParent(gameObject.transform, false);

                newHeight += newMissionItem.height + 20;
            }
            else
            {
                current_item.Setup(i, missionPopup);
            }
        }

        /*
        if(newHeight > m_holderHieght)
        {
            containerRectTransform.sizeDelta.Set(containerRectTransform.rect.width, newHeight);
        }
        */
    }

    MyMissionItem GetMyMissionItem(int ID)
    {
        Component[] myMissionItems;
        myMissionItems = GetComponentsInChildren(typeof(MyMissionItem));

        if (myMissionItems == null)
        {
            return null;
        }
        else
        {
            foreach (MyMissionItem item in myMissionItems)
            {
                if (item.itemID == ID)
                {
                    return item;
                }
            }
            return null;
        }

    }
}
