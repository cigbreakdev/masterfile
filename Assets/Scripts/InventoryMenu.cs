﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryMenu : MonoBehaviour {

    private string menuName = "Inventory";

    public Text menuText;

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called InventoryMenu - OnEnable");
        // Bring this game object to the front
        transform.SetAsLastSibling();
        //
        DoSomeSetup();
    }

    // Called Order 2
    void DoSomeSetup()
    {
        Debug.Log("Called InventoryMenu - DoSomeSetup");
        //
        menuText.text = menuName;
    }

    public void ClosePanel()
    {
        // Hide this game object
        gameObject.SetActive(false);
    }

    public void DeleteAll()
    {
        //
        SQLiteManager.Instance.ClearAllData();
        //
        PlayerPrefs.DeleteAll();
        //
        GameManager.Instance.Quit();
    }

    public void NewDay()
    {
        //
        GameManager.Instance.IsCustomisedNewDayLoad = true;
        //
        LoadingScene.LoadScene("OpenScene");
    }

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        Debug.Log("Called InventoryMenu - Start");

    }

    // Called when the game is terminated. Called Order Last
    void OnDisable()
    {
        Debug.Log("Called when the game is terminated - OnDisable");
    }
}
