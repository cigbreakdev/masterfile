﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TriggerFadeToSnapshot : MonoBehaviour
{
    public AudioMixerSnapshot active;
    public AudioMixerSnapshot inactive;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            active.TransitionTo(1f);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            inactive.TransitionTo(1f);
        }
    }

}
