﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public bool isLevel5Cleared;
    public bool isLevel27Cleared;

    public GameObject[] holderAt;

    public LevelOnMap[] levels;
    private int NumberOfLevels;

    public GameObject fruitMarket;

    void Awake()
    {
        NumberOfLevels = levels.Length;
    }

    public void SetupAllLevelsOnMap()
    {
        //
        fruitMarket.SetActive(false);
        //
        SetupFirstFive();
    }

    void SetupFirstFive()
    {
        //
        holderAt[0].SetActive(true);
        //
        for (int i = 0; i < 5; i++)
        {
            levels[i].Setup();
        }

        if (levels[4].isLevelCleared)
        {
            isLevel5Cleared = true;
            //
            fruitMarket.SetActive(true);
            //
            holderAt[0].SetActive(false);
            //
            OpenLevelsToNextHolder();
        }
    }

    void OpenLevelsToNextHolder()
    {
        //
        holderAt[1].SetActive(true);
        //
        for (int i = 5; i < NumberOfLevels; i++)
        {
            if (levels[i-1].isLevelCleared) { 
                levels[i].Setup();
            }
        }

        if (levels[26].isLevelCleared)
        {
            // Legacy "levels stop here" code
            //isLevel27Cleared = true;
            //holderAt[1].SetActive(false);
        }
    }

    public void SetupLevelAt(int levelID)
    {
        levels[levelID - 1].Setup();
    }
}
