﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial2 : MonoBehaviour
{
    public Transform startPoint;
    public Transform endPoint;
    public float speed = 1.0F;
    private float startTime;
    private float journeyLength;

    public GameObject carrot;
    public Sprite complete;

    // Use this for initialization
    void OnEnable()
    {
        gameObject.transform.position = startPoint.position;
        gameObject.GetComponent<Mask>().showMaskGraphic = false;
        startTime = Time.time;
        journeyLength = Vector3.Distance(startPoint.position, endPoint.position);
        carrot.GetComponent<Image>().sprite = complete;
        // Calculate speed
        speed = journeyLength / 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(startPoint.position, endPoint.position, fracJourney);
        yield return null;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "VisibleArea")
        {
            Debug.Log("Visible Enter");
            gameObject.GetComponent<Mask>().showMaskGraphic = true;
        }

        if (collision.gameObject.name == "Carrot")
        {
            Debug.Log("Break");
            gameObject.transform.position = startPoint.position;
            gameObject.GetComponent<Mask>().showMaskGraphic = false;
            startTime = Time.time;
            journeyLength = Vector3.Distance(startPoint.position, endPoint.position);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name == "VisibleArea")
        {
            Debug.Log("Visible Exit");
            gameObject.transform.position = startPoint.position;
            gameObject.GetComponent<Mask>().showMaskGraphic = false;
            startTime = Time.time;
            journeyLength = Vector3.Distance(startPoint.position, endPoint.position);
        }
    }
}
