﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelapseChoiceDetail : MonoBehaviour
{
    public GameObject relapseChoiceItemPrefab;
    public GameObject relapseOtherChoiceItemPrefab;

    private int m_relapseChoiceItemCount = 0;

    public void SetupRelapseChoiceDetail()
    {
        int newHeight = 0;

        m_relapseChoiceItemCount = SQLiteManager.Instance.RowsCount("RelapseChoice");
        Debug.Log("m_relapseChoiceItemCount:" + m_relapseChoiceItemCount);

        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
        Debug.Log("containerRectTransform's height:" + containerRectTransform.rect.height);

        RelapseChoiceItem current_item;
        for (int i = 1; i <= m_relapseChoiceItemCount; i++)
        {
            current_item = GetRelapseChoiceItem(i);
            if (current_item == null)
            {
                //create a new item, name it, and set the parent
                GameObject newItem = Instantiate(relapseChoiceItemPrefab) as GameObject;
                newItem.name = "RelapseChoiceItem_" + i;
                RelapseChoiceItem newRelapseChoiceItem = newItem.GetComponent<RelapseChoiceItem>();
                newRelapseChoiceItem.Setup(i);
                newItem.transform.SetParent(gameObject.transform, false);
                // Grid layout: height = 80
                newHeight += 80;
            }
            else
            {
                current_item.Setup(i);
            }
        }

        if (!IsRelapseOtherChoiceExist())
        {
            Debug.Log("Add the other choice.");
            // Add the other choice.
            //create a new item, name it, and set the parent
            GameObject otherChoiceItem = Instantiate(relapseOtherChoiceItemPrefab) as GameObject;
            otherChoiceItem.name = "RelapseOtherChoiceItem";
            RelapseOtherChoice newRelapseOtherChoiceItem = otherChoiceItem.GetComponent<RelapseOtherChoice>();
            newRelapseOtherChoiceItem.Setup();
            otherChoiceItem.transform.SetParent(gameObject.transform, false);
            // Grid layout: height = 80
            newHeight += 80;
        }

        /*
        if(newHeight > m_holderHieght)
        {
            containerRectTransform.sizeDelta.Set(containerRectTransform.rect.width, newHeight);
        }
        */
    }

    RelapseChoiceItem GetRelapseChoiceItem(int ID)
    {
        Component[] relapseChoiceItems;
        relapseChoiceItems = GetComponentsInChildren(typeof(RelapseChoiceItem));

        if (relapseChoiceItems == null)
        {
            return null;
        }
        else
        {
            foreach (RelapseChoiceItem item in relapseChoiceItems)
            {
                if (item.itemID == ID)
                {
                    return item;
                }
            }
            return null;
        }
    }

    bool IsRelapseOtherChoiceExist()
    {
        Component[] relapseOtherChoiceItems;
        relapseOtherChoiceItems = GetComponentsInChildren(typeof(RelapseOtherChoice));

        if (relapseOtherChoiceItems == null)
        {
            Debug.Log("RelapseOtherChoice does not found.");
            return false;
        }
        else
        {
            if (relapseOtherChoiceItems.Length > 0)
            {
                Debug.Log("RelapseOtherChoice found.");
                return true;
            }
            return false;
        }
    }

    public string GetRelapseAnswer()
    {
        string m_relapseAnswer = string.Empty;

        Component[] relapseChoiceItems;
        relapseChoiceItems = GetComponentsInChildren(typeof(RelapseChoiceItem));

        return m_relapseAnswer;
    }
}
