﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyHealthItem : MonoBehaviour {
    //
    public int height = 200;

    public int itemID;

    public Text titleText;
    public Text descriptionText;
    public Slider progressSlider;
    public Text percentText;

    public void Setup(int ID)
    {
        MyHealth record = SQLiteManager.Instance.GetMyHealthItem(ID);

        if(record.ID > 0)
        {
            itemID = record.ID;
            titleText.text = record.Title;
            descriptionText.text = record.Description;
            progressSlider.value = 0.0f;
            percentText.text = "0.0%";
        }
    }
}
