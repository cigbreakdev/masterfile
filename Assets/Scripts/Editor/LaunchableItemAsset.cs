﻿using UnityEngine;
using UnityEditor;

public class LaunchableItemAsset
{
    [MenuItem("Assets/Create/Launchable Item")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<LaunchableItem>();
    }
}