﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScene : MonoBehaviour
{

    private void Start()
    {
        SoundManager.Instance.StartCoroutine("StopMainMusic");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            LoadingScene.LoadScene("MapScene");
        }
    }
}
