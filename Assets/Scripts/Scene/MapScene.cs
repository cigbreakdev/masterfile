﻿using UnityEngine;
using SQLite4Unity3d;
using UnityEngine.UI;

public class MapScene : MonoBehaviour
{
    private GameObject m_gameManager;

    public Map map;
    public Text starAmount;
    public Text coinAmount;

    // Called Order 0
    void Awake()
    {
        Debug.Log("MapScene - Awake");

        m_gameManager = GameObject.Find("GameManager");

        if (m_gameManager == null)
        {
            GameManager.Instance.Init();
        }
    }

    // Use this for initialization
    void Start ()
	{
        starAmount.text = GameManager.Instance.CurrentProfile.TotalStar.ToString();
        coinAmount.text = GameManager.Instance.CurrentProfile.CurrentCoin.ToString();

        map.SetupAllLevelsOnMap();

        if(!SoundManager.Instance.mainMusicAudioSource.isPlaying)
        {
            SoundManager.Instance.StartCoroutine("RestartMainMusic");
        }
	}

    public void UpdateScorePanel()
    {
        starAmount.text = GameManager.Instance.CurrentProfile.TotalStar.ToString();
        coinAmount.text = GameManager.Instance.CurrentProfile.CurrentCoin.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.Instance.Quit();
        }
    }
}
