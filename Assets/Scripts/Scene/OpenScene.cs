﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class OpenScene : MonoBehaviour
{
    // Panel shows logo video.
    public GameObject logoVideo;
    // Raw Image to Show Video Images [Assign from the Editor].
    public RawImage image;
    // Video To Play [Assign from the Editor].
    public VideoClip videoToPlay;
    // Panel shows logo image.
    public GameObject logoImage;
    // Panel shows splash image [person smoking].
    public GameObject splashImage;

    private AudioSource m_SoundEffectSource;
    private VideoPlayer videoPlayer;
    private VideoSource videoSource;

    private string m_playerPrefCurrentDate;
    private bool isLogoSplashVideoIsPlayed;
    private bool isLogoSplashImageIsPlayed;
    private bool isAllFinished;

    // Called Order 0
    private GameObject m_gameManager;

    private void Awake()
    {
        Debug.Log("Scene1 - Awake() called.");
        Debug.Log("Time::" + System.DateTime.Now.ToString());
        m_gameManager = GameObject.Find("GameManager");
        if (m_gameManager == null)
        {
            GameManager.Instance.Init();
        }
    }

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        // Disable all panels.
        logoVideo.SetActive(false);
        logoImage.SetActive(false);
        splashImage.SetActive(false);
        //
        m_SoundEffectSource = null;
        isLogoSplashVideoIsPlayed = false;
        isLogoSplashImageIsPlayed = false;
        isAllFinished = false;

        Debug.Log("Called Order 3 - Start");
        //
        UpdateProfileStats();
        //
        LogoSelector();
    }

    private void UpdateProfileStats()
    {
        Debug.Log("Update profile stats on game start.");
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        int result = -1;

        AddLogin(mySQLTime);

        if (GameManager.Instance.CurrentProfile.TotalLoginCounts == 0)
        {
            // First Time.
            Debug.Log("First time to open game");
            GameManager.Instance.CurrentProfile.LastLoginDate = mySQLTime;
            GameManager.Instance.CurrentProfile.FirstTimeOfDay = 1;
            GameManager.Instance.CurrentProfile.TotalLoginCounts++;
            GameManager.Instance.CurrentProfile.TotalLoginDays++;
            GameManager.Instance.CurrentProfile.ConsecutiveLogin++;
            //
            PlayerPrefs.SetString("CurrentD", mySQLTime);
        }
        else
        {
            result = CalculateDateDiff(mySQLTime, GameManager.Instance.CurrentProfile.LastLoginDate);
            // Customised Load
            if(GameManager.Instance.IsCustomisedNewDayLoad)
            {
                GameManager.Instance.IsCustomisedNewDayLoad = false;
                // Fetch the FirstTimeOpened from the PlayerPrefs (set these Playerprefs in another script). 
                // If no Int of this name exists, the default is 0.
                m_playerPrefCurrentDate = PlayerPrefs.GetString("CurrentD", "");
                result = CalculateDateDiffCustom(m_playerPrefCurrentDate);
            }
            //
            switch (result)
            {
                case 0:
                    // Same Day.
                    Debug.Log("OpenScene - Same Day.");
                    GameManager.Instance.CurrentProfile.LastLoginDate = mySQLTime;
                    GameManager.Instance.CurrentProfile.FirstTimeOfDay = 0;
                    GameManager.Instance.CurrentProfile.TotalLoginCounts++;
                    break;
                case 1:
                    // Next Day.
                    Debug.Log("OpenScene - Next Day.");
                    GameManager.Instance.CurrentProfile.LastLoginDate = mySQLTime;
                    GameManager.Instance.CurrentProfile.FirstTimeOfDay = 1;
                    GameManager.Instance.CurrentProfile.TotalLoginCounts++;
                    GameManager.Instance.CurrentProfile.TotalLoginDays++;
                    GameManager.Instance.CurrentProfile.ConsecutiveLogin++;
                    break;
                case 2:
                    // More than 1 day.
                    Debug.Log("OpenScene - More than 1 day.");
                    GameManager.Instance.CurrentProfile.LastLoginDate = mySQLTime;
                    GameManager.Instance.CurrentProfile.FirstTimeOfDay = 1;
                    GameManager.Instance.CurrentProfile.TotalLoginCounts++;
                    GameManager.Instance.CurrentProfile.TotalLoginDays++;
                    GameManager.Instance.CurrentProfile.ConsecutiveLogin = 1;
                    break;
                case -1:
                    GameManager.Instance.CurrentProfile.LastLoginDate = mySQLTime;
                    GameManager.Instance.CurrentProfile.FirstTimeOfDay = 1;
                    GameManager.Instance.CurrentProfile.ConsecutiveLogin = 1;
                    break;
                default:
                    break;
            }
        }

        //
        GameManager.Instance.CurrentProfile.UpdateDate = mySQLTime;
        SQLiteManager.Instance.Profile_Update(GameManager.Instance.CurrentProfile);
        //
        Debug.Log(GameManager.Instance.CurrentProfile.ToString());
    }

    private int CalculateDateDiff(string currentLoginDate, string lastLoginDate)
    {
        DateTime currentLogin;
        DateTime lastLogin;

        bool failedToConvertDate = false;

        if (DateTime.TryParse(currentLoginDate, out currentLogin))
        {
            Debug.Log("Success convert string to DateTime");
        }
        else
        {
            Debug.Log("Fail convert string to DateTime");
            failedToConvertDate = true;
        }

        if (DateTime.TryParse(lastLoginDate, out lastLogin))
        {
            Debug.Log("Success convert string to DateTime");
        }
        else
        {
            Debug.Log("Fail convert string to DateTime");
            failedToConvertDate = true;
        }

        if(failedToConvertDate)
        {
            return -1;
        }

        // Same Day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays == 0)
        {
            //
            Debug.Log("Same Day.");
            return 0;
        }

        // Next Day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays == 1)
        {
            //
            Debug.Log("Next Day.");
            return 1;
        }

        // More than 1 day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays > 1)
        {
            //
            Debug.Log("More than 1 day.");
            return 2;
        }

        //
        Debug.Log("Error.");
        return -1;
    }

    private int CalculateDateDiffCustom(string playerPrefCurrentDateTime)
    {
        DateTime currentLogin;
        DateTime lastLogin;

        bool failedToConvertDate = false;

        if (DateTime.TryParse(playerPrefCurrentDateTime, out lastLogin))
        {
            Debug.Log("Success convert string to DateTime");
        }
        else
        {
            Debug.Log("Fail convert string to DateTime");
            failedToConvertDate = true;
        }

        if (failedToConvertDate)
        {
            return -1;
        }

        currentLogin = lastLogin.AddDays(1.0f);
        //
        string mySQLTime = currentLogin.Year + "-" + currentLogin.Month + "-" + currentLogin.Day + " " + currentLogin.Hour + ":" + currentLogin.Minute + ":" + currentLogin.Second;
        PlayerPrefs.SetString("CurrentD", mySQLTime);

        // Same Day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays == 0)
        {
            //
            Debug.Log("Same Day.");
            return 0;
        }

        // Next Day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays == 1)
        {
            //
            Debug.Log("Next Day.");
            return 1;
        }

        // More than 1 day.
        if ((currentLogin.Date - lastLogin.Date).TotalDays > 1)
        {
            //
            Debug.Log("More than 1 day.");
            return 2;
        }

        //
        Debug.Log("Error.");
        return -1;
    }

    private void AddLogin(string currentLoginDate)
    {

    }

    private void LogoSelector()
    {
        if (!PlayerPrefs.HasKey("FirstTimeOpened"))
        {
            Debug.Log("PlayerPrefs don't have FirstTimeOpened.");
            // Set the PlayerPrefs "FirstTimeOpened" to current date.
            string curDate = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            //
            Debug.Log("curDate = " + curDate);
            PlayerPrefs.SetString("FirstTimeOpened", curDate);
        }
        else
        {
            Debug.Log("PlayerPrefs have FirstTimeOpened.");
            // Fetch the FirstTimeOpened from the PlayerPrefs (set these Playerprefs in another script). 
            // If no Int of this name exists, the default is 0.
            Debug.Log(PlayerPrefs.GetString("FirstTimeOpened", ""));
        }

        // LogoSelector depend on the value of FirstTimeOfDay.
        if (GameManager.Instance.CurrentProfile.FirstTimeOfDay == 1)
        {
            // First time on the day, play animate logo.
            StartCoroutine(PlayLogoSplashVideo());
        }
        else
        {
            // Else, play logo image.
            StartCoroutine(PlayLogoSplashImage());
        }
        //
        StartCoroutine(PlaySplashImage());
        //
        StartCoroutine(LoadNextScene());
    }

    IEnumerator PlayLogoSplashVideo()
    {
        logoVideo.SetActive(true);

        // Add VideoPlayer to the GameObject
        videoPlayer = gameObject.AddComponent<VideoPlayer>();

        // Add AudioSource
        m_SoundEffectSource = gameObject.AddComponent<AudioSource>();

        // Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        m_SoundEffectSource.playOnAwake = false;

        // We want to play from url
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";

        // We want to play from video clip not from url
        videoPlayer.source = VideoSource.VideoClip;

        // Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        // Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, m_SoundEffectSource);

        // Set video To Play then prepare Audio to prevent Buffering
        videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        // Wait until video is prepared
        WaitForSeconds waitTime = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            //Debug.Log ("Preparing Video");
            // Prepare/Wait for 1 sceonds only
            yield return waitTime;
            // Break out of the while loop after 1 seconds wait
            break;
        }

        Debug.Log("Done Preparing Video");

        // Assign the Texture from Video to RawImage to be displayed
        image.texture = videoPlayer.texture;

        // Play Video
        videoPlayer.Play();

        // Play Sound
        m_SoundEffectSource.Play();

        //Debug.Log ("Playing Video");
        while (videoPlayer.isPlaying && videoPlayer.time < 8)
        {
            //Debug.LogWarning ("Video Time: " + Mathf.FloorToInt ((float)videoPlayer.time));
            yield return null;
        }

        //Debug.Log ("Done Playing Video");

        isLogoSplashVideoIsPlayed = true;
        logoVideo.SetActive(false);
    }

    IEnumerator PlayLogoSplashImage()
    {
        logoImage.SetActive(true);
        //
        Image imgObject = logoImage.GetComponent<Image>();
        Debug.Log("Begin fade-in");

        imgObject.CrossFadeAlpha(255.0f, 0.5f, false);
        yield return new WaitForSeconds(1.0f);

        Debug.Log("Begin fade-out");

        imgObject.CrossFadeAlpha(1.0f, 0.5f, false);
        yield return new WaitForSeconds(1.0f);

        Debug.Log("End splash image");

        isLogoSplashImageIsPlayed = true;
        logoImage.SetActive(false);
    }

    IEnumerator PlaySplashImage()
    {
        while (!isLogoSplashVideoIsPlayed && !isLogoSplashImageIsPlayed)
            yield return new WaitForSeconds(0.1f);

        if (isLogoSplashVideoIsPlayed || isLogoSplashImageIsPlayed)
        {
            splashImage.SetActive(true);
            //
            Image imgObject = splashImage.GetComponent<Image>();
            Debug.Log("Begin fade-in");
            imgObject.CrossFadeAlpha(255.0f, 0.5f, false);

            yield return new WaitForSeconds(1.0f);

            Debug.Log("Begin fade-out");
            imgObject.CrossFadeAlpha(1.0f, 0.5f, false);

            yield return new WaitForSeconds(1.0f);
            Debug.Log("End splash image");

            splashImage.SetActive(false);
            isAllFinished = true;
        }
    }

    IEnumerator LoadNextScene()
    {
        while (!isAllFinished)
            yield return new WaitForSeconds(0.1f);

        if (isAllFinished)
        {
            Debug.Log("Loading next scene from OpenScene.");
            //
            LoadingScene.LoadFromOpenScene();
        }
    }
}
