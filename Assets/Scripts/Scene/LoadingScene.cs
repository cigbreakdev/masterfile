﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LoadingScene : MonoBehaviour
{
    //
    private static string LevelToLoad { get; set; }
    //
    public Text loadingText = null;
    //
    private bool isSceneLoad = false;

    private string initialText = string.Empty;

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        Debug.Log("Called Order 3 - Start");
        //
        initialText = "Loading";
        isSceneLoad = false;
        //
        StartCoroutine(UpdateLoadingText(initialText));
        //
        SceneManager.LoadSceneAsync(LevelToLoad);
    }

    IEnumerator UpdateLoadingText(string initText)
    {
        yield return new WaitForSeconds(0.1f);
        loadingText.text = initText + ".";
        yield return new WaitForSeconds(0.1f);
        loadingText.text = initText + "..";
        yield return new WaitForSeconds(0.1f);
        loadingText.text = initText + "...";
        yield return new WaitForSeconds(0.1f);
        loadingText.text = initText;
        StartCoroutine(UpdateLoadingText(initText));
    }

    public static void LoadScene(string levelName)
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
        Debug.Log("Gameplay start::" + mySQLTime);
        LevelToLoad = levelName;
        SceneManager.LoadSceneAsync("LoadingScene");
    }

    public static void LoadFromOpenScene()
    {
        Debug.Log("LoadFromOpenScene");
        Debug.Log(GameManager.Instance.CurrentProfile.ToString());
        // Don't ask question with non-smoker player.
        if (GameManager.Instance.CurrentProfile.IsNonSmoker == 1)
        {
            // Skip loading dairy scene and go to map scene.
            LevelToLoad = "MapScene";
            SceneManager.LoadSceneAsync("LoadingScene");
        }
        else
        {
            if (GameManager.Instance.CurrentProfile.FirstTimeOfDay == 1)
            {
                Debug.Log("Load the daily scene");
                //

                // Only first try on the day.
                LevelToLoad = "DailyScene";
                SceneManager.LoadSceneAsync("LoadingScene");
            }
            else
            {
                Debug.Log("Load the map scene");
                // Skip loading daily scene and go to map scene.
                LevelToLoad = "MapScene";
                SceneManager.LoadSceneAsync("LoadingScene");
            }
        }
    }
}
