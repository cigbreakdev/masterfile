﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LevelOnMap : MonoBehaviour
{
    public GameObject levelInfoPanel = null;

    [Header("Properties")]
    public int MapID = 1;
    public int LevelID = 1;
    public bool isLevelCleared = false;
    public LevelOnMap previousLevel = null;

    [Header("Graphics")]
    [SerializeField]
    private Text LevelText = null;
    [SerializeField]
    private Image LungImage = null;
    [SerializeField]
    private Image Star1Image = null;
    [SerializeField]
    private Image Star2Image = null;
    [SerializeField]
    private Image Star3Image = null;
    [SerializeField]
    private Sprite[] LungSprite;
    [SerializeField]
    private Sprite[] StarSprite;

    public void Setup()
    {
        // By default, display level number and hide the lung and the stars.
        gameObject.SetActive(true);

        LevelText.text = LevelID.ToString();
        LungImage.enabled = false;
        Star1Image.enabled = false;
        Star2Image.enabled = false;
        Star3Image.enabled = false;

        // Load result from database
        int result = SQLiteManager.Instance.GetPastResult(GameManager.Instance.CurrentProfile.ProfileID, LevelID);
        Debug.Log("Level " + LevelID + " - debug mode: " + GameManager.Instance.debugMode + "; result =" + result);
        DisplayResult(result);
    }

    void DisplayResult(int result)
    {
        switch (result)
        {
            case 3:
                Star1Image.sprite = StarSprite[result];
                Star2Image.sprite = StarSprite[result];
                Star3Image.sprite = StarSprite[result];
                break;
            case 2:
                Star1Image.sprite = StarSprite[result];
                Star2Image.sprite = StarSprite[result];
                Star3Image.sprite = StarSprite[0]; // empty star.
                break;
            case 1:
                Star1Image.sprite = StarSprite[result];
                Star2Image.sprite = StarSprite[0]; // empty star.
                Star3Image.sprite = StarSprite[0]; // empty star.
                break;
            case 0:
                Star1Image.sprite = StarSprite[result];
                Star2Image.sprite = StarSprite[result];
                Star3Image.sprite = StarSprite[result];
                break;
            default:
                Star1Image.sprite = StarSprite[0];
                Star2Image.sprite = StarSprite[0];
                Star3Image.sprite = StarSprite[0];
                break;
        }

        if (result > -1)
        {
            // Level is cleared (return 0-3). Hide level text and Show lung image.
            LungImage.sprite = LungSprite[result];
            LungImage.enabled = true;
            Star1Image.enabled = true;
            Star2Image.enabled = true;
            Star3Image.enabled = true;
            LevelText.enabled = false;
            isLevelCleared = true;
        }
        else
        {
            // Level is not cleared (return -1). Show level text and Hide lung image.
            LungImage.enabled = false;
            Star1Image.enabled = false;
            Star2Image.enabled = false;
            Star3Image.enabled = false;
            LevelText.enabled = true;
            isLevelCleared = false;
        }
    }

    public void DisplayLevelStart(Text levelText)
    {
        if (GameManager.Instance.debugMode)
        {
            Debug.Log("Debug mode is ON");
            GameManager.Instance.CurrentLevel = LevelID;
            levelInfoPanel.SetActive(true);
        }
        else
        {
            Debug.Log("Debug mode is OFF");
            Debug.Log("Level " + LevelID + " was clicked");
            GameManager.Instance.CurrentLevel = LevelID;
            if (LevelID == 1)
            {
                Debug.Log("Level " + LevelID + " displayed");
                levelInfoPanel.SetActive(true);
            }
            else
            {
                Debug.Log("previousLevel.isLevelCleared:" + previousLevel.isLevelCleared);
                if (LevelID > 1 && previousLevel.isLevelCleared)
                {
                    Debug.Log("Level " + LevelID + " displayed");
                    levelInfoPanel.SetActive(true);
                }
            }
        }
    }
}
