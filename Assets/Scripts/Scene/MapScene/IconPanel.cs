﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconPanel : MonoBehaviour
{
    public GameObject relatedMenuUI;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CallMenu()
    {
        relatedMenuUI.SetActive(true);
    }
}
