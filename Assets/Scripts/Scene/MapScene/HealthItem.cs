﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthItem : MonoBehaviour {

	public Text titleText = null;
	public Text descriptionText = null;
	public Slider barSlider = null;
	public Text progressText = null;
	public string titleMessage = string.Empty;
	public string statementMessage = string.Empty;
	public float dayToAchieve = 0.0f;

	// Use this for initialization
	void Start () {
		if (titleMessage != string.Empty) {
			titleText.text = titleMessage;
		}

		if (statementMessage != string.Empty) {
			descriptionText.text = statementMessage;
		}
		// Slider min value = 0, max value = 1
		barSlider.value = CigbreakManager.instance.TotalPlayedTimeOnDevice / dayToAchieve;
		progressText.text = (barSlider.value * 100.0f).ToString ("F2") + "%";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
