﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyMissionItem : MonoBehaviour
{
    public Image missionImage;
    public Text missionDay;
    public Text missionDetail;
    public int itemID;
    //
    public Sprite[] MissionIcon;
    public int height = 100;
    //
    public GameObject missionPopup;

    public void Setup(int ID, GameObject Popup)
    {
        QuitMission record = SQLiteManager.Instance.GetMyMissionItem(ID);

        if (record.ID > 0)
        {
            itemID = record.ID;
            // Need to fix when the player did not do the mission.
            if (GameManager.Instance.CurrentProfile.TotalLoginDays == record.GameDay)
            {
                missionDay.text = "Today";
            }
            else
            {
                missionDay.text = "Day " + record.GameDay.ToString();
            }

            if (MissionIcon[itemID] != null)
            {
                missionImage.sprite = MissionIcon[itemID];
            }
            else
            {
                missionImage.sprite = MissionIcon[0];
            }

            missionDetail.text = record.MissionDetail;
            missionPopup = Popup;
        }
    }

    public void Popup()
    {
        if(missionDay.text == "Today")
        {
            PlayerPrefs.SetInt("IsTodayMission", 1);
        }
        else
        {
            PlayerPrefs.SetInt("IsTodayMission", 0);
        }

        PlayerPrefs.SetInt("MissionID", itemID);
        
        missionPopup.SetActive(true);
    }
}
