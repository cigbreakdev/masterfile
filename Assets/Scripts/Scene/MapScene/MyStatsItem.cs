﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyStatsItem : MonoBehaviour {

	public Text statsDescriptions;
	public Text statsValue;
	public string statsName;
	public int itemID;

    public int height = 100;

    public void Setup(int ID)
    {
        StatsData record = SQLiteManager.Instance.GetMyStatsItem(ID);

        if (record.ID > 0)
        {
            itemID = record.ID;
            statsDescriptions.text = record.StatsDesc;
            statsValue.text = "0";
        }
    }


    // Use this for initialization
    void Start () {
        /*
		statsIndex = transform.GetSiblingIndex () + 1;

		DatabaseService db = new DatabaseService ();
		StatsData S = db.StatsData_At (statsIndex);

		statsDescriptions.text = S.StatsDesc;
		statsValue.text = CigbreakManager.instance.currentProfile.getStatsValue(S.StatsParameter).ToString();
        */
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
