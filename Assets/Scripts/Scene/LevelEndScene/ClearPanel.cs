﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearPanel : MonoBehaviour
{
	//
	public Text levelText = null;
	//
	[Header ("Star panel items:")]
	public Image copperImageFill = null;
	public Image silverImageFill = null;
	public Image goldImageFill = null;
    public Image lungImageFill = null;
	//
	[Header ("Break panel items:")]
	public GameObject breakPanel = null;
	public GameObject cigaretteItem = null;
	public Text cigaretteText = null;
	public GameObject rollupItem = null;
	public Text rollupText = null;
	public GameObject cigarItem = null;
	public Text cigarText = null;
	public GameObject pipeItem = null;
	public Text pipeText = null;
	//
	[Header ("Collect panel items:")]
	public GameObject collectPanel = null;
	public GameObject carrotItem = null;
	public Text carrotText = null;
	public GameObject celeryItem = null;
	public Text celeryText = null;
	public GameObject springOnionItem = null;
	public Text springOnionText = null;
	public GameObject chiliItem = null;
	public Text chiliText = null;
	public GameObject cornItem = null;
	public Text cornText = null;
	public GameObject bananaItem = null;
	public Text bananaText = null;
	[Space (10)]
	//
	public Button nextButton = null;
    //
    public GameObject nextLevelInfo;
	//
	private bool isCopperFill = false;
	private bool isSilverFill = false;
	private bool isGoldFill = false;
    private bool isLungFill = false;
	//
	private bool updateProfileComplete = false;

	//
	private LevelPlayed record;

	void Start ()
	{
		Debug.Log ("LevelEnd - WinPanel - ClearPanel: Start");
		//
        /*
		BreakItemsSetup ();
		CollectionItemsSetup ();
        */
		//
		record = GameManager.Instance.lastLevelPlayed;
		Debug.Log (record.ToString ());

        // Update player profile
        GameManager.Instance.CurrentProfile.updateProfile(record);
        SQLiteManager.Instance.Profile_Update(GameManager.Instance.CurrentProfile);
		Debug.Log ("1:" + GameManager.Instance.CurrentProfile.ToString ());
		//
		levelText.text = "LEVEL " + record.Level.ToString ();
        /*
		nextButton.enabled = false;
		nextButton.gameObject.SetActive (false);
        */
		//
		copperImageFill.fillAmount = 0f;
		silverImageFill.fillAmount = 0f;
		goldImageFill.fillAmount = 0f;

		//
		StartCoroutine (StarFillAnimation ());
		//
        /*
		BreakItemsDisplay ();
		CollectionItemsDisplay ();
        */
        //
        updateProfileComplete = true;
    }

	IEnumerator StarFillAnimation ()
	{
		Debug.Log ("LevelEnd - WinPanel - ClearPanel: StarFillAnimation");
		// When WinLevelPanel is active. The result should be 1, 2, or 3.
		if (record.Result == 1) {
			StartCoroutine (CopperFill ());
			isSilverFill = true;
			isGoldFill = true;
		} else if (record.Result == 2) {
			StartCoroutine (CopperFill ());
			StartCoroutine (SivlerFill ());
			isGoldFill = true;
		} else if (record.Result == 3) {
			StartCoroutine (CopperFill ());
			StartCoroutine (SivlerFill ());
			StartCoroutine (GoldFill ());
		}
		yield return null;
	}

	IEnumerator CopperFill ()
	{
		while (copperImageFill.fillAmount < 1) {
			copperImageFill.fillAmount += 0.05f;
			yield return new WaitForSeconds (0.1f);
		}
		isCopperFill = true;
		Debug.Log ("CopperFill End");
	}

	IEnumerator SivlerFill ()
	{
		while (!isCopperFill) {
			yield return new WaitForSeconds (0.1f);
		}

		while (silverImageFill.fillAmount < 1) {
			silverImageFill.fillAmount += 0.05f;
			yield return new WaitForSeconds (0.1f);
		}
		isSilverFill = true;
		Debug.Log ("SivlerFill End");
	}

	IEnumerator GoldFill ()
	{
		while (!isSilverFill) {
			yield return new WaitForSeconds (0.1f);
		}

		while (goldImageFill.fillAmount < 1) {
			goldImageFill.fillAmount += 0.05f;
			yield return new WaitForSeconds (0.1f);
		}
		isGoldFill = true;
		Debug.Log ("GoldFill End");
	}

	void BreakItemsSetup ()
	{
		breakPanel.SetActive (true);

		cigaretteItem.SetActive (true);
		cigaretteText.text = "Cigarette X ";
		rollupItem.SetActive (true);
		rollupText.text = "Rollup X ";
		cigarItem.SetActive (true);
		cigarText.text = "Cigar X ";
		pipeItem.SetActive (true);
		pipeText.text = "Pipe X ";
	}

	void BreakItemsDisplay ()
	{
		if (record.CigaretteBreak > 0) {
			cigaretteText.text += record.CigaretteBreak.ToString ();
		} else {
			cigaretteItem.SetActive (false);
		}

		if (record.RollupBreak > 0) {
			rollupText.text += record.RollupBreak.ToString ();
		} else {
			rollupItem.SetActive (false);
		}

		if (record.CigarBreak > 0) {
			cigarText.text += record.CigarBreak.ToString ();
		} else {
			cigarItem.SetActive (false);
		}

		if (record.PipeBreak > 0) {
			pipeText.text += record.PipeBreak.ToString ();
		} else {
			pipeItem.SetActive (false);
		}
	}

	void CollectionItemsSetup ()
	{
		collectPanel.SetActive (true);

		carrotItem.SetActive (true);
		carrotText.text = "Carrot X ";
		celeryItem.SetActive (true);
		celeryText.text = "Celery X ";
		springOnionItem.SetActive (true);
		springOnionText.text = "Spring Onion X ";
		chiliItem.SetActive (true);
		chiliText.text = "Chili X ";
		cornItem.SetActive (true);
		cornText.text = "Corn X ";
		bananaItem.SetActive (true);
		bananaText.text = "Banana X ";
	}

	void CollectionItemsDisplay ()
	{
		int tapCount = 0;

		if (record.CarrotTap > 0) {
			carrotText.text += record.CarrotTap.ToString ();
			tapCount += record.CarrotTap;
		} else {
			carrotItem.SetActive (false);
		}

		if (record.CeleryTap > 0) {
			celeryText.text += record.CeleryTap.ToString ();
			tapCount += record.CeleryTap;
		} else {
			celeryItem.SetActive (false);
		}

		if (record.SpringOnionTap > 0) {
			springOnionText.text += record.SpringOnionTap.ToString ();
			tapCount += record.SpringOnionTap;
		} else {
			springOnionItem.SetActive (false);
		}

		if (record.ChiliTap > 0) {
			chiliText.text += record.ChiliTap.ToString ();
			tapCount += record.ChiliTap;
		} else {
			chiliItem.SetActive (false);
		}

		if (record.CornTap > 0) {
			cornText.text += record.CornTap.ToString ();
			tapCount += record.CornTap;
		} else {
			cornItem.SetActive (false);
		}

		if (record.BananaTap > 0) {
			bananaText.text += record.BananaTap.ToString ();
			tapCount += record.BananaTap;
		} else {
			bananaItem.SetActive (false);
		}

		if (tapCount == 0) {
			collectPanel.SetActive (false);
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if ((Input.touchCount == 1 || Input.GetMouseButtonDown (0)) && updateProfileComplete) {
			Debug.Log ("Touch Detected");
            StopAllCoroutines();
            // When WinLevelPanel is active. The result should be 1, 2, or 3.
            if (record.Result == 1) {
				copperImageFill.fillAmount = 1;
			} else if (record.Result == 2) {
				copperImageFill.fillAmount = 1;
				silverImageFill.fillAmount = 1;
			} else if (record.Result == 3) {
				copperImageFill.fillAmount = 1;
				silverImageFill.fillAmount = 1;
				goldImageFill.fillAmount = 1;
			}
            /*
			nextButton.gameObject.SetActive (true);
			nextButton.enabled = true;
            */
		}

		if (isCopperFill && isSilverFill && isGoldFill && updateProfileComplete) {
            StopAllCoroutines();
            /*
            nextButton.gameObject.SetActive (true);
			nextButton.enabled = true;
            */
		}
	}

    public void LoadMapScene()
    {
        LoadingScene.LoadScene("MapScene");
    }

    public void LoadNextLevel()
    {
        if (GameManager.Instance.CurrentLevel > -1)
        {
            GameManager.Instance.CurrentLevel++;
            nextLevelInfo.SetActive(true);
        }
    }
}
