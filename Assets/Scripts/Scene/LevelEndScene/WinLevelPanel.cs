﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLevelPanel : MonoBehaviour
{
    //
    public GameObject clearPanel = null;
    public GameObject rewardPanel = null;
    public GameObject unlockPanel = null;

    public Text healthMessageText;
    public string[] healthMessage;


    public AudioClip winSound;

    AudioSource musicSource;

    // Use this for initialization
    void Start()
    {
        Debug.Log("LevelEnd - WinPanel: Start");
        clearPanel.SetActive(true);

        //
        string randomText = healthMessage[Random.Range(0, healthMessage.Length)];
        healthMessageText.text = randomText;

        rewardPanel.SetActive(false);
        unlockPanel.SetActive(false);

        musicSource = GetComponent<AudioSource>();
        // Set up music sound
        //musicSource.clip = Resources.Load ("Audio/Music/Win_Music_Mix", typeof(AudioClip)) as AudioClip;
        musicSource.clip = winSound;
        musicSource.loop = true;
        StartCoroutine(FadeInMusic(1f));

    }

    private IEnumerator FadeInMusic(float time)
    {
        musicSource.volume = 0;
        musicSource.Play();

        float speed = 1f / time;
        while (musicSource.volume < 0.5f)
        {
            musicSource.volume += speed * Time.deltaTime;
            yield return null;
        }
    }

    public void NextToRewardPanel()
    {
        clearPanel.SetActive(false);
        rewardPanel.SetActive(true);
    }
}
