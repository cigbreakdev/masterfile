﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseLevelPanel : MonoBehaviour {

	//
	public Text levelText = null;

    public AudioClip loseSound;

	AudioSource musicSource;

	//
	private LevelPlayed record;

	// Use this for initialization
	void Start () {
        //
        record = GameManager.Instance.lastLevelPlayed;
        Debug.Log(record.ToString());
        //
        levelText.text = "LEVEL " + record.Level.ToString();

		musicSource = GetComponent<AudioSource> ();
        // Set up music sound
        //musicSource.clip = Resources.Load ("Audio/Music/Lose_Music_Mix", typeof(AudioClip)) as AudioClip;
        musicSource.clip = loseSound;
		musicSource.loop = true;
		StartCoroutine (FadeInMusic (1f));
	}

	private IEnumerator FadeInMusic (float time)
	{
		musicSource.volume = 0;
		musicSource.Play ();

		float speed = 1f / time;
		while (musicSource.volume < 0.5f) {
			musicSource.volume += speed * Time.deltaTime;
			yield return null;
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
