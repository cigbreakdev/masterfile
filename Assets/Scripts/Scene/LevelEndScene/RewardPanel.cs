﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardPanel : MonoBehaviour
{
    //
    private LevelPlayed record;
    //
    public Text levelText = null;
	//
	[Header ("Star panel items:")]
	public Image copperImageFill = null;
	public Image silverImageFill = null;
	public Image goldImageFill = null;
	//
	[Header ("Reward panel items:")]
	public GameObject rewardPanel = null;
	public GameObject coinItem = null;
	public Text coinText = null;
	public GameObject carrotItem = null;
	public Text carrotText = null;
	public GameObject celeryItem = null;
	public Text celeryText = null;
	public GameObject springOnionItem = null;
	public Text springOnionText = null;
	public GameObject chiliItem = null;
	public Text chiliText = null;
	public GameObject cornItem = null;
	public Text cornText = null;
	public GameObject bananaItem = null;
	public Text bananaText = null;
	public GameObject sprayItem = null;
	public Text sprayText = null;
	public GameObject gumItem = null;
	public Text gumText = null;
	public GameObject patchItem = null;
	public Text patchText = null;
	[Space (10)]
	//
	private LevelReward reward;

	void Start ()
	{
		Debug.Log ("LevelEnd - WinPanel - RewardPanel: Start");
        //
        record = GameManager.Instance.lastLevelPlayed;
        levelText.text = "LEVEL " + record.Level.ToString();
        //
        RewardItemsSetup ();
		//
		RewardItemsDisplay (record.Level);
		//
		copperImageFill.fillAmount = 0f;
		silverImageFill.fillAmount = 0f;
		goldImageFill.fillAmount = 0f;

		// When WinLevelPanel is active. The result should be 1, 2, or 3.
		if (record.Result == 1) {
			copperImageFill.fillAmount = 1;
		} else if (record.Result == 2) {
			copperImageFill.fillAmount = 1;
			silverImageFill.fillAmount = 1;
		} else if (record.Result == 3) {
			copperImageFill.fillAmount = 1;
			silverImageFill.fillAmount = 1;
			goldImageFill.fillAmount = 1;
		}
	}

	void RewardItemsSetup()
	{
		rewardPanel.SetActive (true);

		coinItem.SetActive (true);
		coinText.text = "Coin X ";
		carrotItem.SetActive (true);
		carrotText.text = "Carrot X ";
		celeryItem.SetActive (true);
		celeryText.text = "Celery X ";
		springOnionItem.SetActive (true);
		springOnionText.text = "Spring Onion X ";
		chiliItem.SetActive (true);
		chiliText.text = "Chili X ";
		cornItem.SetActive (true);
		cornText.text = "Corn X ";
		bananaItem.SetActive (true);
		bananaText.text = "Banana X ";
		sprayItem.SetActive (true);
		sprayText.text = "Spray X ";
		gumItem.SetActive (true);
		gumText.text = "Gum X ";
		patchItem.SetActive (true);
		patchText.text = "Patch X ";
	}

	void RewardItemsDisplay(int levelIndex)
	{
        // Load level's reward from database.
        reward = SQLiteManager.Instance.LoadReward(levelIndex);
		Debug.Log (reward.ToString());

		//
		if (reward.Level_ID > 0) {
			//
			GameManager.Instance.CurrentProfile.updateProfile (reward);
            SQLiteManager.Instance.Profile_Update(GameManager.Instance.CurrentProfile);
            Debug.Log ("4:" + GameManager.Instance.CurrentProfile.ToString ());
			//
			if (reward.Coins > 0) {
				coinItem.SetActive (true);
				coinText.text += reward.Coins.ToString();
			} else {
				coinItem.SetActive (false);
			}

			//
			if (reward.Carrot > 0) {
				carrotItem.SetActive (true);
				carrotText.text += reward.Carrot.ToString();
			} else {
				carrotItem.SetActive (false);
			}

			//
			if (reward.Celery > 0) {
				celeryItem.SetActive (true);
				celeryText.text += reward.Celery.ToString();
			} else {
				celeryItem.SetActive (false);
			}

			//
			if (reward.Spring_Onion > 0) {
				springOnionItem.SetActive (true);
				springOnionText.text += reward.Spring_Onion.ToString();
			} else {
				springOnionItem.SetActive (false);
			}

			//
			if (reward.Chili > 0) {
				chiliItem.SetActive (true);
				chiliText.text += reward.Chili.ToString();
			} else {
				chiliItem.SetActive (false);
			}

			//
			if (reward.Corn > 0) {
				cornItem.SetActive (true);
				cornText.text += reward.Corn.ToString();
			} else {
				cornItem.SetActive (false);
			}

			//
			if (reward.Banana > 0) {
				bananaItem.SetActive (true);
				bananaText.text += reward.Banana.ToString();
			} else {
				bananaItem.SetActive (false);
			}

			//
			if (reward.Spray > 0) {
				sprayItem.SetActive (true);
				sprayText.text += reward.Spray.ToString();
			} else {
				sprayItem.SetActive (false);
			}

			//
			if (reward.Gum > 0) {
				gumItem.SetActive (true);
				gumText.text += reward.Gum.ToString();
			} else {
				gumItem.SetActive (false);
			}

			//
			if (reward.Patch > 0) {
				patchItem.SetActive (true);
				patchText.text += reward.Patch.ToString();
			} else {
				patchItem.SetActive (false);
			}
		} else {
			Debug.Log ("Can find reward data");
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.touchCount == 1 || Input.GetMouseButtonDown (0)) {
			Debug.Log ("Touch Detected");
			// When WinLevelPanel is active. The result should be 1, 2, or 3.
			if (record.Result == 1) {
				copperImageFill.fillAmount = 1;
			} else if (record.Result == 2) {
				copperImageFill.fillAmount = 1;
				silverImageFill.fillAmount = 1;
			} else if (record.Result == 3) {
				copperImageFill.fillAmount = 1;
				silverImageFill.fillAmount = 1;
				goldImageFill.fillAmount = 1;
			}
		} 
	}

    public void LoadMapScene()
    {
        LoadingScene.LoadScene("MapScene");
    }

    public void LevelStartPanel_LoadLevel()
    {
        if (GameManager.Instance.CurrentLevel > -1)
        {
            GameManager.Instance.CurrentLevel++;
            LoadingScene.LoadScene("MainScene");
        }
    }
}
