﻿using UnityEngine;

public class DailyScene : MonoBehaviour
{
    private GameObject m_gameManager;
    //
    public GameObject oneTimePanel = null;
    //
	public GameObject dailyPanel = null;

    // Called Order 0
    void Awake()
    {
        Debug.Log("DailyScene - Awake");

        m_gameManager = GameObject.Find("GameManager");

        if (m_gameManager == null)
        {
            LoadingScene.LoadScene("OpenScene");
        }
    }

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called Order 1 - OnEnable called");
        //
        oneTimePanel.SetActive (false);
        dailyPanel.SetActive (false);
    }

    // Called Order 3
	// Use this for initialization
	void Start ()
	{
		// Display the panel when open Cigbreak at first time of the day.
		if (GameManager.Instance.CurrentProfile.TotalLoginDays == 1) {
			// Day 1.
			oneTimePanel.SetActive (true);
            dailyPanel.SetActive (false);
		} else {
			// Day 2 or later.
			oneTimePanel.SetActive (false);
            dailyPanel.SetActive (true);
            //
            SecondDayPanel secondDayPanel = dailyPanel.GetComponent<SecondDayPanel>();
            secondDayPanel.triggerChoiceDetail.SetupTriggerChoiceDetail();
            secondDayPanel.relapseChoiceDetail.SetupRelapseChoiceDetail();
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
            GameManager.Instance.Quit();
		}
	}
}
