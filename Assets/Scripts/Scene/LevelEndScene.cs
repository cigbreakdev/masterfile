﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEndScene : MonoBehaviour
{
	public GameObject winLevelPanel = null;
	public GameObject loseLevelPanel = null;
	public GameObject nextLevelPanel = null;
	public ParticleSystem firework = null;

    private GameObject m_gameManager;

    // Called Order 0
    void Awake()
    {
        Debug.Log("LevelEndScene - Awake");

        m_gameManager = GameObject.Find("GameManager");

        if (m_gameManager == null)
        {
            LoadingScene.LoadScene("OpenScene");
        }
    }

    // Use this for initialization
    void Start ()
	{
		// Hide all panel
		winLevelPanel.SetActive (false);
		loseLevelPanel.SetActive (false);
		nextLevelPanel.SetActive (false);

		//cigbreakManager.TotalPlayedTimeOnDevice = dbService.LevelPlayed_TotalPlayedTime (CigbreakManager.instance.CurrentDevice.DeviceInfo);

		if (GameManager.Instance.lastLevelPlayed.RemainedLife > 0) {
			Debug.Log("LevelEnd: Win");
			winLevelPanel.SetActive (true);
			firework.Play ();
		} else {
			Debug.Log ("LevelEnd: Lose");
			loseLevelPanel.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{

    }

	public void Load_Next_Level ()
	{
		if (GameManager.Instance.lastLevelPlayed.Level > -1) {
            GameManager.Instance.lastLevelPlayed.Level++;

            if (firework.isPlaying) {
				firework.Stop ();
			}
			winLevelPanel.SetActive (false);
			nextLevelPanel.SetActive (true);
		}
	}

	public void LevelStartPanel_LoadLevel ()
	{
		if (GameManager.Instance.lastLevelPlayed.Level > -1) {
			LoadingScene.LoadScene ("MainScene");
		}
	}

	public void Replay_Level ()
	{
		if (GameManager.Instance.lastLevelPlayed.Level > -1) {
			LoadingScene.LoadScene ("MainScene");
		}
	}

	public void LoadMapScene ()
	{
		LoadingScene.LoadScene("MapScene");
	}
}
