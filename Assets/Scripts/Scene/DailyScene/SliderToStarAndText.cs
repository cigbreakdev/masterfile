﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToStarAndText : MonoBehaviour {

	public Image copperFillImage = null;
	public Image silverFillImage = null;
	public Image goldFillImage = null;

	public Slider readySlider = null;
	public Text readyText = null;

	// Use this for initialization
	void Start () {
		copperFillImage.fillAmount = 0;
		silverFillImage.fillAmount = 0;
		goldFillImage.fillAmount = 0;
		readyText.text = "0%";
	}
	
	public void ConvertSliderValueToStarAndText()
	{
		// cravingSlider.value = 3300 = 100%;
		int valSlider = Mathf.FloorToInt(readySlider.value);

		float valToFillStar = float.Parse (valSlider.ToString ()) / 1100.0f;
		float valToFillCopper = 1.0f - valToFillStar;
		float valToFillSilver;
		float valToFillGold;

		if (valToFillCopper >= 0f) {
			copperFillImage.fillAmount = valToFillStar;
			silverFillImage.fillAmount = 0;
			goldFillImage.fillAmount = 0;
		} else {
			valToFillSilver = 2.0f - valToFillStar;
			if (valToFillSilver >= 0f) {
				copperFillImage.fillAmount = 1;
				silverFillImage.fillAmount = valToFillStar - 1.0f;
				goldFillImage.fillAmount = 0;
			} else {
				valToFillGold = 3.0f - valToFillStar;
				if(valToFillGold >= 0f){
					copperFillImage.fillAmount = 1;
					silverFillImage.fillAmount = 1;
					goldFillImage.fillAmount = valToFillStar - 2.0f;
				}
			}
		}

		float valPercent = (float.Parse (valSlider.ToString ()) / 3300.0f) * 100.0f;
		readyText.text = valPercent.ToString("F2") + "%";
	}
}
