﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class SecondDayPanel : MonoBehaviour
{

    public GameObject afterWakeupPanel = null;
    public GameObject trackProgressPanel = null;
    public GameObject earnedStarPanel = null;
    public Text textGoodWork = null;
    public GameObject triggeredSmokePanel = null;
    public GameObject relapseFactorPanel = null;
    public GameObject welcomePanel = null;

    public Slider afterAwakeSlider = null;
    private float firstCravingAfterAwake = 0.0f;

    public Slider numberOfCigsYesterdaySlider = null;
    private int numberOfCigsYesterday = 0;
    private bool m_skipQuestion = false;
    private float earnedStarRate = 0.0f;

    public Text smokedAmountMessage = null;
    public Image copperFillImage = null;
    public Image silverFillImage = null;
    public Image goldFillImage = null;

    public TriggerChoiceDetail triggerChoiceDetail;
    private string triggerAnswer = "0000000";

    public RelapseChoiceDetail relapseChoiceDetail;
    private string relapseAnswer = string.Empty;

    public Text welcomeText = null;

    // Use this for initialization
    void Start()
    {
        afterWakeupPanel.SetActive(true);
        trackProgressPanel.SetActive(false);
        earnedStarPanel.SetActive(false);
        triggeredSmokePanel.SetActive(false);
        relapseFactorPanel.SetActive(false);
        welcomePanel.SetActive(false);
    }

    public void AfterWakeupPanel_NextButton()
    {
        // afterAwakeSlider max value = (8*60) * 100;
        firstCravingAfterAwake = afterAwakeSlider.value / 100.0f;
        //
        string val = firstCravingAfterAwake.ToString("F2");
        firstCravingAfterAwake = float.Parse(val);
        Debug.Log("First craving after wakeup: " + firstCravingAfterAwake.ToString());

        afterWakeupPanel.SetActive(false);
        trackProgressPanel.SetActive(true);
    }

    public void TrackProgressPanel_NextButton()
    {
        // numberOfCigsYesterdaySlider max value = 50 * 10;
        numberOfCigsYesterday = int.Parse(numberOfCigsYesterdaySlider.value.ToString());
        // re-adjust
        numberOfCigsYesterday = numberOfCigsYesterday / 10;
        Debug.Log("Number of cigarrettes on yesterday: " + numberOfCigsYesterday.ToString());

        // Get last answer in DailyAnswer.
        DailyAnswer lastAnswer = new DailyAnswer();
        lastAnswer = SQLiteManager.Instance.GetLastDailyAnswer(GameManager.Instance.CurrentProfile.ProfileID);
        //
        int cigsPerDay;
        if (lastAnswer.ID > 0)
        {
            cigsPerDay = lastAnswer.CigaretteOnYesterday;
        }
        else
        {
            // ID = -1, no previous record.
            cigsPerDay = GameManager.Instance.CurrentProfile.CigarettePerDay;
        }
        //
        int diff = numberOfCigsYesterday - cigsPerDay;
        if (diff == 0)
        {
            earnedStarRate = 0.0f;
            smokedAmountMessage.text = "You smoked cigarettes you used to!";
            //
            textGoodWork.text = "What happend?";
            m_skipQuestion = false;
        }
        else if (diff > 0)
        {
            earnedStarRate = 0.0f;
            if (diff == 1)
            {
                smokedAmountMessage.text = "You smoked " + Math.Abs(diff) + " cigarette more than you used to!";
            }
            else
            {
                smokedAmountMessage.text = "You smoked " + Math.Abs(diff) + " cigarettes more than you used to!";
            }
            //
            textGoodWork.text = "What happend?";
            m_skipQuestion = false;
        }
        else if (diff < 0)
        {
            earnedStarRate = Convert.ToSingle(cigsPerDay - numberOfCigsYesterday) / Convert.ToSingle(cigsPerDay);
            if (diff == -1)
            {
                smokedAmountMessage.text = "You smoked " + Math.Abs(diff) + " cigarette less than you used to!";
            }
            else
            {
                smokedAmountMessage.text = "You smoked " + Math.Abs(diff) + " cigarettes less than you used to!";
            }
            //
            textGoodWork.text = "Keep up the good work!";
            m_skipQuestion = true;
        }
        //
        Debug.Log("Earned star rate: " + earnedStarRate.ToString());
        //
        trackProgressPanel.SetActive(false);
        earnedStarPanel.SetActive(true);

        earnedStarRate = earnedStarRate * 100;

        float valToFillCopper = 33.0f - earnedStarRate;
        float valToFillSilver;
        float valToFillGold;

        if (valToFillCopper >= 0f)
        {
            copperFillImage.fillAmount = (earnedStarRate / 33.0f);
            silverFillImage.fillAmount = 0;
            goldFillImage.fillAmount = 0;
        }
        else
        {
            valToFillSilver = 66.0f - earnedStarRate;
            if (valToFillSilver >= 0f)
            {
                copperFillImage.fillAmount = 1;
                silverFillImage.fillAmount = ((earnedStarRate - 33.0f) / 33.0f);
                goldFillImage.fillAmount = 0;
            }
            else
            {
                valToFillGold = 100.0f - earnedStarRate;
                if (valToFillGold >= 0f)
                {
                    copperFillImage.fillAmount = 1;
                    silverFillImage.fillAmount = 1;
                    goldFillImage.fillAmount = ((earnedStarRate - 66.0f) / 34.0f);
                }
            }
        }
    }

    public void EarnedStarPanel_NextButton()
    {
        earnedStarPanel.SetActive(false);
        if(m_skipQuestion)
        {
            welcomePanel.SetActive(true);
            //
            welcomeText.text = "Congratulations!\nYou are on day " + GameManager.Instance.CurrentProfile.TotalLoginDays + "!";
        }
        else
        {
            triggeredSmokePanel.SetActive(true);
        }
    }

    public void TriggeredSmokePanel_NextButton()
    {
        triggerAnswer = triggerChoiceDetail.GetTriggerAnswer();
        Debug.Log("Trigger answers: " + triggerAnswer);

        triggeredSmokePanel.SetActive(false);
        relapseFactorPanel.SetActive(true);
    }

    public void RelapseFactorPanel_NextButton()
    {
        relapseAnswer = relapseChoiceDetail.GetRelapseAnswer();
        Debug.Log("Relapse answers: " + relapseAnswer);

        relapseFactorPanel.SetActive(false);
        welcomePanel.SetActive(true);

        //
        welcomeText.text = "Congratulations!\nYou are on day " + GameManager.Instance.CurrentProfile.TotalLoginDays + "!";
    }

    public void WelcomePanel_CloseButton()
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
        //
        DailyAnswer record = new DailyAnswer();
        record.ProfileID = GameManager.Instance.CurrentProfile.ProfileID;
        record.FirstCigaretteAfterWakeup = firstCravingAfterAwake;
        record.CigaretteOnYesterday = numberOfCigsYesterday;
        record.TriggeredSmoke = triggerAnswer;
        record.RelapseFactor = relapseAnswer;
        record.JournalDay = GameManager.Instance.CurrentProfile.TotalLoginDays;
        record.CreateDate = mySQLTime;
        record.Status = 1;
        // Save Data
        SQLiteManager.Instance.DailyAnswer_Insert(record);
        //
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        Debug.Log("Second Day Panel: Load next scene.");
        // Load MapScene
        LoadingScene.LoadScene("MapScene");
    }
}
