﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToCigs : MonoBehaviour {

	public Slider cigsSlider;
	public Text cigsText;

	void Start()
	{
		cigsText.text = "0 Cigs";
	}

	public void ConvertSliderValueToCigsText()
	{
		// cigsSlider.value = 100 * 10;
		int val = Mathf.FloorToInt(cigsSlider.value) / 10;

		cigsText.text = val.ToString() + " Cigs";
	}
}
