﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderToTime : MonoBehaviour {

	public Slider cravingSlider;
	public Text timeText;

	void Start()
	{
		timeText.text = "0 Hours 0 Mins";
	}

	public void ConvertSliderValueToTimeText()
	{
		// cravingSlider.value = 1440 (24H * 60M) * 100;
		int val = Mathf.FloorToInt(cravingSlider.value) / 100;
		int hours = Mathf.FloorToInt(val / 60);
		int mins = val - (hours * 60);

		timeText.text = hours.ToString() + " Hours " + mins.ToString() + " Mins";
	}
}
