﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class OneTimePanel : MonoBehaviour
{
    public GameObject readyToQuitPanel = null;
    public GameObject afterWakeupPanel = null;
    public GameObject nonSmokerPanel = null;
    public GameObject firstWelcomePanel = null;

    public Slider readySlider = null;
    private float readinessRate = 0.0f;

    public Slider afterAwakeSlider = null;
    private float firstCravingAfterAwake = 0.0f;

    public Slider numberOfCigsSlider = null;
    private int numberOfCigsPerDay = 0;

    private int isNonSmoker = 0;

    // Called Order 0
    void Awake()
    {
        Debug.Log("Called Order 0 - Awake");
    }

    // Called Order 1
    void OnEnable()
    {
        Debug.Log("Called Order 1 - OnEnable called");
        //
        Setup();
    }

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        Debug.Log("Called Order 3 - Start");
        //
        readyToQuitPanel.SetActive(true);
    }

    private void Setup()
    {
        // Disable all panels.
        readyToQuitPanel.SetActive(false);
        afterWakeupPanel.SetActive(false);
        nonSmokerPanel.SetActive(false);
        firstWelcomePanel.SetActive(false);
        //
        readinessRate = 0.0f;
        firstCravingAfterAwake = 0.0f;
        numberOfCigsPerDay = 0;
        isNonSmoker = 0;
    }

    public void ReadyToQuitPanel_NextButton()
    {
        readinessRate = readySlider.value / 33.0f;
        Debug.Log("Readiness rate: " + readinessRate.ToString());

        readyToQuitPanel.SetActive(false);
        afterWakeupPanel.SetActive(true);
    }

    public void AfterWakeupPanel_NextButton()
    {
        // afterAwakeSlider max value = (8*60) * 100;
        firstCravingAfterAwake = afterAwakeSlider.value / 100.0f;
        //
        string val = firstCravingAfterAwake.ToString("F2");
        firstCravingAfterAwake = float.Parse(val);
        Debug.Log("First craving after wakeup: " + firstCravingAfterAwake.ToString());

        // numberOfCigsSlider max value = 50 * 10;
        numberOfCigsPerDay = int.Parse(numberOfCigsSlider.value.ToString());
        // re-adjust
        numberOfCigsPerDay = numberOfCigsPerDay / 10;
        Debug.Log("Number of cigarrettes per day: " + numberOfCigsPerDay.ToString());

        afterWakeupPanel.SetActive(false);

        if (readinessRate == 0 || firstCravingAfterAwake == 0 || numberOfCigsPerDay == 0)
        {
            nonSmokerPanel.SetActive(true);
        }
        else
        {
            firstWelcomePanel.SetActive(true);
        }
    }

    public void NonSmokerPanel_YesButton()
    {
        //
        isNonSmoker = 1;
        Debug.Log("Is non smoker: " + isNonSmoker.ToString());
        //
        nonSmokerPanel.SetActive(false);
        firstWelcomePanel.SetActive(true);
    }

    public void NonSmokerPnale_NoButton()
    {
        //
        isNonSmoker = 0;
        Debug.Log("Is non smoker: " + isNonSmoker.ToString());
        //
        nonSmokerPanel.SetActive(false);
        firstWelcomePanel.SetActive(true);
    }

    public void FirstWelcomePanel_PlayButton()
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
        //
        GameManager.Instance.CurrentProfile.ReadinessRate = readinessRate;
        GameManager.Instance.CurrentProfile.FirstCigaretteAfterWakeup = firstCravingAfterAwake;
        GameManager.Instance.CurrentProfile.CigarettePerDay = numberOfCigsPerDay;
        GameManager.Instance.CurrentProfile.IsNonSmoker = isNonSmoker;
        GameManager.Instance.CurrentProfile.UpdateDate = mySQLTime;
        // Save Data
        SQLiteManager.Instance.Profile_Update(GameManager.Instance.CurrentProfile);
        //
        LoadNextScene();
    }

    private void LoadNextScene()
    {
        Debug.Log("One Time Panel: Load next scene.");
        // Load MapScene
        LoadingScene.LoadScene("MapScene");
    }
}
