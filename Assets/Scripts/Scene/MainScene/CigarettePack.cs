﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CigarettePack : MonoBehaviour {
	// Object references
	protected Gameplay gameplay = null;
	//
	public GameObject frameObject;

	// Use this for initialization
	void Start () {
		if (gameplay == null) {
			// Cache references
			gameplay = Gameplay.gameplay;
		}
		//
		frameObject.SetActive (gameplay.GameItemOnFired);
	}

	// Update is called once per frame
	void Update () {
		//
		frameObject.SetActive (gameplay.GameItemOnFired);
	}
}
