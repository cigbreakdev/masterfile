﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFactory : MonoBehaviour
{

	// Static reference to current instance
	public static ObjectFactory currentFactory = null;

	// Prefabs of each object type
	[Header ("Unhealthy")]
	[SerializeField]
	private GameItemData cigarettePrefab = null;
	[SerializeField]
	private GameItemData rollupPrefab = null;
	[SerializeField]
	private GameItemData cigarPrefab = null;
	[SerializeField]
	private GameItemData pipePrefab = null;

	[Header ("Vegetables")]
	[SerializeField]
	private GameItemData carrotPrefab = null;
	[SerializeField]
	private GameItemData celeryPrefab = null;
	[SerializeField]
	private GameItemData springOnionPrefab = null;
	[SerializeField]
	private GameItemData cornPrefab = null;
	[SerializeField]
	private GameItemData chilliPrefab = null;
	[SerializeField]
	private GameItemData bananaPrefab = null;
	[SerializeField]
	private GameItemData broccoliPrefab = null;
	[SerializeField]
	private GameItemData okraPrefab = null;
	[SerializeField]
	private GameItemData sweetPotatoPrefab = null;

	[Header ("Gameover")]
	[SerializeField]
	private GameItemData dynamitePrefab = null;

	[Header ("Candies")]
	[SerializeField]
	private GameItemData lollipopPrefab = null;
	[SerializeField]
	private GameItemData iceLollyPrefab = null;
	[SerializeField]
	private GameItemData candyCanePrefab = null;
	[SerializeField]
	private GameItemData colaPrefab = null;
	[SerializeField]
	private GameItemData chocolatePrefab = null;

	[Header ("Powerup")]
	[SerializeField]
	private GameItemData lighterPrefab = null;

	// swipePrefab (Unhealthy Game Object) mean Cigarette, Rollup, Cigar.
	// unswipePrefab (Healthy Game Object) mean Veggie (Banana, Carrot, Celery, Chilli, Corn, SpringOnion).
	public GameItemData[] respawnObject;
	// six subtypes of game items: Unhealthy, Vegetables, Candies, Powerup, Gameover, Unidentify
	public GameItemData[] UnhealthyObject;
	public GameItemData[] VegetableObject;
	public GameItemData[] CandyObject;
	public GameItemData[] PowerupObject;
	public GameItemData[] GameoverObject;
	public GameItemData[] UnidentifyObject;

	//
	public int pooledAmount = 10;
	//
	public bool willGrow = true;
	//
	[SerializeField]
	List<GameItemData> pooledObjects = null;


	private void Awake ()
	{
		// Don't allow duplicate factories
		if (currentFactory == null) {
			currentFactory = this;
		} else {
			Destroy (gameObject);
			return;
		}
	}

	public void InitialiseObjectFactory (LevelData recordOfLevelData)
	{
		//
		System.Collections.Generic.List<GameItemData> list = new System.Collections.Generic.List<GameItemData> (respawnObject);
		list.RemoveRange (0, list.Count);

		Debug.Log (recordOfLevelData.ToString ());
		// Initialise respawnObject based on Level.

		// Update item's spawn chance depends on each level.
		// Unhealthy items.
		cigarettePrefab.SpawnChance = recordOfLevelData.Cigarette_SpawnChance;
		if (cigarettePrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (cigarettePrefab);
		}
		rollupPrefab.SpawnChance = recordOfLevelData.Rollup_SpawnChance;
		if (rollupPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (rollupPrefab);
		}
		cigarPrefab.SpawnChance = recordOfLevelData.Cigar_SpawnChance;
		if (cigarPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (cigarPrefab);
		}
		pipePrefab.SpawnChance = recordOfLevelData.Pipe_SpawnChance;
		if (pipePrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (pipePrefab);
		}
		// Vegetable items.
		carrotPrefab.SpawnChance = recordOfLevelData.Carrot_SpawnChance;
		if (carrotPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (carrotPrefab);
		}
		celeryPrefab.SpawnChance = recordOfLevelData.Celery_SpawnChance;
		if (celeryPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (celeryPrefab);
		}
		springOnionPrefab.SpawnChance = recordOfLevelData.SpringOnion_SpawnChance;
		if (springOnionPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (springOnionPrefab);
		}
		cornPrefab.SpawnChance = recordOfLevelData.Corn_SpawnChance;
		if (cornPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (cornPrefab);
		}
		chilliPrefab.SpawnChance = recordOfLevelData.Chili_SpawnChance;
		if (chilliPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (chilliPrefab);
		}
		bananaPrefab.SpawnChance = recordOfLevelData.Banana_SpawnChance;
		if (bananaPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (bananaPrefab);
		}
		// Gameover items.
		dynamitePrefab.SpawnChance = recordOfLevelData.Dynamite_SpawnChance;
		if (dynamitePrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (dynamitePrefab);
		}
		// Candy items.
		lollipopPrefab.SpawnChance = recordOfLevelData.Lollipop_SpawnChance;
		if (lollipopPrefab.SpawnChance > 0) {
			// Add Prefab
			list.Add (lollipopPrefab);
		}
		respawnObject = list.ToArray ();
		InitialiseSubTypeObject ();
	}

	public void InitialiseSubTypeObject ()
	{
		//
		List<GameItemData> listOfUnhealthyObject = new List<GameItemData> ();
		List<GameItemData> listOfVegetable = new List<GameItemData> ();
		List<GameItemData> listOfCandy = new List<GameItemData> ();
		List<GameItemData> listOfPowerup = new List<GameItemData> ();
		List<GameItemData> listOfGameover = new List<GameItemData> ();
		List<GameItemData> listOfUnidentify = new List<GameItemData> ();

		for (int i = 0; i < respawnObject.Length; i++) {
			//
			//Debug.Log ("respawnObject [" + i.ToString() + "].TypeOfItem" + respawnObject [i].TypeOfItem);
			//
			if (respawnObject [i].SpawnChance > 0) {
				switch (respawnObject [i].TypeOfItem) 
				{
				case GameItemData.ItemType.Unhealthy:
					listOfUnhealthyObject.Add (respawnObject [i]);
					break;
				case GameItemData.ItemType.Vegetable:
					listOfVegetable.Add (respawnObject [i]);
					break;
				case GameItemData.ItemType.Candy:
					listOfCandy.Add (respawnObject [i]);
					break;
				case GameItemData.ItemType.Powerup:
					listOfPowerup.Add (respawnObject [i]);
					break;
				case GameItemData.ItemType.Gameover:
					listOfGameover.Add (respawnObject [i]);
					break;
				default: //Unidentify
					listOfUnidentify.Add (respawnObject [i]);
					break;
				}
			}
		}

		UnhealthyObject = listOfUnhealthyObject.ToArray ();
		VegetableObject = listOfVegetable.ToArray ();
		CandyObject = listOfCandy.ToArray ();
		PowerupObject = listOfPowerup.ToArray ();
		GameoverObject = listOfGameover.ToArray ();
		UnidentifyObject = listOfUnidentify.ToArray ();
	}

	public GameItemData getItemFromFactory ()
	{
		string itemName = "";
		// Change random method from zero to one to enum random objects.
		// Need to create two type of object collection for Cigs, and Vegs
		float randomVal = Random.Range (0.0f, 1.0f);
		//Debug.Log ("randomVal :: " + randomVal.ToString ());

		// TODO: Adjust the ratio of the respawn items, Unhealthy : Others == 3 : 1

		for (int i = 0; i < respawnObject.Length; i++) {
			//
			if (randomVal < respawnObject [i].SpawnChance) {
				itemName = respawnObject [i].ItemName;
				break;
			} else {
				randomVal = randomVal - respawnObject [i].SpawnChance;
			}
		}

		if (itemName != "") {
			return getPooledObject (itemName);
		} else {
			return null;
		}
	}

	public GameItemData getCigsItemFromFactory ()
	{
		string itemName = "";
		if (UnhealthyObject.Length > 0) {
			int randomVal = Random.Range (0, UnhealthyObject.Length - 1);
			//Debug.Log ("randomVal :: " + randomVal.ToString ());

			itemName = UnhealthyObject [randomVal].ItemName;

			if (itemName != "") {
				return getPooledObject (itemName);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private GameItemData getPooledObject (string itemName)
	{
		GameItemData selectedPooledItem = null;

		//Debug.Log ("pooledObjects.Count::" + pooledObjects.Count.ToString ());

		for (int i = 0; i < pooledObjects.Count; i++) {
			//Debug.Log ("pooledObjects " + i.ToString () + " :: " + pooledObjects [i].name);
			//Debug.Log ("pooledObjects " + i.ToString () + " :: " + pooledObjects [i].gameObject.activeInHierarchy);
			if (pooledObjects [i].ItemName == itemName && !pooledObjects [i].gameObject.activeInHierarchy) {
				//Debug.Log ("Get from inactive.");
				selectedPooledItem = pooledObjects [i];
				break;
			}
		}

		if (selectedPooledItem == null && willGrow) {
			selectedPooledItem = CreateNewGameItem (itemName);
		}

		return selectedPooledItem;
	}

	private GameItemData CreateNewGameItem (string itemName)
	{
		GameItemData newObject = null;
		for (int i = 0; i < respawnObject.Length; i++) {
			if (respawnObject [i].ItemName == itemName) {
				newObject = Instantiate (respawnObject [i]);
				newObject.transform.position = new Vector3 (0, 0, 0);
				newObject.gameObject.SetActive (false);
				pooledObjects.Add (newObject);
				//Debug.Log ("Create new item.");
				break;
			}
		}

		return newObject;
	}

	public void ReturnObject (GameObject obj)
	{
		// Reset GameItem's health and sprite.
		obj.SetActive (false);
		obj.transform.position = transform.position;
	}
}
