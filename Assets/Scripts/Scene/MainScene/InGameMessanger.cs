﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Works on data from InptController and triggers InteractableObjects with input from player
/// </summary>
[RequireComponent (typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class InGameMessanger : MonoBehaviour
{
	// Component and object references
	private CircleCollider2D cCollider = null;
	private MasterProjectile touchedObject = null;

	// Reference to input qualifier function
	private System.Func<LinesHandler.InputType> inputTypeQualifier = null;

	void Awake ()
	{
		cCollider = GetComponent<CircleCollider2D> ();
	}

	private void Start ()
	{

	}

	public void Initialise (System.Func<LinesHandler.InputType> inputTypeQualifier)
	{
		this.inputTypeQualifier = inputTypeQualifier;
	}

	public void Enable ()
	{
		cCollider.enabled = true;
	}

	public void Disable ()
	{
		if (touchedObject != null) {
			ProcessInput (touchedObject);
		}
		touchedObject = null;
		cCollider.enabled = false;
	}

	public void SetPosition (Vector3 pos)
	{
		transform.position = new Vector3 (pos.x, pos.y, 0);
	}

	private void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "GameInteractable") {
			touchedObject = col.GetComponent<MasterProjectile> ();
		}
	}

	private void OnTriggerExit2D (Collider2D col)
	{
		touchedObject = null;
		if (col.tag == "GameInteractable") {
			MasterProjectile obj = col.GetComponent<MasterProjectile> ();
			ProcessInput (obj);
		}
	}

	private void ProcessInput (MasterProjectile obj)
	{
		// Qualify input and call the corresponding event on the object involved
		LinesHandler.InputType inputType = inputTypeQualifier ();

		switch (inputType) {
		case LinesHandler.InputType.Swipe:
			obj.OnSwipe ();
			break;
		case LinesHandler.InputType.Tap:
			obj.OnTap ();
			break;
		}
	}
}
