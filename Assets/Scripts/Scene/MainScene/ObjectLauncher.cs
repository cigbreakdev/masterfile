﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Launches new objects onto the screen during the game
/// </summary>
public class ObjectLauncher : MonoBehaviour
{
	// Customisable values
	[SerializeField]
	private float spread = 2f;
    private int NonCiggieCounter = 0;

	public bool Paused { get; set; }

	// Object references
	Gameplay gameplay = null;
    ObjectFactoryTwo factory = null;
	public GameObject panel;

	

	// Use this for initialization
	void Start ()
	{
		// Cache references
		gameplay = Gameplay.gameplay;
        //factory = ObjectFactory.currentFactory;
        factory = ObjectFactoryTwo.currentFactory;
    }

	public void StartLevel ()
	{
		NonCiggieCounter = 0;
		// Start launch loop based on the level data
        if (gameplay.GamePlay_Debug)
        {
            StartCoroutine(LaunchObjects(gameplay.Debug_First_Spawn_Delay, gameplay.Debug_Frequency));
        }
        else
        {
		    StartCoroutine (LaunchObjects (Gameplay.gameplay.currentLevelData.First_Spawn_Delay, Gameplay.gameplay.currentLevelData.Time_Before_Repeating));
        }
    }

	public void EndLevel ()
	{
		Debug.Log ("Stop all coroutine from ObjectLauncher");
		StopAllCoroutines ();
	}

    private IEnumerator LaunchObjects(float delay, float interval)
    {
        yield return new WaitForSeconds(delay);
        while (true)
        {
            if (!gameplay.Paused && !Paused)
            {
                MasterProjectile obj = null;
                while (obj == null)
                {
                    if (gameplay.GamePlay_Debug)
                    {
                        obj = factory.getNamedItemFromFactory(gameplay.Debug_Item);
                    }
                    else
                    {
                        obj = factory.getItemFromFactory();
                    }
                }

                if (!gameplay.GamePlay_Debug)
                {
                    if (obj.TypeOfItem == "Unhealthy")
                    {
                        NonCiggieCounter = 0;
                    }
                    else
                    {
                        NonCiggieCounter++;
                    }
                }

                if (NonCiggieCounter >= gameplay.maxNonCiggieChainLength)
                {
                    obj = factory.makeCigarette();
                    NonCiggieCounter = 0;
                }

                Debug.Log("Launched -> " + obj.ItemName);
                LaunchObject(obj);

                // Select a new angle for the launcher
                float newAngle = Random.Range(-spread, spread);
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, newAngle));

                yield return new WaitForSeconds(interval);
            }
            else
            {
                yield return null;
            }
        }
    }



    /// <summary>
    /// Repetatly launch object based on the delay and interval
    /// </summary>
    /// <param name="delay"></param>
    /// <param name="interval"></param>
    /// <returns></returns>
    /// Change from LevelObject to healthyObject, unhealthyObject, gameoverObject
    /// LevelObject equal total object that can respawn in level with respawn chance
 //   private IEnumerator LaunchObjectsORIGINAL (float delay, float interval)
	//{
	//	yield return new WaitForSeconds (delay);
	//	while (true) {
	//		if (!gameplay.Paused && !Paused) {
 //               GameItemData obj = null;
 //               while (obj == null) {
	//				obj = factory.getItemFromFactory ();
	//				//
	//				if (gameplay.GameItemOnFired) {
	//					if(obj.TypeOfItem == GameItemData.ItemType.Gameover || obj.ItemName == "Umbrella")
	//					{
	//						obj = null;
	//					}
	//				}
	//			}

	//			if (obj.TypeOfItem == GameItemData.ItemType.Unhealthy) {
	//				myCount = 0;
	//			} else {
	//				myCount++;
	//			}
	//			//Debug.Log ("My Count:" + myCount.ToString ());

	//			if (myCount == maxChain) {
	//				//
	//				if (factory.UnhealthyObject.Length == 0) {
	//					myCount = 0;
	//					break;
	//				} else {
	//					//Debug.Log ("Get new items.");
	//					obj = factory.getCigsItemFromFactory ();
	//					myCount = 0;
	//				}
	//			}
	//			Debug.Log ("Launched -> " + obj.ItemName);
	//			LaunchObject (obj);

	//			// Select a new angle for the launcher
	//			float newAngle = Random.Range (-spread, spread);
	//			transform.rotation = Quaternion.Euler (new Vector3 (0f, 0f, newAngle));

	//			yield return new WaitForSeconds (interval);
	//		} else {
	//			yield return null;
	//		}
	//	}
	//}

	///// <summary>
	///// Launches a single object immidiately using the supplied data item
	///// </summary>
	///// <param name="itemData">Data object to be used</param>
	//public void LaunchSingleObject (GameItemData itemData)
	//{
	//	//
	//	//Debug.Log("LaunchSingleObject");
	//	transform.rotation = Quaternion.Euler (Vector3.zero);
	//	LaunchObject (itemData);
	//}

    private void LaunchObject(MasterProjectile itemData)
    {
        MasterProjectile obj = itemData;
        if (obj != null)
        {
            //
            gameplay.currentLevelPlayed.UpdateSpawnedStats(obj.ItemName);
            //
            obj.gameObject.SetActive(true);
            obj.gameObject.transform.SetParent(gameplay.gameItemsOnScene.transform);
            //
            obj.transform.position = transform.position;
            obj.transform.rotation = transform.rotation;
            //obj.transform.position = new Vector3(Random.Range(-2.0f, 2.0f), Random.Range(0.0f, 5.0f), 0f);
            //obj.transform.rotation = Quaternion.identity;

            Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
            rb.AddForce(obj.transform.up * gameplay.ObjectForce * obj.speed);
            rb.AddTorque(gameplay.ObjectTorque, ForceMode2D.Impulse);

            //gameplay.ObjectSpawned (obj);
        }
    }

    private void LaunchObjectORIGINAL (GameItemData itemData)
	{
		GameItemData obj = itemData;
		if (obj != null) {
			//
			gameplay.currentLevelPlayed.UpdateSpawnedStats (obj.ItemName);
			//
			obj.gameObject.SetActive (true);
			obj.gameObject.transform.SetParent (gameplay.gameItemsOnScene.transform);
			//
			obj.transform.position = transform.position;
			obj.transform.rotation = transform.rotation;
			//obj.transform.position = new Vector3(Random.Range(-2.0f, 2.0f), Random.Range(0.0f, 5.0f), 0f);
			//obj.transform.rotation = Quaternion.identity;

			Rigidbody2D rb = obj.GetComponent<Rigidbody2D> ();
			rb.AddForce (obj.transform.up * gameplay.ObjectForce * obj.Speed);
			rb.AddTorque (gameplay.ObjectTorque, ForceMode2D.Impulse);

			//gameplay.ObjectSpawned (obj);
		}
	}

}
