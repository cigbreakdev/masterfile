﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFactoryTwo : MonoBehaviour {

    // Static reference to current instance
    public static ObjectFactoryTwo currentFactory = null;

    [Header("Master Projectile")]
    public MasterProjectile master = null;
    public float[] odds;
    public string[] items;
    public string item = "";

    private void Awake()
    {
        // Don't allow duplicate factories
        if (currentFactory == null)
        {
            currentFactory = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Use this for initialization
    void Start () {
        items = new string[] { "Cigarette", "Rollup","Cigar",
            "Pipe","Snuff","Dynamite",
            "Banana","Broccoli","Carrot",
            "Celery","Chili","Corn",
            "Ocra","SpringOnion","SweetPotato",
            "CandyCane","Cola","IceLolly",
            "Lollipop","Chocolate","Lighter",
            "DollarBill","Gum","Patch",
            "Spray","Umbrella","eCigarette" };
        odds = new float[items.Length];
    }

    public void InitialiseObjectFactory(LevelData recordOfLevelData)
    {
        int len = items.Length;
        float count = 0.0f;
        odds = new float[len];
        odds[0] = recordOfLevelData.Cigarette_SpawnChance;
        odds[1] = recordOfLevelData.Rollup_SpawnChance;
        odds[2] = recordOfLevelData.Cigar_SpawnChance;
        odds[3] = recordOfLevelData.Pipe_SpawnChance;
        // odds[4] = recordOfLevelData.Snuff_SpawnChance;
        odds[4] = 0.0f;
        odds[5] = recordOfLevelData.Dynamite_SpawnChance;
        odds[6] = recordOfLevelData.Banana_SpawnChance;
        // odds[7] = recordOfLevelData.Broccoli_SpawnChance;
        odds[7] = 0.0f;
        odds[8] = recordOfLevelData.Carrot_SpawnChance;
        odds[9] = recordOfLevelData.Celery_SpawnChance;
        odds[10] = recordOfLevelData.Chili_SpawnChance;
        odds[11] = recordOfLevelData.Corn_SpawnChance;
        // odds[12] = recordOfLevelData.Okra_SpawnChance;
        odds[12] = 0.0f;
        odds[13] = recordOfLevelData.SpringOnion_SpawnChance;
        // odds[14] = recordOfLevelData.SweetPotato_SpawnChance;
        odds[14] = 0.0f;
        // odds[15] = recordOfLevelData.CandyCane_SpawnChance;
        odds[15] = 0.0f;
        // odds[16] = recordOfLevelData.Cola_SpawnChance;
        odds[16] = 0.0f;
        // odds[17] = recordOfLevelData.IceLolly_SpawnChance;
        odds[17] = 0.0f;
        odds[18] = recordOfLevelData.Lollipop_SpawnChance;
        // odds[19] = recordOfLevelData.Chocolate_SpawnChance;
        odds[19] = 0.0f;
        // odds[20] = recordOfLevelData.Lighter_SpawnChance;
        odds[20] = 0.0f;
        // odds[21] = recordOfLevelData.DollarBill_SpawnChance;
        odds[21] = 0.0f;
        // odds[22] = recordOfLevelData.Gum_SpawnChance;
        odds[22] = 0.0f;
        //odds[23] = recordOfLevelData.Patch_SpawnChance;
        odds[23] = 0.0f;
        // odds[24] = recordOfLevelData.Spray_SpawnChance;
        odds[24] = 0.0f;
        // odds[25] = recordOfLevelData.Umbrella_SpawnChance;
        odds[25] = 0.0f;
        // odds[26] = recordOfLevelData.eCigarette_SpawnChance;
        odds[26] = 0.0f;

        // normalise values to ensure adds up to 1
        for (int i = 0; i  < len; i++ )
        {
            count += odds[i];
        }
        if (count != 1.0f)
        {
            count = 1 / count;
            for (int i = 0; i < len; i++)
            {
                odds[i] *= count;
            }
        }

    }

    public MasterProjectile makeCigarette()
    {
        MasterProjectile newObject = null;
        newObject = Instantiate(master);
        newObject.ItemName = "Cigarette";
        newObject.Configure();
        newObject.transform.position = new Vector3(0, 0, 0);
        newObject.gameObject.SetActive(false);
        return newObject;
    }

    public MasterProjectile getItemFromFactory()
    {
        int index = 0;
        float RunningTotal = 0.0f;
        int len = odds.Length;
        float random = Random.Range(0.0f, 1.0f);
        for (int i = 0; i < len; i++)
        {
            RunningTotal += odds[i];
            if (RunningTotal >= random)
            {
                index = i;
                break;
            }
        }
        MasterProjectile newObject = null;
        newObject = Instantiate(master);
        newObject.ItemName = items[index];
        newObject.Configure();
        newObject.transform.position = new Vector3(0, 0, 0);
        newObject.gameObject.SetActive(false);
        return newObject;
    }

    public MasterProjectile getNamedItemFromFactory(string itemName)
    {
        MasterProjectile newObject = null;
        newObject = Instantiate(master);
        newObject.ItemName = itemName;
        newObject.Configure();
        newObject.transform.position = new Vector3(0, 0, 0);
        newObject.gameObject.SetActive(false);
        return newObject;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
