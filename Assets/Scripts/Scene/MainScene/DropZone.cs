﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropZone : MonoBehaviour {

	private ObjectFactory factory = null;

	private void Start()
	{
		factory = ObjectFactory.currentFactory;
	}

	private void OnTriggerEnter2D(Collider2D col)
	{   
		// If interectable object collides with the area emit the OutOfScreen event and return object to object pool
		if (col.tag == "GameInteractable")
		{
            //GameItemData io = col.GetComponent<GameItemData>();
            //io.ReachDropZone();
            col.GetComponent<MasterProjectile>().ReachDropZone();

			//factory.ReturnObject(col.gameObject);
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		Debug.Log (col.tag);
	}
}
