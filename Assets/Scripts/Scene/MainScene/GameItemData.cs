﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using UnityEngine.Events;

public abstract class GameItemData : MonoBehaviour
{
	public enum ItemType
	{
		Unhealthy,
		Vegetable,
		Candy,
		Powerup,
		Gameover,
		Unidentify
	}

	[SerializeField]
	private ItemType itemType = ItemType.Unidentify;

	/// <summary>
	/// Gets or sets the type of the game item.
	/// There are six types of game item: Unhealthy, Vegetables, Candies, Powerup, Gameover and Unidentify.
	/// </summary>
	public ItemType TypeOfItem { get { return itemType; } set { itemType = value; } }

	[SerializeField]
	private string itemName = string.Empty;

	/// <summary>
	/// Gets or sets the name of the game item.
	/// </summary>
	public string ItemName { get { return itemName; } set { itemName = value; } }

	[SerializeField]
	private int maxItemHealth = 1;

	/// <summary>
	/// Gets or sets the max health of the game item.
	/// </summary>
	public int MaxItemHealth { get { return maxItemHealth; } set { maxItemHealth = value; } }

	[SerializeField]
	private int lifeCost = 0;

	/// <summary>
	/// Gets or sets the player life's cost of the game item.
	/// When this game item is destroyed by swipe or drop zone, the player life will deducted by X.
	/// This value should be zero or negative value.
	/// </summary>
	public int LifeCost { get { return lifeCost; } set { lifeCost = value; } }

	[SerializeField]
	private float spawnChance = 0.0f;

	/// <summary>
	/// Gets or sets the spawn chance of the game item.
	/// </summary>
	public float SpawnChance { get { return spawnChance; } set { spawnChance = value; } }

	[SerializeField]
	private float price = 0;

	/// <summary>
	/// Gets or sets the price of the game item.
	/// </summary>
	public float Price { get { return price; } set { price = value; } }

	[Header ("Audio")]
	[SerializeField]
	protected AudioClip[] swipeSounds;

	[Header ("Sprite")]
	[SerializeField]
	protected Sprite[] spritesSet;

	protected int currentItemHealth;
	// Object references
	protected Gameplay gameplay = null;

	// Components
	protected Rigidbody2D rigidBody2D = null;
	protected SpriteRenderer spriteRenderer = null;
	protected BoxCollider2D bCollider = null;
	protected AudioSource audioSource = null;

	// for pause functionality
	protected bool paused = false;
	protected Vector3 velocity = Vector3.zero;
	protected float angularVelocity = 0f;
	protected float gravityScale = 1f;

	//
	protected bool onFire = false;
	[SerializeField]
	protected GameObject flame = null;

	//
	[SerializeField]
	private float speed = 1.0f;

	/// <summary>
	/// Gets or sets the speed of the game item.
	/// </summary>
	public float Speed { get { return speed; } set { speed = value; } }

	// Input response functions
	public abstract void OnSwipe ();
	public abstract void OnTap ();

	//
	protected abstract void OnEnable();
	protected abstract void PrepareGameItem();
	protected abstract void UpdateObjectHealthAndChangeSprite();
	protected abstract void UpdateGameUIDisplay();
	public abstract void ReachDropZone();

	protected void Awake ()
	{
		if (gameplay == null) {
			// Cache references
			gameplay = Gameplay.gameplay;
		}
		// Get and cache component references
		rigidBody2D = GetComponent<Rigidbody2D> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		bCollider = GetComponent<BoxCollider2D> ();
		audioSource = GetComponent<AudioSource> ();
	}

	/// <summary>
	/// Pause or unpause object
	/// Cache values of moving object so it can be sent to contnue the same movement when unpause
	/// </summary>
	/// <param name="pause">true to pause, false to unpause</param>
	public void TogglePause (bool pause)
	{
		paused = pause;
		if (pause) {
			velocity = rigidBody2D.velocity;
			angularVelocity = rigidBody2D.angularVelocity;

			rigidBody2D.velocity = Vector3.zero;
			rigidBody2D.angularVelocity = 0f;
			rigidBody2D.gravityScale = 0f;
		} else {
			rigidBody2D.velocity = velocity;
			rigidBody2D.angularVelocity = angularVelocity;
			rigidBody2D.gravityScale = gravityScale;
		}       
	}

	// Called when object was 'destroyed' due to user input
	protected void OnObjectDestroyed ()
	{
		Debug.Log ("GameItemData - OnObjectDestroyed: " + ItemName);
		gameObject.SetActive (false);
	}
}
