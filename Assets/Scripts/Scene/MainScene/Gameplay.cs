﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class Gameplay : MonoBehaviour
{
    [Header("Change to reflect what level 'bagging veggies' is enabled")]
    public int BaggingLevel = 7;

    [Header("Debug Level Configuration")]
    public bool GamePlay_Debug = true;
    public int Debug_TargetScore = 5000;
    public string Debug_Item = "Cigarette";
    public float Debug_First_Spawn_Delay = 0.0f;
    public float Debug_Time_Before_Repeating = 2.0f;
    public float Debug_Min_Torque = -0.2f;
    public float Debug_Max_Torque = 0.2f;
    public bool Debug_Bagging = true;

    // Singleton variable
    public static Gameplay gameplay = null;

    // Scene references
    private ObjectFactoryTwo factory = null;
    [SerializeField]
    private ObjectLauncher launcher = null;

    public ObjectLauncher Launcher
    {
        get { return launcher; }
    }

    public int LevelIndex = 0;
    public LevelData currentLevelData;
    public LevelPlayed currentLevelPlayed;

    // Game UI
    public Text levelText = null;
    public Text counterText = null;

    private int counter = 0;
    // Counters
    public int CigsBroken { get; private set; }

    // Player health
    [SerializeField]
    private int playerMaxHealth = 6;

    public int PlayerMaxHealth { get { return playerMaxHealth; } }

    [SerializeField]
    private int currentPlayerHealth = 0;

    public int CurrentPlayerHealth { get { return currentPlayerHealth; } }

    [SerializeField]
    private Image lungsSmoke = null;
    [SerializeField]
    private Image screenSmoke = null;

    private float targetSmokeFill = 0f;

    // Values controlling behaviour of in game objects
    [SerializeField]
    private float minObjectForce = 550f;
    [SerializeField]
    private float maxObjectForce = 650f;
    [SerializeField]
    private float forceScale = 1f;

    // Return a random value from the set range
    public float ObjectForce { get { return UnityEngine.Random.Range(minObjectForce, maxObjectForce) * forceScale; } }

    public float ObjectTorque { get
        {
            // DEBUG LEVEL ADAPTATION
            if (GamePlay_Debug)
            {
                return UnityEngine.Random.Range(Debug_Min_Torque, Debug_Max_Torque);
            }
            else
            {
                return UnityEngine.Random.Range(gameplay.currentLevelData.Min_Torque, gameplay.currentLevelData.Max_Torque);
            }
        } }

    public AudioClip[] coughSoundsList;

    AudioSource musicSource;
    AudioSource soundEffectSource;

    [SerializeField]
    private GameObject levelStartImage = null;
    [SerializeField]
    private AnimationCurve scaleAnimation = null;
    [SerializeField]
    private Image blackout = null;

    private bool displaySplash = false;
    private bool displayBlackout = false;

    public GameObject gameItemsOnScene = null;

    public bool Paused { get; private set; }
    //
    public bool GameItemOnFired { get; set; }
    //
    public Button pauseButton;

    public Sprite pauseWhite;
    public Sprite pauseColor;

    public AudioClip[] musicInLevel;
    public Sprite[] itemSprites;
    public AudioClip[] itemSFX;

    private GameObject m_gameManager;
    private bool isNewlyCreated = false;

    void Awake()
    {
        Debug.Log("MainScene - Awake");

        m_gameManager = GameObject.Find("GameManager");

        if (m_gameManager == null)
        {
            GameManager.Instance.Init();
            isNewlyCreated = true;
        }

        // Singleton
        if (gameplay == null)
        {
            gameplay = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if(isNewlyCreated)
        {
            GameManager.Instance.CurrentLevel = gameplay.LevelIndex;
        }
    }

    // Use this for initialization
    void Start()
    {
        // Set up factory and objects
        factory = ObjectFactoryTwo.currentFactory;
        // Interactable objects should not collide with eachother. Layer 12 : Interactable Layer
        Physics2D.IgnoreLayerCollision(12, 12);
        // Get audio source reference
        musicSource = GetComponent<AudioSource>();
        // Add new audio source
        soundEffectSource = gameObject.AddComponent<AudioSource>();

        displaySplash = false;
        displayBlackout = false;
        //
        GameItemOnFired = false;
        //
        LevelStart();

        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;
        Debug.Log("Gameplay end::" + mySQLTime);
    }

    public void SetFullHealth()
    {
        currentPlayerHealth = playerMaxHealth;
        UpdateHealth(currentPlayerHealth);
    }

    public void UpdateHealth(int change)
    {
        //Debug.Log ("change:" + change.ToString());
        int prevHealth = currentPlayerHealth;
        currentPlayerHealth = Mathf.Clamp(currentPlayerHealth + change, 0, playerMaxHealth);

        PlayFailSound();

        if (prevHealth != currentPlayerHealth)
        {

            targetSmokeFill = (float)(playerMaxHealth - currentPlayerHealth) / (float)playerMaxHealth;

            lungsSmoke.fillAmount = targetSmokeFill;
            screenSmoke.color = new Color(1f, 1f, 1f, targetSmokeFill);

            if (currentPlayerHealth == 0)
            {
                //
                LevelFinish();
            }
        }
    }

    public void UpdateScore(int point)
    {
        counter = counter + point;
        counterText.text = counter.ToString() + "/" + currentLevelData.Goal.ToString();
        Debug.Log("Counter " + counter.ToString() + " | Level's Goal " + currentLevelData.Goal.ToString());

        if (counter >= currentLevelData.Goal)
        {
            Debug.Log("Equal Equal");
            //
            LevelFinish();
        }
    }

    public void UpdateSpawnItem(string itemName)
    {
        currentLevelPlayed.UpdateSpawnedStats(itemName);
    }

    private void LevelStart()
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        currentLevelPlayed = new LevelPlayed();
        currentLevelPlayed.ProfileID = GameManager.Instance.CurrentProfile.ProfileID;
        currentLevelPlayed.Level = GameManager.Instance.CurrentLevel;
        currentLevelPlayed.StartTime = mySQLTime;
        currentLevelPlayed.PastResult = SQLiteManager.Instance.GetPastResult(currentLevelPlayed.ProfileID, currentLevelPlayed.Level);

        // Load level data
        LevelIndex = currentLevelPlayed.Level;
        currentLevelData = SQLiteManager.Instance.LoadLevel(LevelIndex);

        levelText.text = "Level " + currentLevelData.Level_ID.ToString();
        counter = 0;
        // DEBUG LEVEL ADAPTION
        if (GamePlay_Debug)
        {
            currentLevelData.Goal = Debug_TargetScore;
        }
        counterText.text = counter.ToString() + "/" + currentLevelData.Goal.ToString();
        Debug.Log(levelText.text + " Goal :" + currentLevelData.Goal.ToString());

        factory.InitialiseObjectFactory(currentLevelData);
        Debug.Log("factory.InitialiseObjectFactory (currentLevelData);");
        SetFullHealth();
        Debug.Log("SetFullHealth ();");

        // Set up level's music
        int MusicLvl = currentLevelData.Level_ID;
        if (MusicLvl > GameManager.Instance.NumberofBackgroundMusicFiles)
        {
            MusicLvl = MusicLvl % GameManager.Instance.NumberofBackgroundMusicFiles;
        }
        //musicSource.clip = Resources.Load ("Audio/Music/Level_" + currentLevelData.Level_ID.ToString (), typeof(AudioClip)) as AudioClip;
        musicSource.clip = musicInLevel[MusicLvl];
        musicSource.loop = true;
        StartCoroutine(FadeInMusic(1f));


        levelStartImage.SetActive(true);
        levelStartImage.transform.localScale = new Vector3(250f, 250f, 1f);
        Debug.Log("Level Start Image");
        StartCoroutine(AnimateBanner());
    }

    private void LevelFinish()
    {

        ToggleMusicPause(true);

        launcher.EndLevel();

        musicSource.Stop();

        EndLevelCalculation();
    }

    private void EndLevelCalculation()
    {
        DateTime curDateTime = DateTime.Now;
        string mySQLTime = curDateTime.Year + "-" + curDateTime.Month + "-" + curDateTime.Day + " " + curDateTime.Hour + ":" + curDateTime.Minute + ":" + curDateTime.Second;

        currentLevelPlayed.EndTime = mySQLTime;
        currentLevelPlayed.Result = currentPlayerHealth / 2;
        currentLevelPlayed.RemainedLife = currentPlayerHealth;
        currentLevelPlayed.CreateDate = mySQLTime;
        currentLevelPlayed.Status = 1;

        GameManager.Instance.lastLevelPlayed = currentLevelPlayed;
        SQLiteManager.Instance.LevelPlayed_Insert(currentLevelPlayed);
        Debug.Log(currentLevelPlayed.ToString());

        StartCoroutine(AnimateBlackout());
    }

    // Coroutine for animate level's start splash
    private IEnumerator AnimateBanner()
    {
        Debug.Log("Animate Banner");
        float step = 1f / 2f;
        float t = 0;
        while (t <= 1f)
        {
            levelStartImage.transform.localScale = new Vector3(250f, 250f) * scaleAnimation.Evaluate(t);
            t += step * Time.deltaTime;
            yield return null;
        }

        levelStartImage.SetActive(false);
        displaySplash = true;
    }

    // Coroutine for animate blackout when level finished.
    private IEnumerator AnimateBlackout()
    {
        Color bgCol = blackout.color;
        float speed = 1f;
        float t = 0f;
        while (t < 1f)
        {
            blackout.color = Color.Lerp(bgCol, Color.black, t);
            t += speed * Time.fixedDeltaTime;
            yield return null;
        }

        displayBlackout = true;
    }

    private void PlayFailSound()
    {
        float healthPercentage = ((float)currentPlayerHealth / (float)playerMaxHealth) * 100;
        if (healthPercentage != 100)
        {
            if (healthPercentage > 80)
            {
                soundEffectSource.PlayOneShot(coughSoundsList[2]);
            }
            else if (healthPercentage > 50)
            {
                soundEffectSource.PlayOneShot(coughSoundsList[1]);
            }
            else
            {
                soundEffectSource.PlayOneShot(coughSoundsList[0]);
            }
        }
    }

    private IEnumerator FadeInMusic(float time)
    {
        musicSource.volume = 0;
        musicSource.Play();

        float speed = 1f / time;
        while (musicSource.volume < 0.5f)
        {
            musicSource.volume += speed * Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator FadeOutMusic(float time)
    {
        float speed = 1f / time;
        while (musicSource.volume > 0f)
        {
            musicSource.volume -= speed * Time.deltaTime;
            yield return null;
        }

        musicSource.Stop();
    }

    public void ToggleMusicPause(bool pause)
    {
        //Paused = pause;
        Time.timeScale = pause ? 0f : 1f;

        if (musicSource != null)
        {
            if (pause)
            {
                musicSource.Pause();
            }
            else
            {
                musicSource.UnPause();
            }
        }
    }

    public void ScreenPause()
    {
        Paused = true;
        // Pause/Unpause all game item on scene.
        GameItemData[] gameObjs = gameItemsOnScene.gameObject.GetComponentsInChildren<GameItemData>(false);
        for (int ii = 0; ii < gameObjs.Length; ii++)
        {
            gameObjs[ii].TogglePause(Paused);
        }
        // Pause/Unpause launcher
        launcher.Paused = Paused;
    }

    public void ScreenUnpause()
    {
        Paused = false;
        // Pause/Unpause all game item on scene.
        GameItemData[] gameObjs = gameItemsOnScene.gameObject.GetComponentsInChildren<GameItemData>(false);
        for (int ii = 0; ii < gameObjs.Length; ii++)
        {
            gameObjs[ii].TogglePause(Paused);
        }
        // Pause/Unpause launcher
        launcher.Paused = Paused;
    }

    public void GameTogglePause()
    {
        Paused = !Paused;
        // Pause/Unpause music
        ToggleMusicPause(Paused);
        // Pause/Unpause all game item on scene.
        GameItemData[] gameObjs = gameItemsOnScene.gameObject.GetComponentsInChildren<GameItemData>(false);
        for (int ii = 0; ii < gameObjs.Length; ii++)
        {
            gameObjs[ii].TogglePause(Paused);
        }
        // Pause/Unpause launcher
        launcher.Paused = Paused;
        //
        if (Paused)
        {
            pauseButton.GetComponent<Image>().sprite = pauseWhite;
        }
        else
        {
            pauseButton.GetComponent<Image>().sprite = pauseColor;
        }
    }

    public void FactoryTogglePause()
    {
        // Pause/Unpause launcher
        launcher.Paused = !launcher.Paused;
    }

    public void ToggleFire()
    {
        GameItemOnFired = !GameItemOnFired;
    }

    // Update is called once per frame
    void Update()
    {
        if (displaySplash)
        {
            Debug.Log("Update display splash");
            displaySplash = false;
            launcher.StartLevel();
        }

        if (displayBlackout)
        {
            displayBlackout = false;
            ToggleMusicPause(false);
            LoadLevelEndScene();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            //Application.Quit ();
            LoadingScene.LoadScene("MapScene");
        }
    }

    public void LoadLevelEndScene()
    {
        LoadingScene.LoadScene("LevelEndScene");
    }

    public void LoadMapScene()
    {
        // Unpaused before go to MapScene,
        // To prevent gameplay stopped when enter again.
        if(Paused)
        {
            GameTogglePause();
        }

        LoadingScene.LoadScene("MapScene");
    }

    void OnApplicationQuit()
    {
        //CigbreakManager.instance.QuitGame ();
        LoadingScene.LoadScene("MapScene");
    }
}
