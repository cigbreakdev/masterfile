﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pipe : Unhealthy
{
	[SerializeField]
	private GameObject pipePowderPrefab = null;
	[SerializeField]
	private Transform releasePoint = null;

	private bool isObjectTurnAround = false;
	private bool isTimeRunOut = false;
	private int tapCount = 0;

	protected Pipe ()
	{
		this.ItemName = "Pipe";
		this.MaxItemHealth = 5;
		// LifeCost should be negative value.
		this.LifeCost = -1;
		//
		this.Speed = 1.0f;
		isObjectTurnAround = false;
		isTimeRunOut = false;
		tapCount = 0;
	}

	void Update()
	{
		/*
		// In game pause state, this item has no effect from input.
		if (isObjectTurnAround && !isTimeRunOut) {
			if (Input.GetMouseButtonDown (0)) {
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				if(Physics.Raycast (ray, out hit))
				{
					if(hit.transform.name == "Pipe")
					{
						Debug.Log ("This is a pipe");
						ReleasePowder ();
					}
					else {
						Debug.Log ("This isn't a pipe");                
					}
				}
			}
		}
		*/
	}

	protected override void OnEnable ()
	{
		PrepareGameItem ();
	}

	protected override void PrepareGameItem ()
	{
		// Initialize value. Prevent confusing with value from editor.
		this.ItemName = "Pipe";
		this.MaxItemHealth = 5;
		// LifeCost should be negative value.
		this.LifeCost = -1;
		//
		this.Speed = 1.0f;
		isObjectTurnAround = false;
		isTimeRunOut = false;
		tapCount = 0;

		//Debug.Log ("UnhealthyItem - PrepareGameItem");
		if (this.spritesSet.Length > 0) {
			if (this.spritesSet [0] != null) {
				GetComponent<SpriteRenderer> ().sprite = this.spritesSet [0];
			} else {
				Debug.Log ("UnhealthyItem - PrepareGameItem: Sprite set is not empty but target sprite can't be found.");
			}
		} else {
			Debug.Log ("UnhealthyItem - PrepareGameItem: Sprite set is empty.");
		}
		// Toggle OnFire state depend on gameplay.
		this.onFire = this.gameplay.GameItemOnFired;
		this.flame.SetActive (this.gameplay.GameItemOnFired);
		// Set up instance's health
		this.currentItemHealth = this.MaxItemHealth;
	}

	private void TurnAround ()
	{
		//
		isObjectTurnAround = true;
		//
		this.gameObject.transform.Rotate (new Vector3 (1, 0, 0), 180.0f);
		//
		StartCoroutine (ActivatePauseEffect (3));
	}

	private IEnumerator ActivatePauseEffect (int second)
	{
		//
		this.gameplay.ScreenPause ();
		//
		for (int count = 0; count < second; count++) {
			//
			yield return new WaitForSeconds (1.0f);
		}
		//
		isTimeRunOut = true;
		//
		this.gameplay.ScreenUnpause ();
	}

	private void ReleasePowder ()
	{
		Rigidbody2D powderClone = (Rigidbody2D)Instantiate (pipePowderPrefab.GetComponent<Rigidbody2D> (), releasePoint.position, releasePoint.rotation);
		powderClone.velocity = -transform.forward * gameplay.ObjectForce;
	}

	public override void OnSwipe ()
	{
		// In game pause state, this item has no effect from input.
		if (!this.paused) {
			Debug.Log ("Pipe - OnSwipe");
			// If the object is not turn around then turn it and stop other obejct 3 sec.
			if (!isObjectTurnAround) {
				//
				TurnAround ();
			}
		}
	}

	public override void OnTap ()
	{
		// In game pause state, this item has no effect from input.
		if (isObjectTurnAround && !isTimeRunOut) {
			// If the object is not yet destroyed continue to reduce its health
			if (this.currentItemHealth > 0) {
				tapCount++;
				Debug.Log ("Pipe On Tap: " + tapCount.ToString ());
				// Each tap need to instantiate the pipe's powder.
				ReleasePowder ();
				//
				UpdateObjectHealthAndChangeSprite ();
			}
		}
	}

	protected override void UpdateObjectHealthAndChangeSprite ()
	{
		//Debug.Log ("Pipe - UpdateObjectHealthAndChangeSprite");
		// Reduce health and update visuals
		this.currentItemHealth = Mathf.Max (this.currentItemHealth - 1, 0);
		UpdateGameUIDisplay ();

		if (this.currentItemHealth == 0) {
			// Update statistics for LevelEndScene
			if (this.onFire) {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName, 1 * 2);
			} else {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName);
			}
		}

		// This item's sprite doesn't relate with its health.

		// This item has only one sound effect.
		if (this.swipeSounds.Length > 0) {
			if (this.swipeSounds [0] != null) {
				this.audioSource.PlayOneShot (this.swipeSounds [0]);
			}
		}

		// Don't deactive it here, let DropZone set them inactive
	}

	protected override void UpdateGameUIDisplay ()
	{
		//Debug.Log ("Pipe - UpdateGameUIDisplay");
		this.gameplay.UpdateScore (1);
	}

	public override void ReachDropZone ()
	{
		//Debug.Log ("Pipe - ReachDropZone");
		if (!isObjectTurnAround) {
			this.gameplay.UpdateHealth (this.LifeCost);
		}

		// Destroyed every objects when it reachs drop zone.
		base.OnObjectDestroyed ();
	}
}
