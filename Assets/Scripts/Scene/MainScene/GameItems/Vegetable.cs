﻿using UnityEngine;

public class Vegetable : GameItemData
{
	[SerializeField]
	protected Sprite paperBag = null;

	protected Vegetable ()
	{
		this.TypeOfItem = ItemType.Vegetable;
		this.ItemName = string.Empty;
		this.MaxItemHealth = 1;
		// LifeCost should be negative value.
		this.LifeCost = -1;
		this.SpawnChance = 0.0f;
		this.Price = 0.0f;
		this.Speed = 1.0f;
	}

	protected override void OnEnable ()
	{
		PrepareGameItem ();
	}

	protected override void PrepareGameItem ()
	{
		//Debug.Log ("Vegetable - PrepareGameItem");
		if (this.spritesSet.Length > 0) {
			if (this.spritesSet [this.MaxItemHealth] != null) {
				GetComponent<SpriteRenderer> ().sprite = this.spritesSet [this.MaxItemHealth];
			} else {
				Debug.Log ("Vegetable - PrepareGameItem: Sprite set is not empty but target sprite can't be found.");
			}
		} else {
			Debug.Log ("Vegetable - PrepareGameItem: Sprite set is empty.");
		}
		// Toggle OnFire state depend on gameplay.
		this.onFire = this.gameplay.GameItemOnFired;
		this.flame.SetActive (this.gameplay.GameItemOnFired);
		// Set up instance's health
		this.currentItemHealth = this.MaxItemHealth;
		//
		this.LifeCost = -1;
	}

	public override void OnSwipe ()
	{
		// In game pause state, this item has no effect from input.
		if (!this.paused) {
			if (GetComponent<SpriteRenderer> ().sprite == paperBag) {
				Debug.Log ("Vegetable - in paper bag");
				// Do nothing.
			} else {
				Debug.Log ("Vegetable - OnSwipe");
				// If the object is not yet destroyed continue to reduce its health
				if (this.currentItemHealth > 0) {
					//
					UpdateObjectHealthAndChangeSprite ();
				}
			}
		}
	}

	public override void OnTap ()
	{
		// In game pause state, this item has no effect from input.
		if (!this.paused) {
			// This option works after level 7.
			if (gameplay.currentLevelData.Level_ID >= 7) {
				Debug.Log ("Vegetable On Tap");
				if (this.onFire) {
					this.gameplay.currentLevelPlayed.UpdateTapStats (this.ItemName, 1 * 2);
				} else {
					this.gameplay.currentLevelPlayed.UpdateTapStats (this.ItemName);
				}

				// Update sprite from vegetable to paper bag.
				if (paperBag != null) {
					if (GetComponent<SpriteRenderer> ().sprite != paperBag && this.currentItemHealth > 0) {
						GetComponent<SpriteRenderer> ().sprite = paperBag;
						Debug.Log ("Vegetable Tap Update");
					}
				} else {
					Debug.Log ("Vegetable - OnTap: Target sprite can't be found.");
				}
			}
		}
	}

	protected override void UpdateObjectHealthAndChangeSprite ()
	{
		//Debug.Log ("Vegetable - UpdateObjectHealthAndChangeSprite");
		// Reduce health and update visuals
		this.currentItemHealth = Mathf.Max (this.currentItemHealth - 1, 0);

		if (this.currentItemHealth == 0) {
			UpdateGameUIDisplay ();
			// Update statistics for LevelEndScene
			if (this.onFire) {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName, 1 * 2);
			} else {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName);
			}
		}

		// Update sprite to match current object health
		if (this.spritesSet.Length > 0) {
			// Prevent if the target sprite doesn't set.
			if (this.spritesSet [this.currentItemHealth] != null) {
				// Sprite 0 mean object destroyed.
				GetComponent<SpriteRenderer> ().sprite = this.spritesSet [this.currentItemHealth];
			} else {
				Debug.Log ("Vegetable - UpdateObjectHealthAndChangeSprite: Sprite set is not empty but target sprite can't be found.");
			}
		} else {
			Debug.Log ("Vegetable - UpdateObjectHealthAndChangeSprite: Sprite set is empty.");
		}

		if (this.swipeSounds.Length > 0) {
			if (this.swipeSounds [currentItemHealth] != null) {
				this.audioSource.PlayOneShot (this.swipeSounds [currentItemHealth]);
			} else {
				this.audioSource.PlayOneShot (this.swipeSounds [0]);
			}
		}

		// Don't deactive it here, let DropZone set them inactive
	}

	protected override void UpdateGameUIDisplay ()
	{
		//Debug.Log ("Vegetable - UpdateGameUIDisplay");
		// No score for this type.
		this.gameplay.UpdateScore (0);
		//
		//Debug.Log("Before this.gameplay.CurrentPlayerHealth:" + this.gameplay.CurrentPlayerHealth.ToString());
		if (this.onFire) {
			this.gameplay.UpdateHealth (this.LifeCost * 2);
		} else {
			this.gameplay.UpdateHealth (this.LifeCost);
		}
		//Debug.Log("After this.gameplay.CurrentPlayerHealth:" + this.gameplay.CurrentPlayerHealth.ToString());
	}

	public override void ReachDropZone ()
	{
		//Debug.Log ("Vegetable - ReachDropZone");
		// This item does not reduce the player's life.
		// Destroyed every objects when it reachs drop zone.
		base.OnObjectDestroyed ();
	}
}
