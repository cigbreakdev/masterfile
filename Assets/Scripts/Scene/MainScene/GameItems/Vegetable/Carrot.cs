﻿public class Carrot : Vegetable {
	Carrot() {
		this.ItemName = "Carrot";
		this.MaxItemHealth = 1;
		// LifeCost should be negative value.
		this.LifeCost = -1;
	}

	protected override void OnEnable ()
	{
		base.PrepareGameItem ();
	}

	protected override void PrepareGameItem ()
	{
		base.PrepareGameItem ();
	}

	public override void OnSwipe ()
	{
		base.OnSwipe ();
	}

	public override void OnTap ()
	{
		base.OnTap ();
	}

	protected override void UpdateObjectHealthAndChangeSprite ()
	{
		base.UpdateObjectHealthAndChangeSprite ();
	}

	protected override void UpdateGameUIDisplay ()
	{
		base.UpdateGameUIDisplay ();
	}

	public override void ReachDropZone ()
	{
		base.ReachDropZone ();
	}
}
