﻿using UnityEngine;

public class Powerup : GameItemData
{
	protected Powerup ()
	{
		this.TypeOfItem = ItemType.Powerup;
		this.ItemName = string.Empty;
		this.MaxItemHealth = 1;
		// LifeCost should be negative value.
		this.LifeCost = 0;
		this.SpawnChance = 0.0f;
		this.Price = 0.0f;
		this.Speed = 1.0f;
	}

	protected override void OnEnable ()
	{
		PrepareGameItem ();
	}

	protected override void PrepareGameItem ()
	{
		//Debug.Log ("Powerup - PrepareGameItem");
		if (this.spritesSet.Length > 0) {
			if (this.spritesSet [this.MaxItemHealth] != null) {
				GetComponent<SpriteRenderer> ().sprite = this.spritesSet [this.MaxItemHealth];
			} else {
				Debug.Log ("Powerup - PrepareGameItem: Sprite set is not empty but target sprite can't be found.");
			}
		} else {
			Debug.Log ("Powerup - PrepareGameItem: Sprite set is empty.");
		}
		// Toggle OnFire state depend on gameplay. This item doesn't affected by lighter
		this.onFire = false;
		this.flame.SetActive (false);
		// Set up instance's health
		this.currentItemHealth = this.MaxItemHealth;
	}

	public override void OnSwipe ()
	{
		// In game pause state, this item has no effect from input.
		if (!this.paused) {
			Debug.Log ("Powerup - OnSwipe");
			// If the object is not yet destroyed continue to reduce its health
			if (this.currentItemHealth > 0) {
				//
				UpdateObjectHealthAndChangeSprite ();
			}
		}
	}

	public override void OnTap ()
	{
		// In game pause state, this item has no effect from input.
		if (!this.paused) {
			Debug.Log ("Powerup On Tap");
		}
	}

	protected override void UpdateObjectHealthAndChangeSprite ()
	{
		//Debug.Log ("Powerup - UpdateObjectHealthAndChangeSprite");
		// Reduce health and update visuals
		this.currentItemHealth = Mathf.Max (this.currentItemHealth - 1, 0);

		if (this.currentItemHealth == 0) {
			UpdateGameUIDisplay ();
			// Update statistics for LevelEndScene
			if (this.onFire) {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName, 1 * 2);
			} else {
				this.gameplay.currentLevelPlayed.UpdateBreakStats (this.ItemName);
			}
		}

		// Update sprite to match current object health
		if (this.spritesSet.Length > 0) {
			// Prevent if the target sprite doesn't set.
			if (this.spritesSet [this.currentItemHealth] != null) {
				// Sprite 0 mean object destroyed.
				GetComponent<SpriteRenderer> ().sprite = this.spritesSet [this.currentItemHealth];
			} else {
				Debug.Log ("Powerup - UpdateObjectHealthAndChangeSprite: Sprite set is not empty but target sprite can't be found.");
			}
		} else {
			Debug.Log ("Powerup - UpdateObjectHealthAndChangeSprite: Sprite set is empty.");
		}

		if (this.swipeSounds.Length > 0) {
			if (this.swipeSounds [currentItemHealth] != null) {
				this.audioSource.PlayOneShot (this.swipeSounds [currentItemHealth]);
			} else {
				this.audioSource.PlayOneShot (this.swipeSounds [0]);
			}
		}

		// Don't deactive it here, let DropZone set them inactive
	}

	protected override void UpdateGameUIDisplay ()
	{
		//Debug.Log ("Powerup - UpdateGameUIDisplay");
		// No score for this type.
		this.gameplay.UpdateScore (0);
		//
		if (this.onFire) {
			this.gameplay.UpdateHealth (this.LifeCost * 2);
		} else {
			this.gameplay.UpdateHealth (this.LifeCost);
		}
	}

	public override void ReachDropZone ()
	{
		//Debug.Log ("Powerup - ReachDropZone");
		// This item does not reduce the player's life.
		// Destroyed every objects when it reachs drop zone.
		base.OnObjectDestroyed ();
	}
}