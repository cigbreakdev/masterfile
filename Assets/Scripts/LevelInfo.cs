﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelInfo : MonoBehaviour {

    public Text menuText;
    public Text targetOnLevel;

    public GameObject journalPanel;

    public GameObject tutorialPanel;
    public GameObject[] tutorialsPrefab;
    private GameObject m_tutorialItem;

    private string m_menuName = "Level ";
    private LevelData m_levelData;
    private Color m_journalPanelColor;

    // Called Order 1
    void OnEnable()
    {
        // Bring this game object to the front
        transform.SetAsLastSibling();
        // Reset background colour from tutorial
        m_journalPanelColor = journalPanel.GetComponent<Image>().color;
        //
        LevelSetup();
    }

    // Called Order 2
    void LevelSetup()
    {
        Debug.Log("Called Order 2 - LevelInfo-LevelSetup");
        //
        Debug.Log("Current level:" + GameManager.Instance.CurrentLevel);
        //
        m_levelData = SQLiteManager.Instance.LoadLevel(GameManager.Instance.CurrentLevel);

        //
        menuText.text = m_menuName + " " + m_levelData.Level_ID;
        //
        targetOnLevel.text = "X " + m_levelData.Goal;

        //
        if (tutorialsPrefab[m_levelData.Level_ID - 1] != null)
        {
            m_tutorialItem = Instantiate(tutorialsPrefab[m_levelData.Level_ID - 1]) as GameObject;
            m_tutorialItem.name = "Tutorial_" + m_levelData.Level_ID;
            m_tutorialItem.SetActive(true);
            m_tutorialItem.transform.SetParent(tutorialPanel.transform, false);
        }

        else
        {
            //
        }

        //
        if(SQLiteManager.Instance.GetPastResult(GameManager.Instance.CurrentProfile.ProfileID, 3) == -1)
        {
            journalPanel.SetActive(false);
        }
        else
        {
            journalPanel.SetActive(true);
            SetupJournalMission();
        }
    }

    void SetupJournalMission()
    {
        //
        if (SQLiteManager.Instance.GetPastResult(GameManager.Instance.CurrentProfile.ProfileID, 3) == -1)
        {
            
        }
    }

    // Called Order 3
    // Use this for initialization.
    void Start()
    {
        Debug.Log("Called Order 3 - Start");
    }

    // Called when the game is terminated. Called Order Last
    void OnDisable()
    {
        // Reset background colour from tutorial
        journalPanel.GetComponent<Image>().color = m_journalPanelColor;
        Debug.Log("Called when the game is terminated - OnDisable");
    }

    public void Play()
    {
        LoadingScene.LoadScene("MainScene");
    }

    public void ClosePanel()
    {
        DestroyObject(m_tutorialItem);

        // Hide this game object
        gameObject.SetActive(false);
    }
}
