﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterProjectile : MonoBehaviour {

    [Header("Customisable Variables")]
    public string ItemName = string.Empty;
    public string TypeOfItem = string.Empty;
    public int MaxItemHealth = 1;
    public float SpeedFactor = 1.0f;
    public float TorqueFactor = 1.0f;
    public float gravityScale = 1.0f;
    public AudioClip[] swipeSounds;
    public Sprite[] spritesSet;
    private Sprite paperbag = null;
    private bool bagged = false;
    public float Price = 1.0f;
    public int LifeCost = -1;
    public GameObject flame;
    public float BoxColliderXSize = 1.0f;
    public float BoxColliderYSize = 1.0f;
    [Header("In-Play/Level Variables")]
    public float speed = 1.0f;
    public Vector3 velocity = Vector3.zero;
    public float angularVelocity = 0.0f;
    public float spawnChance = 0.0f;
    public int currentItemHealth = 1;
    public bool paused = false;
    public bool onFire = false;

    [Header("Behaviour Bools")]
    public bool KillOnSwipe = false;
    public bool KillOnTap = false;
    public bool BagOnTap = false;
    public bool BagOnSwipe = false;
    public bool LifePenaltyOnDropZone = false;
    public bool LifePenaltyOnKill = false;
    public bool ScoreOnKill = false;
    public bool SpecialBehaviourPipe = false;   // used by Pipe
    public bool ControlBoolOne = false;         // used by Pipe
    public int ControlIntOne = 0;
    public float ControlTimer = 0.0f;           // used by Pipe

    private Gameplay gameplay;
    private ObjectFactoryTwo factory;
    private Rigidbody2D rigidBody2D;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D bCollider;
    private AudioSource audioSource;
    private Transform trans;

    void Awake()
    {
        if (gameplay == null)
        {
            // Cache references
            gameplay = Gameplay.gameplay;
        }
        factory = ObjectFactoryTwo.currentFactory;
        // Get and cache component references
        rigidBody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        bCollider = GetComponent<BoxCollider2D>();
        audioSource = GetComponent<AudioSource>();
        trans = GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        Configure();
	}
	
	// Update is called once per frame
	void Update () {
		if (ControlTimer > 0.0f)
        {
            ControlTimer = Mathf.Max(0.0f, ControlTimer -= Time.deltaTime);
            if (ControlTimer == 0.0f)
            {
                TimerRunOut();
            }
        }
	}

    // Called when item is created and after ItemName has been set.
    public void Configure()
    {
        // set ItemType from ItemName && set defaults for type
        // N.B. using switch fall-through intentionally - BE CAREFUL
        switch (ItemName)
        {
            // unhealthy
            case "Cigarette":
            case "Rollup":
            case "Cigar":
            case "Pipe":
            case "Snuff":
                TypeOfItem = "Unhealthy";
                KillOnSwipe = true;
                LifePenaltyOnDropZone = true;
                ScoreOnKill = true;
                MaxItemHealth = 1;
                break;
            // gameover
            case "Dynamite":
                TypeOfItem = "Gameover";
                KillOnSwipe = true;
                LifePenaltyOnKill = true;
                LifeCost = -6;
                MaxItemHealth = 1;
                break;
            // vegetables
            case "Banana":
            case "Broccoli":
            case "Carrot":
            case "Celery":
            case "Chili":
            case "Corn":
            case "Ocra":
            case "SpringOnion":
            case "SweetPotato":
                TypeOfItem = "Vegetable";
                KillOnSwipe = true;
                LifePenaltyOnKill = true;
                BagOnTap = true;
                paperbag = gameplay.itemSprites[0];
                MaxItemHealth = 1;
                break;
            // candy
            case "CandyCane":
            case "Cola":
            case "IceLolly":
            case "Lollipop":
            case "Chocolate":
                TypeOfItem = "Candy";
                KillOnSwipe = true;
                LifePenaltyOnKill = true;
                MaxItemHealth = 1;
                break;
            // powerup/specials
            case "Lighter":
            case "DollarBill":
            case "Gum":
            case "Patch":
            case "Spray":
            case "Umbrella":
            case "eCigarette":
            case "Tobacco":
            case "Powder":
                TypeOfItem = "Powerup";
                MaxItemHealth = 1;
                break;
            // anything else is a default
            default:
                break;
        }

        // Second pass to set individual items and special cases
        switch (ItemName)
        {
            case "Cigarette":
                setSpritesAndSound(new int[] { 1,2 }, new int[] { 1 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.2f, 1.1f });
                break;
            case "Rollup":
                setSpritesAndSound(new int[] { 3, 4 }, new int[] { 2 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.1f });
                break;
            case "Cigar":
                setSpritesAndSound(new int[] { 5,6,7 }, new int[] { 2,3 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.2f, 1.0f });
                MaxItemHealth = 2;
                LifeCost = -2;
                break;
            case "Pipe":
                setSpritesAndSound(new int[] { 8 }, new int[] { 4 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 1.0f, 0.6f });
                MaxItemHealth = 5;
                LifeCost = -2;
                // re-set Unhealthy behaviour bools
                // N.B. Pipe scores for each tap ... but LifePenaltyOnDropZone = true .. so will do damage if itemhealth > 0
                KillOnSwipe = false;
                ScoreOnKill = false;
                // set pipe behaviour
                SpecialBehaviourPipe = true;
                ControlTimer = 0.0f;
                break;
            case "Snuff":
                setSpritesAndSound(new int[] { 10 }, new int[] { 5 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 1.0f, 1.0f }); // N.B. Not tested BBox scale
                MaxItemHealth = 5;
                LifeCost = -5;
                break;
            case "Dynamite":
                setSpritesAndSound(new int[] { 30,31 }, new int[] { 11,10 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                audioSource.PlayOneShot(swipeSounds[1]);
                break;
            case "Banana":
                setSpritesAndSound(new int[] { 12,13 }, new int[] { 9 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.1f });
                break;
            case "Broccoli":
                setSpritesAndSound(new int[] { 14,15 }, new int[] { 8 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.7f, 1.1f });
                break;
            case "Carrot":
                setSpritesAndSound(new int[] {16,17 }, new int[] { 7 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.1f });
                break;
            case "Celery":
                setSpritesAndSound(new int[] { 18,19 }, new int[] { 8 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.6f, 1.1f });
                break;
            case "Chili":
                setSpritesAndSound(new int[] { 20,21 }, new int[] { 9 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.2f, 1.0f });
                break;
            case "Corn":
                setSpritesAndSound(new int[] { 22,23 }, new int[] { 9 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                break;
            case "Ocra":
                setSpritesAndSound(new int[] { 24,25 }, new int[] { 7 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                break;
            case "SpringOnion":
                setSpritesAndSound(new int[] { 26,27 }, new int[] { 8 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                break;
            case "SweetPotato":
                setSpritesAndSound(new int[] { 28,29 }, new int[] { 7 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                break;
            case "CandyCane":
                setSpritesAndSound(new int[] { 32,33 }, new int[] { 2 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.1f });
                break;
            case "Cola":
                setSpritesAndSound(new int[] { 34,35 }, new int[] { 1 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.35f, 1.0f });
                break;
            case "IceLolly":
                setSpritesAndSound(new int[] { 36,37 }, new int[] { 1 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.3f, 1.0f });
                break;
            case "Lollipop":
                setSpritesAndSound(new int[] { 38,39 }, new int[] { 1 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.0f });
                break;
            case "Chocolate":
                setSpritesAndSound(new int[] { 40,41 }, new int[] { 2 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.63f, 1.1f });
                break;
            case "Lighter":
                setSpritesAndSound(new int[] { 42 }, new int[] { 12 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.0f });
                break;
            case "DollarBill":
                setSpritesAndSound(new int[] { 43,44 }, new int[] { 13 });
                setScaleAndBoundingBox(new float[] { 1.5f, 1.5f, 0.5f, 1.0f });
                KillOnSwipe = true;
                break;
            case "Gum":
                setSpritesAndSound(new int[] { 45,46 }, new int[] { 15,14 });
                setScaleAndBoundingBox(new float[] { 1.5f, 1.5f, 0.5f, 1.0f });
                KillOnSwipe = true;
                break;
            case "Patch":
                setSpritesAndSound(new int[] { 47 }, new int[] { 16,17 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.9f, 0.9f });
                break;
            case "Spray":
                setSpritesAndSound(new int[] { 48 }, new int[] { 18 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.0f }); // N.B. Not tested BBox scale
                break;
            case "Umbrella":
                setSpritesAndSound(new int[] { 49 }, new int[] { 19 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.0f }); // N.B. Not tested BBox scale
                break;
            case "eCigarette":
                setSpritesAndSound(new int[] { 50 }, new int[] { 20 });
                setScaleAndBoundingBox(new float[] { 2.5f, 2.5f, 0.5f, 1.0f }); // N.B. Not tested BBox scale
                break;
            case "Tobacco":
                setSpritesAndSound(new int[] { 9 }, new int[] { 4 });
                setScaleAndBoundingBox(new float[] { 1.0f, 1.0f, 0.1f, 0.1f });
                break;
            case "Powder":
                setSpritesAndSound(new int[] { 11 }, new int[] { 6 });
                setScaleAndBoundingBox(new float[] { 1.0f, 1.0f, 1.0f, 1.0f });
                break;

            default:
                break;
        }

        // set up ready to go (( Replaces previous PrepareGameItem () method))
        currentItemHealth = MaxItemHealth;
        if (spritesSet.Length > 0)
        {
            if (spritesSet.Length == 1) // solo sprite set
            {
                GetComponent<SpriteRenderer>().sprite = spritesSet[0];
            }
            else if (spritesSet[currentItemHealth] != null)
            {
                GetComponent<SpriteRenderer>().sprite = spritesSet[currentItemHealth];
            }
            else
            {
                Debug.Log(ItemName + " - Configure(): Sprite set is not empty but target sprite can't be found.");
            }
        }
        else
        {
            Debug.Log(ItemName + " - Configure(): Sprite set is empty.");
        }
        // Toggle OnFire state depend on gameplay. //NOT CURRENTLY IMPLEMENTED
        //onFire = gameplay.GameItemOnFired;
        //flame.SetActive(gameplay.GameItemOnFired);
    }

    public void OnSwipe()
    {
        // In game pause state, this item has no effect from input.
        if (paused) return;

        Debug.Log(ItemName + " - OnSwipe()");
        if (KillOnSwipe && currentItemHealth > 0 && !bagged)
        {
            UpdateObjectHealthAndChangeSprite();
        }

        else if (SpecialBehaviourPipe && ControlTimer == 0.0f && currentItemHealth > 0)
        {
            ControlTimer = 3.0f;
            gameplay.ScreenPause();
            TurnAround();
            audioSource.PlayOneShot(swipeSounds[0]);
        }

    }

 public void OnTap()
    {

        // Pipe behaviour HAPPENS WHEN ITEM IS PAUSED
        if (SpecialBehaviourPipe && ControlTimer > 0 && currentItemHealth > 0)
        {
            // instantiate and drop tobacco
            MasterProjectile tobacco = factory.getNamedItemFromFactory("Tobacco");
            DropObject(tobacco, transform);
            tobacco.Score(); // since it has a health of 1 ... else would use pipe's Health (i.e. more)
            audioSource.PlayOneShot(swipeSounds[0]);
            currentItemHealth = Mathf.Max(0,currentItemHealth -1);
            // if last of Tobacco ... can unpause the continue game)
            if (currentItemHealth == 0)
            {
                ControlTimer = 0.0f;
                TimerRunOut();
            }
            return;
        }

        // In game pause state, this item has no effect from input.
        if (paused) return;

        Debug.Log(ItemName + "OnTap()");
        // do "Bagging" first (and return)
        if (BagOnTap && currentItemHealth > 0 && !bagged) // will move this to a seperate function if "BagOnSwipe" is implemented 
        {
            if ((gameplay.currentLevelData.Level_ID >= gameplay.BaggingLevel) || (gameplay.GamePlay_Debug && gameplay.Debug_Bagging) )
            {
                bagged = true;
                currentItemHealth = 0;
                Debug.Log(ItemName + " On Tap");
                if (onFire)
                {
                    gameplay.currentLevelPlayed.UpdateTapStats(ItemName, 1 * 2);
                }
                else
                {
                    gameplay.currentLevelPlayed.UpdateTapStats(ItemName);
                }
                // Update sprite from vegetable to paper bag.
                if (paperbag != null)
                {
                    if (GetComponent<SpriteRenderer>().sprite != paperbag)
                    {
                        GetComponent<SpriteRenderer>().sprite = paperbag;
                        Debug.Log(ItemName + " bagged at OnTap()");
                    }
                }
                else
                {
                    Debug.Log(ItemName + " OnTap(): Target 'bagged' sprite can't be found.");
                }
                // play Collect Veggie sound
                audioSource.PlayOneShot(gameplay.itemSFX[0]);
            }
            return;
        }

        if (KillOnTap && currentItemHealth > 0)
        {
            UpdateObjectHealthAndChangeSprite();
        }
    }

    private void DropObject(MasterProjectile item, Transform location)
    {
        MasterProjectile obj = item;
        if (obj != null)
        {
            obj.gameObject.SetActive(true);
            obj.gameObject.transform.SetParent(gameplay.gameItemsOnScene.transform);
            obj.transform.position = location.position;
            // apply items type offsets
            if (obj.ItemName == "Tobacco")
            {
                obj.transform.Translate(0.95f, -0.55f, 0, Space.Self);
            }
            Rigidbody2D rb = obj.GetComponent<Rigidbody2D>();
            rb.AddForce(-obj.transform.up * gameplay.ObjectForce * obj.speed);
        }
    }


    public void TimerRunOut()
    {
        if (SpecialBehaviourPipe)
        {
            TurnAround();
            gameplay.ScreenUnpause();
        }
    }

    private void UpdateObjectHealthAndChangeSprite()
    {
        // Reduce health and update visuals
        currentItemHealth = Mathf.Max(currentItemHealth - 1, 0);

        if (currentItemHealth == 0)
        {
            UpdateGameUIDisplay();
            // Update statistics for LevelEndScene
            if (onFire)
            {
                gameplay.currentLevelPlayed.UpdateBreakStats(ItemName, 1 * 2);
            }
            else
            {
                gameplay.currentLevelPlayed.UpdateBreakStats(ItemName);
            }
        }

        // Update sprite to match current object health
        if (spritesSet.Length > 0)
        {
            if (spritesSet.Length == 1) // solo sprite set
            {
                // do nothing.  Sprite will have already been set
            }
            // Prevent if the target sprite isn't set.
            else if (spritesSet[currentItemHealth] != null)
            {
                // Sprite 0 mean object destroyed.
                GetComponent<SpriteRenderer>().sprite = spritesSet[currentItemHealth];
            }
            else
            {
                Debug.Log(ItemName + " UpdateObjectHealthAndChangeSprite(): Sprite set is not empty but target sprite can't be found.");
            }
        }
        else
        {
            Debug.Log(ItemName + " UpdateObjectHealthAndChangeSprite: Sprite set is empty.");
        }

        if (swipeSounds.Length > 0)
        {
            if (swipeSounds[currentItemHealth] != null)
            {
                audioSource.PlayOneShot(swipeSounds[currentItemHealth]);
            }
            else
            {
                audioSource.PlayOneShot(swipeSounds[0]);
            }
        }
        // Don't deactive it here, let DropZone set them inactive
    }

    void UpdateGameUIDisplay()
    {
        // called when an object is killed (Health = 0)
        // You wanted to do that .....
        if (ScoreOnKill) Score();

        // You didn't want to do this .....
        if (LifePenaltyOnKill) LooseLife();
    }

    public void ReachDropZone()
    {
        if (LifePenaltyOnDropZone && currentItemHealth > 0) LooseLife();
        Destroy(gameObject);
    }

    private void TurnAround()
    {
        ControlBoolOne = !ControlBoolOne;
        gameObject.transform.Rotate(new Vector3(1, 0, 0), 180.0f);
    }


    public void TogglePause(bool pause)
    {
        paused = pause;
        if (pause)
        {
            velocity = rigidBody2D.velocity;
            angularVelocity = rigidBody2D.angularVelocity;

            rigidBody2D.velocity = Vector3.zero;
            rigidBody2D.angularVelocity = 0f;
            rigidBody2D.gravityScale = 0f;
        }
        else
        {
            rigidBody2D.velocity = velocity;
            rigidBody2D.angularVelocity = angularVelocity;
            rigidBody2D.gravityScale = gravityScale;
        }
    }

    void Score()
    {
        int mod = 1;
        if (onFire) mod = 2;
        gameplay.UpdateScore(MaxItemHealth * mod);
        Debug.Log(ItemName + " Score() of " + (MaxItemHealth * mod));
    }

    void LooseLife()
    {
        if (!gameplay.Debug_InfiniteLives)
        {
            int mod = 1;
            if (onFire) mod = 2;
            gameplay.UpdateHealth(LifeCost * mod);
            Debug.Log(ItemName + " LooseLife() of " + (LifeCost * mod));
        }
    }

    void setSpritesAndSound(int[] sprites, int[] sounds)
    {
        spritesSet = new Sprite[sprites.Length];
        for (int i = 0; i < sprites.Length; i++)
        {
            spritesSet[i] = gameplay.itemSprites[sprites[i]];
        }
        swipeSounds = new AudioClip[sounds.Length];
        for (int i = 0; i < sounds.Length; i++)
        {
            swipeSounds[i] = gameplay.itemSFX[sounds[i]];
        }
    }

    void setScaleAndBoundingBox (float[] vars)
    {
        if (vars.Length == 4)
        {
            trans.localScale = new Vector3(vars[0], vars[1], 1);
            bCollider.size = new Vector2(vars[2], vars[3]);
        }
    }

}
