﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitMarket : MonoBehaviour
{
    public GameObject fruitMarketPanel;

    public void displayFruitMarketPanel()
    {
        fruitMarketPanel.SetActive(true);
    }
}
